package by.training.yukopilets.core;

import java.util.HashMap;
import java.util.Map;

public class BeanRegistry {

    private Map<Class<?>, Object> beans;

    public BeanRegistry() {
        beans = new HashMap<>();
    }

    public<T> void addBean(Class<T> clazz, T obj) {
        beans.put(clazz, obj);
    }

    public Object getBean(Class<?> clazz) {
        return beans.get(clazz);
    }
}
