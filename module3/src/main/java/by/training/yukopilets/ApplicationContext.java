package by.training.yukopilets;

import by.training.yukopilets.builder.DeviceBuilder;
import by.training.yukopilets.builder.EntityBuilder;
import by.training.yukopilets.command.*;
import by.training.yukopilets.core.BeanRegistry;
import by.training.yukopilets.dao.InMemoryDeviceDao;
import by.training.yukopilets.model.Device;
import by.training.yukopilets.parser.*;
import by.training.yukopilets.service.LoadDeviceService;
import by.training.yukopilets.service.SelectDeviceService;
import by.training.yukopilets.validator.XMLValidator;
import by.training.yukopilets.validator.XMLValidatorImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

public class ApplicationContext {

    private static final Logger LOGGER = LogManager.getLogger();
    private static ApplicationContext instance;
    private BeanRegistry registry = new BeanRegistry();

    private ApplicationContext() { }

    public static void initialize() {
        ApplicationContext appContext = new ApplicationContext();
        appContext.init();
        instance = appContext;
    }

    private void init() {
        InMemoryDeviceDao deviceDao = new InMemoryDeviceDao();
        registry.addBean(InMemoryDeviceDao.class, deviceDao);
        LoadDeviceService loadDeviceService = new LoadDeviceService(deviceDao);
        SelectDeviceService selectDeviceService = new SelectDeviceService(deviceDao);
        registry.addBean(LoadDeviceService.class, loadDeviceService);
        registry.addBean(SelectDeviceService.class, selectDeviceService);

        XMLValidator validator = null;
        try {
            String schemaLocation = getClass().getClassLoader().getResource("devices.xsd").toURI().getPath();
            validator = new XMLValidatorImpl("http://www.w3.org/2001/XMLSchema", schemaLocation);
        } catch (SAXException e) {
            LOGGER.warn("Validator initialize exception!", e);
        } catch (URISyntaxException e) {
            LOGGER.warn("URI making exception!", e);
        }

        EntityBuilder<Device> builder = new DeviceBuilder();
        DeviceSAXParser saxParser = new DeviceSAXParser(validator, builder);
        DeviceDOMParser domParser = new DeviceDOMParser(validator, builder);
        DeviceStAXParser stAXParser = new DeviceStAXParser(validator, builder);
        Map<DeviceParserName, AbstractXMLParser<Device>> parsers = new HashMap<>();
        parsers.put(DeviceParserName.SAX, saxParser);
        parsers.put(DeviceParserName.DOM, domParser);
        parsers.put(DeviceParserName.STAX, stAXParser);
        DeviceParserFactory parserFactory = new DeviceParserFactory(parsers);
        registry.addBean(DeviceParserFactory.class, parserFactory);

        UploadServletCommand uploadCommand = new UploadServletCommand(loadDeviceService, parserFactory);
        DownloadServletCommand downloadCommand = new DownloadServletCommand(selectDeviceService);
        ShowServletCommand showCommand = new ShowServletCommand(selectDeviceService);
        Map<ServletCommandName, ServletCommand> commands = new HashMap<>();
        commands.put(ServletCommandName.UPLOAD, uploadCommand);
        commands.put(ServletCommandName.DOWNLOAD, downloadCommand);
        commands.put(ServletCommandName.SHOW, showCommand);
        ServletCommandFactory commandFactory = new ServletCommandFactory(commands);
        registry.addBean(ServletCommandFactory.class, commandFactory);
    }

    public Object getBean(Class<?> clazz) {
        return registry.getBean(clazz);
    }

    public static ApplicationContext getInstance() {
        if(instance == null) {
            throw new IllegalStateException("Context hasn't been initialized");
        }
        return instance;
    }
}
