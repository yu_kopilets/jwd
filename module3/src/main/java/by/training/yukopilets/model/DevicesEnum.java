package by.training.yukopilets.model;

public enum DevicesEnum {

    DEVICES("Devices"),
    DEVICE("Device"),
    ID("id"),
    MARKET_ENTRY("market-entry"),
    NAME("Name"),
    ORIGIN("Origin"),
    PRICE("Price"),
    INTERNAL("Internal"),
    PERIPHERAL("Peripheral"),
    ENERGY_CONSUMPTION("EnergyConsumption"),
    COOLER("Cooler"),
    DEVICE_GROUP("DeviceGroup"),
    PORT("Port"),
    CRITICAL("Critical");

    private String tagName;

    DevicesEnum(String tagName) {
        this.tagName = tagName;
    }

    public String getTagName() {
        return this.tagName;
    }

    public static DevicesEnum fromString(String tagName) throws DevicesEnumNotFoundException {
        for (DevicesEnum value : DevicesEnum.values()) {
            if(value.tagName.equals(tagName)) {
                return value;
            }
        }
        throw new DevicesEnumNotFoundException("DevicesEnum value from this tag doesn't exist!");
    }
}
