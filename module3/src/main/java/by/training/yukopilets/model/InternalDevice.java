package by.training.yukopilets.model;

import by.training.yukopilets.builder.XMLTag;

@XMLTag(name = "Internal")
public class InternalDevice extends DeviceType {

    @XMLTag(name = "Cooler")
    private boolean cooler;

    public boolean isCooler() {
        return cooler;
    }

    public void setCooler(boolean cooler) {
        this.cooler = cooler;
    }

    @Override
    public String toString() {
        return "InternalDevice{" +
                "cooler=" + cooler +
                '}';
    }
}
