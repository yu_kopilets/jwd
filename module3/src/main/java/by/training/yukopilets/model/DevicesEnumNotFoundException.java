package by.training.yukopilets.model;

public class DevicesEnumNotFoundException extends Exception {
    public DevicesEnumNotFoundException() {
    }

    public DevicesEnumNotFoundException(String message) {
        super(message);
    }
}
