package by.training.yukopilets.model;

import by.training.yukopilets.builder.XMLTag;

@XMLTag(name = "Peripheral")
public class PeripheralDevice extends DeviceType {

    @XMLTag(name = "DeviceGroup")
    private String deviceGroup;

    @XMLTag(name = "Port")
    private String port;

    public String getDeviceGroup() {
        return deviceGroup;
    }

    public void setDeviceGroup(String deviceGroup) {
        this.deviceGroup = deviceGroup;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    @Override
    public String toString() {
        return "PeripheralDevice{" +
                "deviceGroup='" + deviceGroup + '\'' +
                ", port='" + port + '\'' +
                '}';
    }
}
