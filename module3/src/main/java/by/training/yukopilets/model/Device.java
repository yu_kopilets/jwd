package by.training.yukopilets.model;

import by.training.yukopilets.builder.XMLRootTag;
import by.training.yukopilets.builder.XMLTag;

@XMLRootTag(name = "Devices")
@XMLTag(name = "Device")
public class Device {

    @XMLTag(name = "id", attribute = true)
    private String id;

    @XMLTag(name = "market-entry", attribute = true)
    private String marketEntry;

    @XMLTag(name = "Name")
    private String name;

    @XMLTag(name = "Origin")
    private String origin;

    @XMLTag(name = "Price")
    private int price;

    @XMLTag(name = "Type", complexType = true)
    private DeviceType deviceType;

    @XMLTag(name = "Critical")
    private boolean critical;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMarketEntry() {
        return marketEntry;
    }

    public void setMarketEntry(String marketEntry) {
        this.marketEntry = marketEntry;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public DeviceType getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(DeviceType deviceType) {
        this.deviceType = deviceType;
    }

    public boolean isCritical() {
        return critical;
    }

    public void setCritical(boolean critical) {
        this.critical = critical;
    }

    @Override
    public String toString() {
        return "Device{" +
                "id='" + id + '\'' +
                ", marketEntry='" + marketEntry + '\'' +
                ", name='" + name + '\'' +
                ", origin='" + origin + '\'' +
                ", price=" + price +
                ", deviceType=" + deviceType +
                ", critical=" + critical +
                '}';
    }
}
