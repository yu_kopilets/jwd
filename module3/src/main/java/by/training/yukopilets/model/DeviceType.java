package by.training.yukopilets.model;

import by.training.yukopilets.builder.XMLTag;

public class DeviceType {

    @XMLTag(name = "EnergyConsumption")
    private boolean energyConsumption;

    public boolean isEnergyConsumption() {
        return energyConsumption;
    }

    public void setEnergyConsumption(boolean energyConsumption) {
        this.energyConsumption = energyConsumption;
    }
}
