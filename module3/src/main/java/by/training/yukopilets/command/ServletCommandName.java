package by.training.yukopilets.command;

public enum ServletCommandName {
    UPLOAD("upload"),
    DOWNLOAD("download"),
    SHOW("show");

    private String commandName;

    ServletCommandName(String commandName) {
        this.commandName = commandName;
    }

    public static ServletCommandName fromString(String commandName) {
        for (ServletCommandName value : ServletCommandName.values()) {
            if(value.commandName.equals(commandName) || value.name().equals(commandName)) {
                return value;
            }
        }
        return SHOW;
    }
}
