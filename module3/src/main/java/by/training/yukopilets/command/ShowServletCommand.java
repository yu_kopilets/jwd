package by.training.yukopilets.command;

import by.training.yukopilets.model.Device;
import by.training.yukopilets.service.SelectDeviceService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class ShowServletCommand implements ServletCommand {

    private final SelectDeviceService deviceService;

    public ShowServletCommand(SelectDeviceService deviceService) {
        this.deviceService = deviceService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        List<Device> devices = deviceService.getAllDevices();

        req.setAttribute("message", "Input xml file's path");
        req.setAttribute("devices", devices);
        req.getRequestDispatcher("/jsp/index.jsp").forward(req, resp);
    }
}
