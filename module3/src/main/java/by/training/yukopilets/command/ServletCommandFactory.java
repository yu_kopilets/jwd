package by.training.yukopilets.command;

import java.util.Map;

public class ServletCommandFactory {

    private Map<ServletCommandName, ServletCommand> commands;

    public ServletCommandFactory(Map<ServletCommandName, ServletCommand> commands) {
        this.commands = commands;
    }

    public ServletCommand createCommand(String commandName) {
        ServletCommandName servletCommandName = ServletCommandName.fromString(commandName);
        return commands.get(servletCommandName);
    }
}
