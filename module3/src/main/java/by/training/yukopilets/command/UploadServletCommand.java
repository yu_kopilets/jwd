package by.training.yukopilets.command;

import by.training.yukopilets.model.Device;
import by.training.yukopilets.parser.AbstractXMLParser;
import by.training.yukopilets.parser.DeviceParserFactory;
import by.training.yukopilets.parser.XMLParseException;
import by.training.yukopilets.service.LoadDeviceService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.*;
import java.util.List;

public class UploadServletCommand implements ServletCommand {

    private static final Logger LOGGER = LogManager.getLogger();
    private final LoadDeviceService deviceService;
    private final DeviceParserFactory parserFactory;

    public UploadServletCommand(LoadDeviceService deviceService, DeviceParserFactory parserFactory) {
        this.deviceService = deviceService;
        this.parserFactory = parserFactory;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        String parserName = req.getParameter("parser");
        Part part = req.getPart("uploadFile");
        String message;

        if("text/xml".equals(part.getContentType())) {
            AbstractXMLParser<Device> xmlParser = parserFactory.createDeviceParser(parserName);
            List<Device> devices;
            try {
                String filePath = getFilePath(part);
                devices = xmlParser.parse(filePath).getParseResults();
                deviceService.loadDevices(devices);
                message = "Devices added!";
            } catch (XMLParseException | FileReadingException e) {
                LOGGER.warn(e.getMessage(), e);
                message = e.getMessage();
            }
        } else {
            message = "File doesn't have XML format!";
        }

        req.setAttribute("message", message);
        req.setAttribute("devices", "");
        req.getRequestDispatcher("/jsp/index.jsp").forward(req, resp);
    }

    private String getFilePath(Part part) throws FileReadingException {
        try {
            File file = File.createTempFile("temp", part.getSubmittedFileName().substring(part.getSubmittedFileName().lastIndexOf('.')));
            OutputStream outputStream = new FileOutputStream(file);
            InputStream is = part.getInputStream();

            int read;
            byte[] bytes = new byte[1024];
            while ((read = is.read(bytes)) != -1) {
                outputStream.write(bytes, 0, read);
            }
            return file.getPath();
        } catch (IOException e) {
            LOGGER.warn("Failed reading (temp file hasn't been written)", e);
            throw new FileReadingException("Failed upload!");
        }
    }
}
