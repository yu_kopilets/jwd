package by.training.yukopilets.command;

public class FileReadingException extends Exception {
    public FileReadingException() {
    }

    public FileReadingException(String message) {
        super(message);
    }
}
