package by.training.yukopilets.command;

import by.training.yukopilets.builder.XMLStringBuildException;
import by.training.yukopilets.builder.XMLStringBuilder;
import by.training.yukopilets.model.Device;
import by.training.yukopilets.service.SelectDeviceService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class DownloadServletCommand implements ServletCommand {

    private static final Logger LOGGER = LogManager.getLogger();
    private final SelectDeviceService deviceService;

    public DownloadServletCommand(SelectDeviceService deviceService) {
        this.deviceService = deviceService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        try {
            XMLStringBuilder builder = new XMLStringBuilder();
            List<Device> devices = deviceService.getAllDevices();
            String xmlString = builder.build(Device.class, devices);

            resp.setContentType("text/xml");
            resp.setHeader("Content-Disposition", "attachment; filename=\"sample\".xml");
            resp.getWriter().write(xmlString);
        } catch (XMLStringBuildException e) {
            LOGGER.warn(e.getMessage(), e);
        }
    }
}
