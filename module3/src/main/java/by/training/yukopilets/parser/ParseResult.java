package by.training.yukopilets.parser;

import java.util.List;

public class ParseResult<T> {

    private List<T> parseResults;

    public ParseResult(List<T> parseResults) {
        this.parseResults = parseResults;
    }

    public List<T> getParseResults() {
        return parseResults;
    }
}