package by.training.yukopilets.parser;

import by.training.yukopilets.builder.EntityBuilder;
import by.training.yukopilets.model.*;
import by.training.yukopilets.validator.XMLValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class DeviceStAXParser extends AbstractXMLParser<Device> {

    private static final Logger LOGGER = LogManager.getLogger();
    private XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
    private Device device;
    private DeviceType deviceType;

    public DeviceStAXParser(XMLValidator validator, EntityBuilder<Device> builder) {
        super(validator, builder);
    }

    @Override
    protected ParseResult<Device> doParse(String filePath) {
        XMLStreamReader reader;
        String name;

        try {
            InputStream is = new FileInputStream(filePath);
            reader = xmlInputFactory.createXMLStreamReader(is);

            while (reader.hasNext()) {
                if (reader.next() == XMLStreamConstants.START_ELEMENT) {
                    name = reader.getLocalName();
                    if (DevicesEnum.valueOf(name.toUpperCase()) == DevicesEnum.DEVICE) {
                        Device newDevice = buildDevice(reader);
                        builder.build(newDevice);
                    }
                }
            }
        } catch (XMLStreamException e) {
            LOGGER.warn("Exception to xml reading (StAX parsing)!", e);
        } catch (DevicesEnumNotFoundException e) {
            LOGGER.warn("StAX parsing exception!", e);
        } catch (FileNotFoundException e) {
            LOGGER.warn(e.getMessage(), e);
        }
        parseResult = new ParseResult<>(builder.getBuildResult());
        return getParseResult();
    }

    private Device buildDevice(XMLStreamReader reader) throws XMLStreamException, DevicesEnumNotFoundException {
        device = new Device();
        device.setId(reader.getAttributeValue(null, DevicesEnum.ID.getTagName()));
        device.setMarketEntry(reader.getAttributeValue(null, DevicesEnum.MARKET_ENTRY.getTagName()));

        deviceType = null;
        while (reader.hasNext()) {
            int i = reader.next();
            switch (i) {
                case XMLStreamConstants.START_ELEMENT:
                    setFieldByElement(reader);
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    String tagName = reader.getLocalName();
                    if (DevicesEnum.fromString(tagName) == DevicesEnum.DEVICE) {
                        device.setDeviceType(deviceType);
                        return device;
                    }
                    break;
            }
        }
        return device;
    }

    private void setFieldByElement(XMLStreamReader reader) throws DevicesEnumNotFoundException, XMLStreamException {

        String tagName = reader.getLocalName();
        switch (DevicesEnum.fromString(tagName)) {
            case NAME:
                device.setName(getTextContent(reader));
                break;
            case ORIGIN:
                device.setOrigin(getTextContent(reader));
                break;
            case PRICE:
                device.setPrice(Integer.parseInt(getTextContent(reader)));
                break;
            case INTERNAL:
                deviceType = new InternalDevice();
                break;
            case PERIPHERAL:
                deviceType = new PeripheralDevice();
                break;
            case ENERGY_CONSUMPTION:
                deviceType.setEnergyConsumption(Boolean.parseBoolean(getTextContent(reader)));
                break;
            case COOLER:
                ((InternalDevice) deviceType).setCooler(Boolean.parseBoolean(getTextContent(reader)));
                break;
            case DEVICE_GROUP:
                ((PeripheralDevice) deviceType).setDeviceGroup(getTextContent(reader));
                break;
            case PORT:
                ((PeripheralDevice) deviceType).setPort(getTextContent(reader));
                break;
            case CRITICAL:
                device.setCritical(Boolean.parseBoolean(getTextContent(reader)));
                break;
        }
    }

    private String getTextContent(XMLStreamReader reader) throws XMLStreamException {
        String text = null;
        if (reader.hasNext()) {
            reader.next();
            text = reader.getText();
        }
        return text;
    }
}
