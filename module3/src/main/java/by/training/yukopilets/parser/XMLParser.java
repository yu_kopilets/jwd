package by.training.yukopilets.parser;

public interface XMLParser<T> {

    ParseResult<T> parse(String filePath) throws XMLParseException;
}
