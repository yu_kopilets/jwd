package by.training.yukopilets.parser;

import by.training.yukopilets.builder.EntityBuilder;
import by.training.yukopilets.model.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class DeviceHandler extends DefaultHandler {

    private static final Logger LOGGER = LogManager.getLogger();
    private final EntityBuilder<Device> builder;
    private Device device;
    private DeviceType deviceType;
    private DevicesEnum devicesEnum;

    public DeviceHandler(EntityBuilder<Device> builder) {
        this.builder = builder;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (DevicesEnum.DEVICE.getTagName().equals(localName)) {
            device = new Device();
            device.setId(attributes.getValue(0));
            if (attributes.getLength() > 1) {
                device.setMarketEntry(attributes.getValue(1));
            } else {
                device.setMarketEntry("");
            }
        } else {
            try {
                devicesEnum = DevicesEnum.fromString(localName);
            } catch (DevicesEnumNotFoundException e) {
                LOGGER.warn("Tag from this name doesn't exist", e);
            }
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (DevicesEnum.DEVICE.getTagName().equals(localName)) {
            device.setDeviceType(deviceType);
            builder.build(device);
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        String textContent = new String(ch, start, length).trim();
        if(devicesEnum != null) {
            switch (devicesEnum) {
                case NAME:
                    device.setName(textContent);
                    break;
                case ORIGIN:
                    device.setOrigin(textContent);
                    break;
                case PRICE:
                    device.setPrice(Integer.parseInt(textContent));
                    break;
                case INTERNAL:
                    deviceType = new InternalDevice();
                    break;
                case PERIPHERAL:
                    deviceType = new PeripheralDevice();
                    break;
                case ENERGY_CONSUMPTION:
                    deviceType.setEnergyConsumption(Boolean.parseBoolean(textContent));
                    break;
                case COOLER:
                    ((InternalDevice) deviceType).setCooler(Boolean.parseBoolean(textContent));
                    break;
                case DEVICE_GROUP:
                    ((PeripheralDevice) deviceType).setDeviceGroup(textContent);
                    break;
                case PORT:
                    ((PeripheralDevice) deviceType).setPort(textContent);
                    break;
                case CRITICAL:
                    device.setCritical(Boolean.parseBoolean(textContent));
                    break;
            }
        }
        devicesEnum = null;
    }
}
