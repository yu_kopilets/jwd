package by.training.yukopilets.parser;

import by.training.yukopilets.model.Device;

import java.util.Map;

public class DeviceParserFactory {

    private Map<DeviceParserName, AbstractXMLParser<Device>> parsers;

    public DeviceParserFactory(Map<DeviceParserName, AbstractXMLParser<Device>> parsers) {
        this.parsers = parsers;
    }

    public AbstractXMLParser<Device> createDeviceParser(String parserName) {
        DeviceParserName deviceParserName = DeviceParserName.fromString(parserName);
        return parsers.get(deviceParserName);
    }
}
