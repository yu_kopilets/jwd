package by.training.yukopilets.parser;

public class XMLParseException extends Exception {
    public XMLParseException() {
    }

    public XMLParseException(String message) {
        super(message);
    }
}
