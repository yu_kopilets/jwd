package by.training.yukopilets.parser;

import by.training.yukopilets.builder.EntityBuilder;
import by.training.yukopilets.validator.XMLValidationException;
import by.training.yukopilets.validator.XMLValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public abstract class AbstractXMLParser<T> implements XMLParser<T>{

    private static final Logger LOGGER = LogManager.getLogger();
    private final XMLValidator validator;
    protected final EntityBuilder<T> builder;
    protected ParseResult<T> parseResult;

    public AbstractXMLParser(XMLValidator validator, EntityBuilder<T> builder) {
        this.validator = validator;
        this.builder = builder;
    }

    protected ParseResult<T> getParseResult() {
        return parseResult;
    }

    @Override
    public ParseResult<T> parse(String filePath) throws XMLParseException {
        try {
            validator.validate(filePath);
            return doParse(filePath);
        } catch (XMLValidationException e) {
            LOGGER.warn(e.getMessage(), e);
            throw new XMLParseException(e.getMessage());
        }
    }

    protected abstract ParseResult<T> doParse(String filePath);
}
