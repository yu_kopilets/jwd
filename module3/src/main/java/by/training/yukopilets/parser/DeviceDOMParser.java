package by.training.yukopilets.parser;

import by.training.yukopilets.builder.EntityBuilder;
import by.training.yukopilets.model.Device;
import by.training.yukopilets.model.DevicesEnum;
import by.training.yukopilets.model.InternalDevice;
import by.training.yukopilets.model.PeripheralDevice;
import by.training.yukopilets.validator.XMLValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

public class DeviceDOMParser extends AbstractXMLParser<Device> {

    private static final Logger LOGGER = LogManager.getLogger();
    private DocumentBuilder documentBuilder;

    public DeviceDOMParser(XMLValidator validator, EntityBuilder<Device> builder) {
        super(validator, builder);
        try {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            documentBuilder = documentBuilderFactory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            LOGGER.warn("DocumentBuilder factory exception!", e);
        }
    }

    @Override
    protected ParseResult<Device> doParse(String filePath) {
        try {
            Document document = documentBuilder.parse(filePath);
            Element rootElement = document.getDocumentElement();
            NodeList deviceList = rootElement.getElementsByTagName("Device");
            for (int i = 0; i < deviceList.getLength(); i++) {
                Element element = (Element) deviceList.item(i);
                Device device = buildDevice(element);
                builder.build(device);
            }
        } catch (SAXException e) {
            LOGGER.warn("DOM parsing exception!", e);
        } catch (IOException e) {
            LOGGER.warn("Input/output exception (DOM parsing)!", e);
        }
        parseResult = new ParseResult<>(builder.getBuildResult());
        return getParseResult();
    }

    private Device buildDevice(Element element) {

        Device device = new Device();
        device.setId(element.getAttribute(DevicesEnum.ID.getTagName()));
        device.setMarketEntry(element.getAttribute(DevicesEnum.MARKET_ENTRY.getTagName()));
        device.setName(getTextContent(element, DevicesEnum.NAME.getTagName()));
        device.setOrigin(getTextContent(element, DevicesEnum.ORIGIN.getTagName()));
        device.setPrice(Integer.parseInt(getTextContent(element, DevicesEnum.PRICE.getTagName())));

        if(element.getElementsByTagName(DevicesEnum.INTERNAL.getTagName()).item(0) != null) {

            InternalDevice internalDevice = new InternalDevice();
            internalDevice.setEnergyConsumption(Boolean.parseBoolean(getTextContent(element, DevicesEnum.ENERGY_CONSUMPTION.getTagName())));
            device.setDeviceType(internalDevice);
            internalDevice.setCooler(Boolean.parseBoolean(getTextContent(element, DevicesEnum.COOLER.getTagName())));
        } else if(element.getElementsByTagName("Peripheral").item(0) != null) {

            PeripheralDevice peripheralDevice = new PeripheralDevice();
            peripheralDevice.setEnergyConsumption(Boolean.parseBoolean(getTextContent(element, DevicesEnum.ENERGY_CONSUMPTION.getTagName())));
            peripheralDevice.setDeviceGroup(getTextContent(element, DevicesEnum.DEVICE_GROUP.getTagName()));
            peripheralDevice.setPort(getTextContent(element, DevicesEnum.PORT.getTagName()));
            device.setDeviceType(peripheralDevice);
        }

        device.setCritical(Boolean.parseBoolean(getTextContent(element, DevicesEnum.CRITICAL.getTagName())));
        return device;
    }

    private static String getTextContent(Element element, String elementName) {
        Node node = element.getElementsByTagName(elementName).item(0);
        return node.getTextContent();
    }
}
