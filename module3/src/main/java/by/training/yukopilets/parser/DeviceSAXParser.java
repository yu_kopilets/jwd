package by.training.yukopilets.parser;

import by.training.yukopilets.builder.EntityBuilder;
import by.training.yukopilets.model.Device;
import by.training.yukopilets.validator.XMLValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import java.io.IOException;

public class DeviceSAXParser extends AbstractXMLParser<Device> {

    private static final Logger LOGGER = LogManager.getLogger();
    private XMLReader xmlReader;

    public DeviceSAXParser(XMLValidator validator, EntityBuilder<Device> builder) {
        super(validator, builder);
        DeviceHandler deviceHandler = new DeviceHandler(builder);
        try {
            xmlReader = XMLReaderFactory.createXMLReader();
            xmlReader.setContentHandler(deviceHandler);
        } catch (SAXException e) {
            LOGGER.warn("SAX parsing. Exception to create xmlReader!", e);
        }
    }

    @Override
    protected ParseResult<Device> doParse(String filePath) {
        try {
            xmlReader.parse(filePath);
        } catch (IOException e) {
            LOGGER.warn("Input/output exception (SAX parsing)!", e);
        } catch (SAXException e) {
            LOGGER.warn("SAX parsing exception!", e);
        }
        parseResult = new ParseResult<>(builder.getBuildResult());
        return getParseResult();
    }
}
