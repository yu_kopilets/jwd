package by.training.yukopilets.parser;

public enum DeviceParserName {
    SAX("sax"),
    DOM("dom"),
    STAX("stax");

    private String parserName;

    DeviceParserName(String parserName) {
        this.parserName = parserName;
    }
    
    public static DeviceParserName fromString(String parserName) {
        for (DeviceParserName value : DeviceParserName.values()) {
            if(value.parserName.equals(parserName) || value.name().equals(parserName)) {
                return value;
            }
        }
        return SAX;
    }
}
