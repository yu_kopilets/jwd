package by.training.yukopilets.validator;

public class XMLValidationException extends Exception {
    public XMLValidationException() {
    }

    public XMLValidationException(String message) {
        super(message);
    }
}
