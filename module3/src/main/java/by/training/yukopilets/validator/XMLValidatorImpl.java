package by.training.yukopilets.validator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;

public class XMLValidatorImpl implements XMLValidator {

    private static final Logger LOGGER = LogManager.getLogger();
    private final Schema schema;

    public XMLValidatorImpl(String schemaLanguage, String schemaLocation) throws SAXException {
        SchemaFactory factory = SchemaFactory.newInstance(schemaLanguage);
        File xsdSchema = new File(schemaLocation);
        schema = factory.newSchema(xsdSchema);
    }

    public void validate(String filePath) throws XMLValidationException {

        Validator validator = schema.newValidator();
        Source source = new StreamSource(filePath);
        try {
            validator.validate(source);
        } catch (SAXException e) {
            LOGGER.warn(e.getMessage(), e);
            throw new XMLValidationException("File doesn't match XSD schema!");
        } catch (IOException e) {
            LOGGER.warn(e.getMessage(), e);
            throw new XMLValidationException("Stream reading exception");
        }
    }
}
