package by.training.yukopilets.validator;

public interface XMLValidator {

    void validate(String filePath) throws XMLValidationException;
}
