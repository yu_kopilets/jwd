package by.training.yukopilets.dao;

import by.training.yukopilets.model.Device;

import java.util.List;

public class InMemoryDeviceDao implements DeviceDao {

    private InMemoryDeviceList inMemoryDeviceList = InMemoryDeviceList.getInstance();

    @Override
    public void update(List<Device> devices) {
        if(!devices.isEmpty()) {
            devices.addAll(inMemoryDeviceList.getAllDevices());
            inMemoryDeviceList.setList(devices);
        }
    }

    @Override
    public List<Device> getAllDevices() {
        return InMemoryDeviceList.getInstance().getAllDevices();
    }
}
