package by.training.yukopilets.dao;

import by.training.yukopilets.model.Device;

import java.util.List;

public interface DeviceDao {

    void update(List<Device> devices);
    List<Device> getAllDevices();
}
