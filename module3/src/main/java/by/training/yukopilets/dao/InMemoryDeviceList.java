package by.training.yukopilets.dao;

import by.training.yukopilets.model.Device;

import java.util.ArrayList;
import java.util.List;

public class InMemoryDeviceList {

    private static InMemoryDeviceList instance;
    private List<Device> devices;

    private InMemoryDeviceList() {
        devices = new ArrayList<>();
    }

    public void setList(List<Device> devices) {
        this.devices = devices;
    }

    public List<Device> getAllDevices() {
        return devices;
    }

    public static InMemoryDeviceList getInstance() {
        if(instance == null) {
            instance = new InMemoryDeviceList();
        }
        return instance;
    }
}
