package by.training.yukopilets.builder;

import java.lang.annotation.*;

@Target(value = { ElementType.TYPE, ElementType.FIELD })
@Retention(value = RetentionPolicy.RUNTIME)
public @interface XMLTag {
    String name();
    boolean attribute() default false;
    boolean complexType() default false;
}
