package by.training.yukopilets.builder;

import by.training.yukopilets.model.Device;

import java.util.ArrayList;
import java.util.List;

public class DeviceBuilder implements EntityBuilder<Device> {

    private final List<Device> devices;

    public DeviceBuilder() {
        devices = new ArrayList<>();
    }

    public DeviceBuilder(List<Device> devices) {
        this.devices = devices;
    }

    public void build(Device device) {
        devices.add(device);
    }

    public List<Device> getBuildResult() {
        return devices;
    }
}
