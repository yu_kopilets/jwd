package by.training.yukopilets.builder;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;
import java.text.MessageFormat;
import java.util.List;

public class XMLStringBuilder {

    private static final Logger LOGGER = LogManager.getLogger();
    private static final String ELEMENT = "<{0}>{1}</{0}>\n";
    private static final String TAG_OPEN = "<{0}>\n";
    private static final String TAG_CLOSE = "</{0}>\n";
    private static final String ATTRIBUTES = " {0}=\"{1}\"";

    public <T> String build(Class<T> clazz, List<T> elements) throws XMLStringBuildException {

        StringBuilder xmlString = new StringBuilder("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n");

        try {
            xmlString.append(MessageFormat.format(TAG_OPEN, clazz.getAnnotation(XMLRootTag.class).name())).append("\n");
            for (T element : elements) {
                xmlString.append(buildComplexElementXML(element.getClass(), element)).append("\n");
            }

            xmlString.append(MessageFormat.format(TAG_CLOSE, clazz.getAnnotation(XMLRootTag.class).name()));
            return xmlString.toString();

        } catch (IllegalAccessException e) {
            LOGGER.warn(e.getMessage(), e);
            throw new XMLStringBuildException("Field's Illegal Access");
        }
    }

    private String buildComplexElementXML(Class<?> clazz, Object element) throws IllegalAccessException {

        StringBuilder attributes = new StringBuilder();
        StringBuilder elementValue = new StringBuilder();
        Field[] fields = element.getClass().getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(XMLTag.class)) {
                XMLTag xmlTag = field.getAnnotation(XMLTag.class);

                field.setAccessible(true);
                if (xmlTag.attribute()) {
                    attributes.append(MessageFormat.format(ATTRIBUTES, xmlTag.name(), field.get(element)));
                } else if (xmlTag.complexType()) {
                    elementValue.append(buildComplexElementXML(field.get(element).getClass(), field.get(element)));
                } else {
                    elementValue.append(MessageFormat.format(ELEMENT, xmlTag.name(), field.get(element)));
                }
            }
        }
        StringBuilder xmlString = new StringBuilder();
        xmlString.append(MessageFormat.format(TAG_OPEN, clazz.getAnnotation(XMLTag.class).name() + attributes.toString()));

        Class sClazz = clazz.getSuperclass();
        while (sClazz != Object.class) {
            for (Field declaredField : sClazz.getDeclaredFields()) {
                if (declaredField.isAnnotationPresent(XMLTag.class)) {
                    XMLTag sXmlTag = declaredField.getAnnotation(XMLTag.class);
                    declaredField.setAccessible(true);
                    xmlString.append(MessageFormat.format(ELEMENT, sXmlTag.name(), declaredField.get(element)));
                }
            }
            sClazz = sClazz.getSuperclass();
        }

        xmlString.append(elementValue.toString());
        xmlString.append(MessageFormat.format(TAG_CLOSE, clazz.getAnnotation(XMLTag.class).name()));
        return xmlString.toString();
    }
}
