package by.training.yukopilets.builder;

import java.util.List;

public interface EntityBuilder<T> {

    void build(T item);
    List<T> getBuildResult();
}
