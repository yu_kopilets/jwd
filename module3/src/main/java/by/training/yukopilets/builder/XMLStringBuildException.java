package by.training.yukopilets.builder;

public class XMLStringBuildException extends Exception {
    public XMLStringBuildException() {
    }

    public XMLStringBuildException(String message) {
        super(message);
    }
}
