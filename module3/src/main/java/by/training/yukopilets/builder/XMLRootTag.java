package by.training.yukopilets.builder;

import java.lang.annotation.*;

@Target(value = ElementType.TYPE)
@Retention(value = RetentionPolicy.RUNTIME)
public @interface XMLRootTag {
    String name();
}
