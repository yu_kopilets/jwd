package by.training.yukopilets.controller;

import by.training.yukopilets.ApplicationContext;
import by.training.yukopilets.command.ServletCommand;
import by.training.yukopilets.command.ServletCommandFactory;
import by.training.yukopilets.command.ServletCommandName;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@WebServlet(urlPatterns = "/*")
@MultipartConfig(maxFileSize = 2_000_000, maxRequestSize = 2_000_000)
public class IndexController extends HttpServlet {

    private static final Logger LOGGER = LogManager.getLogger();
    private ServletCommandFactory commandFactory;

    @Override
    public void init() throws ServletException {
        commandFactory = (ServletCommandFactory) ApplicationContext.getInstance().getBean(ServletCommandFactory.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        LOGGER.info("Query: " + req.getQueryString() + "; SID: " + req.getSession().getId());

        String commandName;
        Map<String, String[]> params = req.getParameterMap();

        if(params.size() != 0) {
            if(params.get("commandName").length == 1) {
                commandName = req.getParameter("commandName");
            } else {
                commandName = ServletCommandName.UPLOAD.name();
            }
            ServletCommand command = commandFactory.createCommand(commandName);
            command.execute(req, resp);

        } else {
            req.setAttribute("message", "Input xml file's path");
            req.setAttribute("devices", "");
            req.getRequestDispatcher("/jsp/index.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}


