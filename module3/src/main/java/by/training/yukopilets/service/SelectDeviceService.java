package by.training.yukopilets.service;

import by.training.yukopilets.dao.InMemoryDeviceDao;
import by.training.yukopilets.model.Device;

import java.util.List;

public class SelectDeviceService {

    private final InMemoryDeviceDao deviceDao;

    public SelectDeviceService(InMemoryDeviceDao deviceDao) {
        this.deviceDao = deviceDao;
    }

    public List<Device> getAllDevices() {
        return deviceDao.getAllDevices();
    }
}
