package by.training.yukopilets.service;

import by.training.yukopilets.dao.InMemoryDeviceDao;
import by.training.yukopilets.model.Device;

import java.util.List;

public class LoadDeviceService {

    private final InMemoryDeviceDao deviceDao;

    public LoadDeviceService(InMemoryDeviceDao deviceDao) {
        this.deviceDao = deviceDao;
    }

    public void loadDevices(List<Device> devices) {
        deviceDao.update(devices);
    }
}
