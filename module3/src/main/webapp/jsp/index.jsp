<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>Document</title>
</head>
<body style="margin-right: 20%; margin-left: 20%; text-align: center">
    <div style="padding: 3%">
        <form method="POST" enctype="multipart/form-data">
            <input type="hidden" name="commandName" value="upload">
            <input type="file" name="uploadFile">
            <label>${message}</label>
            <p>Chose a parser:&nbsp;<input type="radio" name="parser" value="sax">&nbsp;SAX;&nbsp;<input type="radio" name="parser" value="dom">&nbsp;DOM;&nbsp;<input type="radio" id="typeRandom" name="parser" value="stax" checked="true">&nbsp;StAX</p>
            <button>Load</button>
        </form>
    </div>
    <div style="padding: 3%">
         <form method="GET">
            <input type="hidden" name="commandName" value="download">
            <button>Download</button>
         </form>
    </div>
    <div style="padding: 3%">
        <form method="GET">
            <input type="hidden" name="commandName" value="show">
            <button>Show</button>
        </form>
        <div style="padding: 3%">
             <table border=1 width=100% cellpadding=5>
                 <thead>
                 <tr>
                     <th>Name</th>
                     <th>Id</th>
                     <th>Origin</th>
                     <th>Market Entry</th>
                     <th>Price</th>
                     <th>Critical</th>
                 </tr>
                 </thead>

                 <tbody>
                 <c:forEach var="device" items="${devices}">
                     <tr>
                         <td>${device.name}</td>
                         <td>${device.id}</td>
                         <td>${device.origin}</td>
                         <td>${device.marketEntry}</td>
                         <td>${device.price}</td>
                         <td>${device.critical}</td>
                     </tr>
                 </c:forEach>
                 </tbody>
              </table>
        </div>
    </div>
</body>
</html>