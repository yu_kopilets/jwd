package decomposition;

public class Main {

    public static void main(String[] args) {

        // 1 - 10
        System.out.println(task1(new Point(1,1), new Point(1, 3), new Point(3, 1)));
        task2(12, 24);
        System.out.println(task3(12, 24, 16, 28));
        System.out.println(task4(6, 8, 10));
        System.out.println(task5(1, 2, 3));
        System.out.println(task6(3));
        task7(new Point(1, 1), new Point(2, 4), new Point(10, 10));
        task8(8, 5, 2, 9, 10, 51);
        System.out.println(task9(10, 2, 5));
        task10();

        System.out.println("\n\n");

        // 11 - 20
        task11(1, 6, 1, 2, 3, 4, 5, 6, 7, 8, 9);
        System.out.println(task12(3, 2, 4, 6));
        task13(123456);
        task14(1, 0);
        task15(6, 100);
        task16(10);
        task17(1000);
        task18(4);
        task19(4);
        task20(200);
    }

    // 1 - 10
    private static double task1(Point a, Point b, Point c) {

        double ab = Point.distanceBetweenPoints(a, b);
        double bc = Point.distanceBetweenPoints(b, c);
        double ac = Point.distanceBetweenPoints(a, c);
        double semiper = (ab + bc + ac) / 2;

        return Math.sqrt(semiper*(semiper - ab)*(semiper - bc)*(semiper - ac));
    }

    private static int gcd(int a,int b){
        return b == 0 ? a : gcd(b,a % b);
    }

    private static int lcm(int a,int b){
        return a / gcd(a,b) * b;
    }

    private static void task2(int a, int b) {
        System.out.println("НОД - " + gcd(a, b));
        System.out.println("НОК - " + lcm(a, b));
    }

    private static int task3(int a, int b, int c, int d) {
        int buf1 = gcd(a, b);
        int buf2 = gcd(buf1, c);
        int res = gcd(buf2, d);
        return res;
    }

    private static int task4(int a, int b, int c) {
        int buf = lcm(a, b);
        return lcm(buf, c);
    }

    private static int min(int a, int b, int c) {

        if(a < b && a < c) {
            return a;
        } else if(b < a && b < c) {
            return b;
        } else {
            return c;
        }
    }

    private static int max(int a, int b, int c) {

        if(a > b && a > c) {
            return a;
        } else if(b > a && b > c) {
            return b;
        } else {
            return c;
        }
    }

    private static int task5(int a, int b, int c) {
        return min(a, b, c) + max(a, b, c);
    }

    private static double triangleArea(double n) {
        return n * n * Math.sqrt(3) / 4;
    }

    private static double task6(double n) {
        return 6 * triangleArea(n);
    }

    private static void task7(Point ... points) {

        double distance = 0;
        Point pointA = null;
        Point pointB = null;

        for (int i = 0; i < points.length - 1; i++) {
            for (int j = 1; j < points.length; j++) {

                double count = points[i].distanceBetweenPoints(points[j]);
                if(count > distance) {
                    distance = count;
                    pointA = points[i];
                    pointB = points[j];
                }
            }
        }

        if(pointA != null && pointB != null) {
            System.out.println(pointA.toString() + "\n" + pointB.toString());
        }
    }

    private static void task8(int ... values) {

        for (int i = 0; i < values.length - 1; i++) {
            for (int j = 1; j < values.length; j++) {

                if(values[i] < values[j]) {
                    int buf = values[i];
                    values[i] = values[j];
                    values[j] = buf;
                }
            }
        }

        if(values.length > 1) {
            System.out.println("Второй по величине элемент - " + values[1]);
        }
    }

    private static boolean task9(int a, int b, int c) {

        int buf1 = gcd(a, b);
        int buf2 = gcd(b, c);
        int buf3 = gcd(a, c);

        if(buf1 == 1 && buf2 == 1 && buf3 == 1) {
            return true;
        }
        return false;
    }

    private static long factorial(long num) {
        if(num == 1) {
            return 1;
        } else {
            return num * factorial(num - 1);
        }
    }

    private static void task10() {

        int sum = 0;
        for (int i = 1; i <= 9; i += 2) {
            sum += factorial(i);
        }
        System.out.println("Сумма = " + sum);
    }

    // 11 - 20

    private static void task11(int k, int m, int ... values) {
        
        if(m > values.length + 1 || k > m - 3) return;

        for (int i = k; i < m; i++) {
            int sum = values[i] + values[i + 1] + values[i + 2];
            System.out.print(sum + "\t");
        }
    }

    private static double task12(double x, double y, double z, double t) {
        return t*x + x*(y - t) / 2;
    }

    private static void task13(int n) {

        int count = 0;
        for (int i = n; i / 10. != 0; i /= 10) {
            count++;
        }

        int[] mas = new int[count];

        for (int i = n, j = 0; i / 10. != 0; i /= 10, j++) {
            mas[j] = i % 10;
            System.out.print(mas[j] + "\t");
        }
        System.out.println();
    }

    private static int countDigits(int num) {

        int count;
        for (count = 1; num / 10. != 0; count++) {
            num /= 10;
        }
        return count;
    }

    private static void task14(int num1, int num2) {

        if (countDigits(num1) > countDigits(num2)) {
            System.out.println("В числе номер 1");
        } else if(countDigits(num1) < countDigits(num2)){
            System.out.println("В числе номер 2");
        } else {
            System.out.println("Одинаково");
        }
    }

    private static void task15(int k, int n) {
        
        int len = 0;
        for (int i = 0; i < n; i++) {

            int sum = 0;
            for (int j = i; j / 10. != 0 ; j /= 10) {
                sum += j % 10;
            }

            if(sum == k) {
                len++;
            }
        }

        if(len > 0) {
            int[] mas = new int[len];

            for (int i = 0, e = 0; i < n; i++) {

                int sum = 0;
                for (int j = i; j / 10. != 0 ; j /= 10) {
                    sum += j % 10;
                }

                if(sum == k) {
                    mas[e] = i;
                    System.out.print(mas[e] + "\t");
                    e++;
                }
            }
            System.out.println();
        }
    }

    private static void printNumbers(int a, int b) {
        System.out.print("Числа близнецы - " + a + " и " + b);
    }

    private static void task16(int n) {
        for (int i = n; i <= 2*n - 2; i++) {
            printNumbers(i, i + 2);
        }
    }

    private static boolean isArmstrongNumber(int num) {

        int sum = 0;
        int count = 0;
        for (int i = num; i / 10. != 0; i /= 10) {
            count++;
        }

        for (int i = num; i / 10. != 0; i /= 10) {
            sum += Math.pow(i % 10, count);
        }

        if(sum == num) {
            return true;
        }
        return false;
    }

    private static void task17(int k) {

        for (int i = 1; i <= k; i++) {
            if(isArmstrongNumber(i)) {
                System.out.print("Число " + i + " - число Армстронга!\t");
            }
        }
        System.out.println();
    }

    private static boolean isSequence(int num) {

        int prev = 10;
        for (int i = num; i / 10. != 0; i /= 10) {
            if(i % 10 < prev) {
                prev = i % 10;
            } else {
                return false;
            }
        }
        return true;
    }

    private static void task18(int n) {

        int topNumbers = (int)Math.pow(10, n) - 1;
        int botNumbers = (int)Math.pow(10, n-1);

        for (int i = botNumbers; i <= topNumbers; i++) {
            if(isSequence(i)) {
                System.out.print(i + "\t");
            }
        }
        System.out.println();
    }

    private static boolean isOdd(int num) {

        for (int i = num; i / 10. != 0; i /= 10) {
            if(i % 10 == 0) {
                return false;
            }
        }
        return true;
    }

    private static int countEvenDigits(int num) {

        int count = 0;
        for (int i = num; i / 10. != 0; i /= 10) {
            if(i % 10 == 1) {
                count++;
            }
        }
        return count;
    }

    private static void task19(int n) {

        int topNumbers = (int)Math.pow(10, n) - 1;
        int botNumbers = (int)Math.pow(10, n-1);
        int sum = 0;

        for (int i = botNumbers; i <= topNumbers; i++) {
            if(isOdd(i)) {
                sum += i;
            }
        }
        System.out.println("Число - " + sum + " , четных цифр в сумме цифр этого числа - " + countEvenDigits(sum));
    }

    private static int sumOfDigits(int num) {
        int sum = 0;
        for (int i = num; i / 10. != 0; i /= 10) {
            sum += i % 10;
        }
        return sum;
    }

    private static void task20(int num) {

        int count = 0;
        while (num > 0) {
            num -= sumOfDigits(num);
            count++;
        }
        System.out.println("Число операций - " + count);
    }
}
