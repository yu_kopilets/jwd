package calculator;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        System.out.println("Это консольный калькулятор, вводи выражения и погнали!");
        String expression;
        Scanner scanner = new Scanner(System.in);

        while(true) {

            System.out.print("\nВыражение: ");
            expression = scanner.nextLine();
            if("quit".equals(expression)) {
                System.out.println("Завершение программы!");
                break;
            }

            int checkForSpaces = 0;
            for (int i = 0; i < expression.length(); i++) {
                if(expression.charAt(i) == ' ') { checkForSpaces++; }
            }

            if(checkForSpaces != 2 || expression.indexOf(' ') != expression.lastIndexOf(' ') - 2) {
                System.out.println("\nВыражение должно состоять из двух числовых частей и арифметической операции между ними (например: 123 + 543)! Повторите ввод или введите \"quit\":");
                continue;
            }

            double num1;
            double num2;
            try {
                num1 = Double.parseDouble(expression.substring(0, expression.indexOf(' ')));
                num2 = Double.parseDouble(expression.substring(expression.lastIndexOf(' ')));
            } catch (NumberFormatException e) {
                System.out.println("\nНеправильный формат числа!");
                System.out.println("Выражение должно состоять из двух числовых частей и арифметической операции между ними (например: 123 + 543)! Повторите ввод или введите \"quit\":");
                continue;
            }

            char symbol = expression.charAt(expression.indexOf(' ') + 1);

            switch (symbol) {
                case '+':
                    System.out.println("Результат = " + (num1 + num2));
                    break;
                case '-':
                    System.out.println("Результат = " + (num1 - num2));
                    break;
                case '*':
                    System.out.println("Результат = " + (num1 * num2));
                    break;
                case '/':
                    System.out.println("Результат = " + (num1 / num2));
                    break;
                default:
                    System.out.println("\nНеправильный формат операции!");
                    System.out.println("Выражение должно состоять из двух числовых частей и арифметической операции между ними (например: 123 + 543)! Повторите ввод или введите \"quit\":");
                    break;
            }
        }
        scanner.close();
    }
}
