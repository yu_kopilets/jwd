package basics.linear;

public class Main {

    public static void main(String[] args) {

        // 1 - 10
        task1(1, 2);
        System.out.println("c = " + task2(3.5));
        System.out.println("z = " + task3(2.4, 5.6));
        System.out.println("z = " + task4(2.4, 5.6, 3));
        System.out.println("Среднее арифметическое = " + task5(4, 7));
        System.out.println(task6(12, 14));
        System.out.println(task7(6));
        System.out.println(task8(6, 5 , 4.4));
        System.out.println(task9(6, 7.8, 3, 6.3));
        System.out.println(task10(1, 3));

        System.out.println("\n\n");

        // 11 - 20
        task11(3, 4);
        System.out.println(task12(1, 2, 2, 2));
        task13(1, 2, 2, 2, 2 ,0);
        task14(3);
        task15();
        System.out.println(task16(1234));
        task17(12, 34);
        task18(2);
        task19(3);
        System.out.println(task20(10));

        System.out.println("\n\n");

        // 21 - 30
        System.out.println(task21(123.456));
        task22(312541532523L);
        System.out.println(task23(1, 3));
        System.out.println(task24(4, 2, 30));
        task25(1, -5, 6);
        task26(2, 4, 60);
        task27(2);
        task28(523);
        task29(3,4,5);
        System.out.println(task30(1, 2, 3));

        System.out.println("\n\n");

        // 31 - 40
        task31(5, 2, 1, 2);
        task32(2, 12, 0, 1, 2, 20);
        task33('b');
        task35(122, 52622);
        System.out.println(task36(3334));
        System.out.println(task37(198));
    }

    //1 - 10

    private static void task1(double x, double y) {
        System.out.println("Сумма = " + (x+y) + ", Разность = " + (x-y) + ", Произведение = " + (x*y) + ", Частное = " + (x/y));
    }

    private static double task2(double a) {
        return 3 + a;
    }

    private static double task3(double x, double y) {
        return 2 * x + (y - 2) * 5;
    }

    private static double task4(double a, double b, double c) {
        return ((a - 3) * b / 2) + c;
    }

    private static double task5(double x, double y) {
        return (x + y) / 2;
    }

    private static double task6(int n, int m) {
        return m * (80. / n + 12);
    }

    private static double task7(double length) {
        return length * length / 2;
    }

    private static double task8(double a, double b, double c) {
        return (b + Math.sqrt(b*b + 4*a*c)) / 2 / a - Math.pow(a, 3) * c + Math.pow(b, -2);
    }

    private static double task9(double a, double b, double c, double d) {
        return a * b / c / d - (a * b - c) / c / d;
    }

    private static double task10(double x, double y) {
        return (Math.sin(x) + Math.cos(y)) / (Math.cos(x) - Math.sin(y)) * Math.tan(x*y);
    }

    // 11 - 20

    private static void task11(double a, double b) {
        System.out.println("Периметр = " + (a+b+Math.sqrt(a*a + b*b) + ", Площадь = " + (a*b/2)));
    }

    private static double task12(int x1, int x2, int y1, int y2) {
        return Math.sqrt(Math.pow(x2-x1, 2) + Math.pow(y2-y1, 2));
    }

    private static void task13(int x1, int x2, int x3,  int y1, int y2, int y3) {
        double a = task12(x1, x2, y1, y2);
        double b = task12(x2, x3, y2, y3);
        double c = task12(x3, x1, y3, y1);
        double p = (a + b + c) / 2;
        System.out.println("Периметр = " + (a+b+c) + ", Площадь = " + Math.sqrt(p*(p-a)*(p-b)*(p-c)));
    }

    private static void task14(double r) {
        System.out.println("Длина окружности = " + (2*Math.PI*r) + ", Площадь круга = " + (Math.PI*r*r));
    }

    private static void task15() {
        System.out.println(Math.PI + "\t" + Math.pow(Math.PI, 2) + "\t" + Math.pow(Math.PI, 3) + "\t" + Math.pow(Math.PI, 4));
    }

    private static double task16(int num) {
        return num / 1000 * (num % 1000 / 100) * (num % 100 / 10) * (num % 10);
    }

    private static void task17(double x, double y) {
        System.out.println("Среднее арифметическое кубов = " + ((Math.pow(x, 3) + Math.pow(y, 3)) / 2) + ", Среднее геометрическое модулей = " + Math.sqrt(Math.abs(x) * Math.abs(y)));
    }

    private static void task18(double a) {
        System.out.println("Площадь грани = " + a*a + ", Площадь полной поверхности = " + a*a*6 + ", Объем = " + a*a*a);
    }

    private static void task19(double a) {
        double h = a * Math.sqrt(3) / 2;
        double s = h * a / 2;
        double R = 2 * h / 3;
        double r = h / 3;
        System.out.println("Площадь = " + s + ", Высота = " + h + ", Радиус впис. = " + r + ", Радиус опис. = " + R);
    }

    private static double task20(double len) {
        return len * len / Math.PI / 4;
    }

    // 21 - 30

    private static double task21(double num) {
        double rightPart = (int)num / 1000.;
        double leftPart = (num - (int)num) * 1000;
        return Math.round((leftPart + rightPart) * 1000) / 1000.;
    }

    private static void task22(long seconds) {
        System.out.println("hours = " + seconds / 3600 + ", mins = " + seconds % 3600 / 60 + ", secs = " + seconds % 3600 % 60);
    }

    private static double task23(double r, double R) {
        return Math.PI * R * R - Math.PI * r * r;
    }

    private static double task24(double a, double b, double alfa) {
        double h = (a - b) / 2 / Math.cos(alfa);
        return (a+b) / 2 * h;
    }

    private static void task25(double a, double b, double c) {
        double d = b * b - 4 * a * c;
        double x1 = (-b - Math.sqrt(d)) / 2 / a;
        double x2 = (-b + Math.sqrt(d)) / 2 / a;
        System.out.println("Корень x1 = " + x1 + ", Корень x2 = " + x2);
    }

    private static double task26(double a, double b, double alfa) {
        return a * b / 2 * Math.sin(alfa);
    }

    private static void task27(int a) {
        int a2 = a*a; //1
        int a4 = a2 * a2; //2
        int a8 = a4 * a4; //3
        int a10 = a8 * a2; //10
        System.out.println("А в 8 = " + a8 + ", A в 10 = " + a10);
    }

    private static void task28(double rd) {
        System.out.println("Gr = " + rd * 180 / Math.PI / 3600 + ", mins = " + rd * 180 / Math.PI % 3600 / 60 + ", secs = " + rd * 180 / Math.PI % 3600 % 60);
    }

    private static void task29(double a, double b, double c) {
        double alfa = Math.cos(-(a*a-b*b-c*c)/(2*b*c));
        double betha = Math.cos(-(b*b-a*a-c*c)/(2*a*c));
        double gama = Math.acos(Math.cos(-(c*c-a*a-b*b)/(2*a*b)));
        System.out.println("Alfa = " + alfa + ", Betha = " + betha + ", Gama = " + gama);
    }

    private static double task30(double r1, double r2, double r3) {
        return r1 * r2 * r3 / (r1 * r2 + r1 * r3 + r2 * r3);
    }

    // 31 - 40

    private static void task31(double v, double v1, double t1, double t2) {
        System.out.println("По течению S = " + (v+v1) * t1 + ", Против течения S = " + (v-v1)*t2);
    }

    private static void task32(int m, int n, int k, int p, int q, int r) {
        int totalTime = m*3600 + n*60 + k + p*3600 + q*60 + r;
        System.out.println("Новое время = " + totalTime / 3600 + "h " + totalTime % 3600 / 60 + "m " + totalTime % 3600 % 60 + "s");
    }

    private static void task33(char ch) {
        System.out.println("Порядковый номер - " + (int)ch + ", Пред. символ - " + (char)(ch - 1) + ", Послед. символ - " + (char)(ch + 1));
    }

    private static void task34(int bt) {
        int kb = bt / 1024;
        int mb = kb / 1024;
        int gb = mb / 1024;
        int tb = gb / 1024;
        System.out.println("kb = " + kb + " ,mb = " + mb + " ,gb = " + gb +" ,tb = " + tb);
    }

    private static void task35(int n, int m) {
        double num = (double)m / n;
        System.out.println("Число = " + num + ", Младшая цифра целой части - " + (int)(num % 10) + ", Старшая цифра дробной части - " + (int)(num * 10 % 10));
    }

    private static double task36(int num) {

        int multOdd = 1;
        int multEven = 1;

        if(num % 10 % 2 == 0) {
            multEven *= num % 10;
        } else {
            multOdd *= num % 10;
        }

        if(num % 100 / 10 % 2 == 0) {
            multEven *= num % 100 / 10;
        } else {
            multOdd *= num % 100 / 10;
        }

        if(num % 1000 / 100 % 2 == 0) {
            multEven *= num % 1000 / 100;
        } else {
            multOdd *= num % 1000 / 100;
        }

        if(num / 1000 % 2 == 0) {
            multEven *= num / 1000;
        } else {
            multOdd *= num / 1000;
        }

        return (double)multEven / multOdd;
    }

    private static boolean task37(int n) {
        if(n / 100 != 0 && n % 2 == 0) {
            return true;
        }
        return false;
    }

    private static void task38() {
    }

    private static double task39(double x) {
        return 2*x*x*(x-2) - x*(3*x*x + 5) + 6;
    }

    private static void task40(double x) {
        double res1 = x*(3*x - 4*x*x - 2);
        double res2 = x*(4*x*x + 3*x + 2) + 1;
        System.out.println("Res1 = " + res1 + ", Res2 = " + res2);
    }
}
