package basics.branching;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        // 1 - 10
        task1(3, 2);
        task2(1, 2);
        task3(2);
        System.out.println(task4(2, 2));
        System.out.println(task5(2, 6));
        System.out.println(task6(3, 4));
        System.out.println(task7(1, 2, -2, 3));
        System.out.println(task8(23, 12));
        System.out.println(task9(1, 1, 1));
        System.out.println(task10(1, 2));

        System.out.println("\n\n");

        // 11 - 20
        System.out.println(task11(2, 3, 3, 5));
        task12(-2, -4, 5);
        System.out.println(task13(2, 3, 3, 5));
        task14(30, 60);
        task15(10, 22);
        task16(1, 0);
        task17(22, 4);
        task18(-1, 2, 2);
        task19(2, 3,2);
        task20(10, 15, 22, 5);

        System.out.println("\n\n");

        // 21 - 30
        task21();
        task22(2, 3);
        task23();
        System.out.println(task24(10));
        task25(61);
        task26(2, 1, 3);
        System.out.println(task27(1, 2, 3, 4));
        System.out.println(task28(1, 2, 3, 3));
        System.out.println(task29(1,2,3,1,2,3));
        task30(-1, 2, 5);

        System.out.println("\n\n");

        // 31 - 40
        System.out.println(task31(1, 2, 3, 2, 2));
        System.out.println(task32(2, 3, 2));
        task33(234);
        task34(12, 13);
        task35(60);
        task36(2);
        task37(2);
        task38(2);
        task39(2);
        task40(2);
    }

    // 1 - 10

    private static void task1(int a, int b) {
        System.out.println(a < b ? "7" : "8");
    }

    private static void task2(int a, int b) {
        System.out.println(a < b ? "yes" : "no");
    }

    private static void task3(int a) {
        System.out.println(a < 3 ? "yes" : "no");
    }

    private static boolean task4(int a, int b) {
        return a == b ? true : false;
    }

    private static int task5(int a, int b) {
        return a < b ? a : b;
    }

    private static int task6(int a, int b) {
        return a > b ? a : b;
    }

    private static double task7(double a, double b, double c, double x) {
        double res = a*x*x + b*x + c;
        return res >= 0 ? res : -res;
    }

    private static int task8(int a, int b) {
        return a*a >= b*b ? a*a : b*b;
    }

    private static boolean task9(double a, double b, double c) {
        return a == b && a == c ? true : false;
    }

    private static String task10(double r1, double r2) {
        return r1 < r2 ? "Площадь первого круга меньше!" : "Площадь второго круга меньше!";
    }

    // 11 - 20

    private static String task11(double a1, double h1, double a2, double h2) {
        return a1*h1 > a2*h2 ? "Площадь первого треугольника больше!" : "Площадь второго треугольника больше!";
    }

    private static void task12(double a, double b, double c) {
        a = a >= 0 ? a * a : Math.pow(a, 4);
        b = b >= 0 ? b * b : Math.pow(b, 4);
        c = c >= 0 ? c * c : Math.pow(c, 4);
        System.out.println("a = " + a + ", b = " + b + ", c = " + c);
    }

    private static String task13(int x1, int x2, int y1, int y2) {
        return Math.sqrt(x1*x1 + y1*y1) < Math.sqrt(x2*x2 + y2*y2) ? "Первая точка ближе!" : "Вторая точка ближе!";
    }

    private static void task14(int alfa, int betha) {
        if(alfa + betha < 180) {
            System.out.print("Такой треугольник существует, ");

            if(alfa + betha == 90 || alfa == 90 || betha == 90) {
                System.out.println("и он является прямоугольным!");
            } else {
                System.out.println("но он не является прямоугольным!");
            }
        } else {
            System.out.println("Такого треугольника не существует");
        }
    }

    private static void task15(double a, double b) {
        double newA;
        double newB;
        if(a > b) {
            newB = (a + b) / 2;
            newA = 2*a*b;
        } else {
            newA = (a + b) / 2;
            newB = 2*a*b;
        }

        System.out.println("Число А = " + newA + ", Число Б = " + newB);
    }

    private static void task16(int x, int y) {
        if(x == y && x == 0) {
            System.out.println("Начало координат");
        } else if(x == 0) {
            System.out.println("Ось ОХ");
        } else if(y == 0) {
            System.out.println("Ось ОY");
        } else if(x > 0 && y > 0) {
            System.out.println("Первая четверть");
        } else if(x > 0 && y < 0) {
            System.out.println("Четвертая четверть");
        } else if(x < 0 && y > 0) {
            System.out.println("Вторая четверть");
        } else if(x < 0 && y < 0) {
            System.out.println("Третья четверть");
        }
    }

    private static void task17(int n, int m) {
        if(n == m) {
            n = m = 0;
        } else if(n > m){
            m = n;
        } else {
            n = m;
        }

        System.out.println("n = " + n + ", m = " + m);
    }

    private static void task18(int a, int b, int c) {
        int col = 0;

        if(a < 0) {
            col++;
        }

        if(b < 0) {
            col++;
        }

        if(c < 0) {
            col++;
        }

        System.out.println("Отрицательных чисел - " + col);
    }

    private static void task19(int a, int b, int c) {
        int col = 0;

        if(a > 0) {
            col++;
        }

        if(b > 0) {
            col++;
        }

        if(c > 0) {
            col++;
        }

        System.out.println("Положительных чисел - " + col);
    }

    private static void task20(int a, int b, int c, int k) {
        System.out.println(a % k == 0 ? "А делится на К без остатка" : "А делится на К с остатком");
        System.out.println(b % k == 0 ? "B делится на К без остатка" : "B делится на К с остатком");
        System.out.println(c % k == 0 ? "C делится на К без остатка" : "C делится на К с остатком");
    }

    // 21 - 30

    private static void task21() {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Кто ты: мальчик или девочка?");
        String answer = scanner.nextLine();

        if (answer.equals("М")) {
            System.out.println("Мне нравятся мальчики!");
        } else {
            System.out.println("Мне нравятся девочки!");
        }
    }

    private static void task22(int x, int y) {

        if(y > x) {
            int buffer = x;
            x = y;
            y = buffer;
        }

        System.out.println("x = " + x + ", y = " + y);
    }

    private static void task23() {

        Scanner scanner = new Scanner(System.in);
        System.out.print("Число?");
        int day = scanner.nextInt();

        System.out.print("Месяц?");
        int month = scanner.nextInt();

        if(month >= 1 && month <= 12 && day >= 1 && day <= 31) {
            System.out.println("day.month - " + day + "." + month);
        } else {
            System.out.println("Некорректно введенные данные");
        }
    }

    private static String task24(int n) {
        return n % 2 == 0 ? "Не любит" : "Любит";
    }

    private static void task25(double temp) {
        if(temp > 60) {
            System.out.println("Пожароопасная ситуация");
        }
    }

    private static void task26(int a, int b, int c) {

        if(a >= b && a >= c) {
            System.out.println("Большее из чисел - " + a);
        } else if(b >= a && b >= c) {
            System.out.println("Большее из чисел - " + b);
        } else {
            System.out.println("Большее из чисел - " + c);
        }

        if(a <= b && a <= c) {
            System.out.println("Меньшее из чисел - " + a);
        } else if(b <= a && b <= c) {
            System.out.println("Меньшее из чисел - " + b);
        } else {
            System.out.println("Меньшее из чисел - " + c);
        }
    }

    private static int task27(int a, int b, int c, int d) {
        int min1 = min(a, b);
        int min2 = min(c, d);
        return min1 > min2 ? min1 : min2;
    }

    private static int min(int x, int y) {
        return x > y ? y : x;
    }

    private static int task28(int a, int b, int c, int d) {
        if(a == d) {
            System.out.println("a = d");
            return a;
        } else if(b == d) {
            System.out.println("b = d");
            return b;
        } else if(c == d) {
            System.out.println("c = d");
            return c;
        }

        if(d - a >= d - b && d - a >= d - c) {
            System.out.println("Большее из разностей - " + (d - a));
        } else if(d - b >= d - a && d - b >= d - c) {
            System.out.println("Большее из разностей - " + (d - b));
        } else {
            System.out.println("Большее из разностей - " + (d - c));
        }
        return d;
    }

    private static boolean task29(int x1, int x2, int x3, int y1, int y2, int y3) {
        return (y1 - y2)*x3 + (x2 - x1)*y3 + (x1*y2 - x2*y1) == 0 ? true : false;
    }

    private static void task30(double a, double b, double c) {

        if(a > b && a > c) {
            a *= 2;
            b *= 2;
            c *= 2;
        } else {
            a = Math.abs(a);
            b = Math.abs(b);
            c = Math.abs(c);
        }

        System.out.println("a = " + a + ", b = " + b + ", c = " + c);
    }

    // 31 - 40

    private static boolean task31(double a, double b, double x, double y, double z) {

        if(x <= a && y <= b || y <= a && x <= b ||
                x <= a && z <= b || z <= a && x <= b ||
                z <= a && y <= b || y <= a && z <= b) {
            return true;
        } else {
            return false;
        }
    }

    private static boolean task32(int a, int b, int c) {

        if(a + b > 0 || a + c > 0 || b + c > 0) {
            return true;
        }
        return false;
    }

    private static void task33(int password) {

        switch (password) {
            case 1747:
                System.out.println("Доступ для баз данных A, B, C");
                break;
            case 3331:
                System.out.println("Доступ для баз данных B, C");
                break;
            case 7922:
                System.out.println("Доступ для баз данных B, C");
                break;
            case 8997:
                System.out.println("Доступ для баз данных C");
                break;
            case 9455:
                System.out.println("Доступ для баз данных C");
                break;
            case 9583:
                System.out.println("Доступ для баз данных A, B, C");
                break;
            default:
                System.out.println("Нет доступа к БД!");
                break;
        }
    }

    private static void task34(double price, double cash) {

        if(cash > price) {
            System.out.println("Ваша здача " + (cash - price));
        } else if(cash < price) {
            System.out.println("Не хватает средств, цена = " + price + ", внесено - " + cash);
        } else {
            System.out.println("Спасибо");
        }
    }

    private static void task35(int day) {
        
        int[] months = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

        for (int i = 0; i < 12; i++) {
            if(day > months[i]) {
                day -= months[i];
            } else {
                System.out.println("day.month - " + day + "." + (i + 1));
                break;
            }
        }
    }

    private static void task36(double x) {
        if(x <= 3) {
            System.out.println(x*x - 3*x + 9);
        } else {
            System.out.println(1. / (Math.pow(x, 3) + 6));
        }
    }

    private static void task37(double x) {
        if(x >= 3) {
            System.out.println(-x*x + 3*x + 9);
        } else {
            System.out.println(1. / (Math.pow(x, 3) - 6));
        }
    }

    private static void task38(double x) {
        if(x <= 3 && x >= 0) {
            System.out.println(x*x);
        } else {
            System.out.println(4);
        }
    }

    private static void task39(double x) {
        if(x >= 8) {
            System.out.println(-x*x + x - 9);
        } else {
            System.out.println(1. / (Math.pow(x, 4) - 6));
        }
    }

    private static void task40(double x) {
        if(x <= 13) {
            System.out.println(Math.pow(-x, 3) + 9);
        } else {
            System.out.println(-(3/(x+1)));
        }
    }
}
