package basics.cycles;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        // 1 -10
        task1();
        task2();
        task3();
        task4();
        task5();
        task6();
        task7(1, 2, 0.1);
        task8(1, 2, 3, 0.1);
        task9();
        task10();

        System.out.println("\n\n");

        // 11 - 20

        task11();
        task12(10);
        task13();
        task14(10);
        task15();
        task16();
        task17(3.2, 8);
        task18(12.3);
        task19(12.3);
        task20(12.3);

        // 21 - 30

        task21(0, 2, 0.3);
        task22(0, 2, 0.3);
        task23(0, 2, 0.3);
        task26();

        // 31 - 40

        task33(83255);
        task34();
    }

    // 1 - 10

    private static void task1() {

        for (int i = 1; i <= 5; i++) {
            System.out.print(i + "\t");
        }

        System.out.println();
    }

    private static void task2() {

        for (int i = 5; i > 0; i--) {
            System.out.print(i + "\t");
        }

        System.out.println();
    }

    private static void task3() {

        for (int i = 1; i < 10; i++) {
            System.out.print(i*3 + "\t");
        }

        System.out.println();
    }

    private static void task4() {

        int i = 2;
        while(i <= 200) {
            System.out.print(i + "\t");
            i += 2;
        }

        System.out.println();
    }

    private static void task5() {

        int sum = 0;
        int i = 1;

        while(i < 100) {
            sum += i;
            i += 2;
        }

        System.out.println("Сумма = " + sum);
    }

    private static void task6() {
        
        Scanner scanner = new Scanner(System.in);
        int num = scanner.nextInt();
        int sum = 0;
        for (int i = 1; i <= num; i++) {
            sum += i;
        }

        System.out.println("Sum = " + sum);
    }

    private static void task7(double a, double b, double h) {

        for (double i = a; i <= b; i += h) {
            if(i > 2) {
                System.out.print(i + "\t");
            } else {
                System.out.print(-i + "\t");
            }
        }

        System.out.println();
    }

    private static void task8(double a, double b, double c, double h) {

        for (double i = a; i <= b; i += h) {
            if(i == 15) {
                System.out.print((i + c)*2 + "\t");
            } else {
                System.out.print((i - c + 6) + "\t");
            }
        }

        System.out.println();
    }

    private static void task9() {

        int sum = 0;
        for (int i = 1; i <= 100; i++) {
            sum += i*i;
        }

        System.out.println("Sum = " + sum);
    }

    private static void task10() {

        long mult = 1;
        for (int i = 1; i <= 200; i++) {
            mult *= i*i;
        }

        System.out.println("Mult = " + mult);
    }

    // 11 - 20

    private static void task11() {

        int diff = 1;
        for (int i = 2; i <= 200; i++) {
            diff -= Math.pow(i, 3);
        }

        System.out.println("Разность = " + diff);
    }

    private static void task12(int n) {

        int a = 1;
        long mult = 1;
        for (int i = 0; i < n; i++, a += 6) {
            mult *= a;
        }

        System.out.println("Произведение до n-ого члена = " + mult);
    }

    private static void task13() {
        for (double i = -5; i <= 5; i += 0.5) {
            System.out.print((5 - i*i/2) + "\t");
        }
    }

    private static void task14(int n) {

        double sum = 0;
        for (int i = 1; i <= n; i++) {
            sum += 1. / i;
        }

        System.out.println("Сумма ряда = " + sum);
    }

    private static void task15() {

        long sum = 0;
        for (int i = 1; i <= 8; i *= 2) {
            sum += i;
        }

        for (int i = 7; i > 1; i++) {
            sum += i;
        }

        System.out.println(Math.pow(sum, 10));
    }

    private static void task16() {

        int sum = 1;
        long mult = 1;
        for (int i = 2; i <= 10; i++) {
            sum += i;
            mult *= sum;
        }

        System.out.println("Mult = " + mult);
    }

    private static void task17(double a, int n) {

        double mult = a;
        for (int i = 1; i < n; i++) {
            mult *= (a + i);
        }

        System.out.println("Mult = " + mult);
    }

    private static void task18(double e) {

        double abs;
        int n = 1;
        double sum = 0;

        do {
            abs = Math.pow(-1, n-1) / n;
            if(abs >= e) {
                sum += abs;
            }
            n++;
        } while(abs >= e);

        System.out.println("Sum = " + sum);
    }

    private static void task19(double e) {

        double abs;
        int n = 1;
        double sum = 0;

        do {
            abs = 1. / Math.pow(2, n) + 1. / Math.pow(3, n);
            if(abs >= e) {
                sum += abs;
            }
            n++;
        } while(abs >= e);

        System.out.println("Sum = " + sum);
    }

    private static void task20(double e) {

        double abs;
        int n = 1;
        double sum = 0;

        do {
            abs = 1. / (3*n - 2) / (3*n + 1);
            if(abs >= e) {
                sum += abs;
            }
            n++;
        } while(abs >= e);

        System.out.println("Sum = " + sum);
    }

    // 21 - 30

    private static void task21(double a, double b, double h) {

        System.out.println();
        for (double i = a; i <= b; i += h) {
            System.out.println("При x = " + i + "\t y = " + (i - Math.sin(i)));
        }
    }

    private static void task22(double a, double b, double h) {

        System.out.println();
        for (double i = a; i <= b; i += h) {
            System.out.println("При x = " + i + "\t y = " + (Math.pow(Math.sin(i), 2)));
        }
    }

    private static void task23(double a, double b, double h) {

        System.out.println();
        for (double i = a; i <= b; i += h) {
            System.out.println("При x = " + i + "\t y = " + (1 / Math.tan(i / 3) + Math.sin(i) / 2));
        }
    }

    private static void task24(int num) {

    }

    private static void task25() {

        Scanner scanner = new Scanner(System.in);
        int num = scanner.nextInt();
        long factorial = 1;

        for (int i = 1; i <= num; i++) {
            factorial *= i;
        }

        System.out.println("Factorial = " + factorial);
    }

    private static void task26() {
        for (int i = 0; i < 32; i++) {
            int j = i;
            while(j < 255) {
                System.out.print(j + "\t" + (char)j + "\t\t");
                j += 32;
            }
            System.out.println();
        }
    }

    private static void task27() {

    }

    private static void task28() {

    }

    private static void task29() {

    }

    private static void task30() {

    }

    private static void task31() {

    }

    private static boolean task32() {

        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();

        for (int i = 0; i < str.length(); i++) {
            if(i == 0 && !((str.charAt(i) >= 'A' && str.charAt(i) <= 'Z') || (str.charAt(i) >= 'a' && str.charAt(i) <= 'z'))) {
                return false;
            } else if(!(str.charAt(i) >= '0' && str.charAt(i) <= '9' || (str.charAt(i) >= 'A' && str.charAt(i) <= 'Z') || (str.charAt(i) >= 'a' && str.charAt(i) <= 'z'))) {
                return false;
            }
        }

        return true;
    }

    private static void task33(int num) {

        int res = 0;
        while(num != 0) {
            if(num % 10 > res) {
                res = num % 10;
            }
            num /= 10;
        }

        System.out.println("Самая большая цифра в числе - " + res);
    }

    private static void task34() {

        for (int i = 1000; i <= 9999; i++) {
            int num = i;
            int sum = 0;
            while(num != 0) {
                sum += num % 10;
                num /= 10;
            }
            if(sum == 15) {
                System.out.print(i + "  ");
            }
        }

        System.out.println();
    }

    private static void task35(int num) {

        int col = 0;
        while(num != 0) {
            if(num % 2 == 0) {
                col++;
            }
            num /= 10;
        }

        System.out.println("Кол-во четных цифр в числе - " + col);
    }

    private static void task36() {

    }

    private static void task37() {

    }

    private static void task38() {

    }

    private static void task39() {

    }

    private static void task40() {

    }
}
