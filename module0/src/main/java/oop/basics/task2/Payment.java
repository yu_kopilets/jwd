package oop.basics.task2;

import java.util.ArrayList;
import java.util.List;

public class Payment {

    private List<Product> products;

    public Payment() {
        products = new ArrayList<Product>();
    }

    public Payment(List<Product> products) {
        this.products = products;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public void showCurrentlyOrders() {

        System.out.println("Currently products:");
        for(Product product: products) {
            System.out.println(product.toString());
        }
    }

    public void addOrder(Product product) {
        products.add(product);
    }
}
