package oop.basics.task2;

public interface PaymentService {

    void showCurrentlyOrders();
    void addOrder(Product product);
}
