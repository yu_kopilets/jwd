package oop.basics.task4;

public class Treasure {

    private double price;

    public Treasure(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Treasure{" +
                "price=" + price +
                '}';
    }
}
