package oop.basics.task4;

import java.util.Arrays;

public class DragonCave implements DragonCaveService {

    private Treasure[] treasures;

    public DragonCave(Treasure[] treasures) {
        this.treasures = treasures;
    }

    public void showTreasures() {

        System.out.println("Treasures in cave:");
        for(Treasure treasure: treasures) {
            System.out.println(treasure.toString());
        }
        System.out.println();
    }

    public Treasure mostExpensiveTreasure() {

        double maxPrice = treasures[0].getPrice();
        Treasure expensiveTreasure = treasures[0];

        for(Treasure treasure: treasures) {
            if(treasure.getPrice() > maxPrice) {
                maxPrice = treasure.getPrice();
                expensiveTreasure = treasure;
            }
        }
        return expensiveTreasure;
    }

    public void summaryTreasure(double summaryPrice) {

        double sum = 0;
        System.out.println("Treasures for picked price:");

        for(Treasure treasure: treasures) {

            sum += treasure.getPrice();
            System.out.println(treasure.toString());

            if(sum >= summaryPrice) {
                break;
            }
        }
        System.out.println();
    }

    public Treasure[] getTreasures() {
        return treasures;
    }

    public void setTreasures(Treasure[] treasures) {
        this.treasures = treasures;
    }

    @Override
    public String toString() {
        return "DragonCave{" +
                "treasures=" + Arrays.toString(treasures) +
                '}';
    }
}
