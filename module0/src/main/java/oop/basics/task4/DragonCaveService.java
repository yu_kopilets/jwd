package oop.basics.task4;

public interface DragonCaveService {

    void showTreasures();
    Treasure mostExpensiveTreasure();
    void summaryTreasure(double summaryPrice);
}
