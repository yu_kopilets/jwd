package oop.basics;

import oop.basics.task1.Directory;
import oop.basics.task1.TextFile;
import oop.basics.task2.Payment;
import oop.basics.task2.Product;
import oop.basics.task4.DragonCave;
import oop.basics.task4.Treasure;
import oop.basics.task5.UserMenu;
import oop.basics.task5.controller.AppController;
import oop.basics.task5.controller.MakingGiftCommand;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        // task 1
        TextFile file = new TextFile(new Directory("d:/abc"), "notes");
        file.rename("new_notes");
        file.addLine("lalala");
        file.printText();
        file.remove();

        System.out.println();

        // task 2
        Payment payment = new Payment();
        payment.addOrder(new Product("Shoe", 10.2));
        payment.addOrder(new Product("Shoe", 10.3));
        payment.showCurrentlyOrders();

        System.out.println();

        // task 4
        DragonCave dragonCave = new DragonCave(new Treasure[]{
                new Treasure(12.3),
                new Treasure(122),
                new Treasure(43.21),
                new Treasure(542.2)
        });

        dragonCave.showTreasures();
        dragonCave.summaryTreasure(150);
        System.out.println(dragonCave.mostExpensiveTreasure().toString());

        System.out.println();

        // task 5
        Scanner scanner = new Scanner(System.in);

        while(true) {

            UserMenu.showMenu();
            String query = scanner.nextLine();
            if("quit".equals(query)) {
                break;
            } else {
                MakingGiftCommand command = new MakingGiftCommand();
                AppController appController = new AppController(command);
                appController.handleUserQuery(query);
            }
        }
    }
}
