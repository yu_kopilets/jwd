package oop.basics.task5.dao;

import oop.basics.task5.model.Wrapper;
import oop.basics.task5.model.WrapperType;

import java.util.ArrayList;
import java.util.List;

public class WrapperList {

    private static WrapperList instance;
    private List<Wrapper> wrappers;

    private WrapperList() {
        wrappers = new ArrayList<Wrapper>();
        wrappers.add(new Wrapper("Default", WrapperType.CARTON));
        wrappers.add(new Wrapper("Pack1", WrapperType.CARTON));
        wrappers.add(new Wrapper("Pack2", WrapperType.PAPER));
        wrappers.add(new Wrapper("Pack3", WrapperType.PAPER));
    }

    public List<Wrapper> getAll() {
        return wrappers;
    }

    public Wrapper getByName(String name) {

        for(Wrapper wrapper: wrappers) {
            if(wrapper.getName().equals(name)) {
                return wrapper;
            }
        }
        return new Wrapper("Default", WrapperType.CARTON);
    }

    public static WrapperList getInstance() {
        if(instance == null) {
            instance = new WrapperList();
        }
        return instance;
    }
}
