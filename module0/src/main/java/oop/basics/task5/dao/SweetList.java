package oop.basics.task5.dao;

import oop.basics.task5.model.Sweet;
import oop.basics.task5.model.SweetType;

import java.util.ArrayList;
import java.util.List;

public class SweetList {

    private static SweetList instance;
    private List<Sweet> sweets;

    private SweetList() {
        sweets = new ArrayList<Sweet>();
        sweets.add(new Sweet("Default", SweetType.BISCUITS));
        sweets.add(new Sweet("Pack1", SweetType.CHOCOLATE));
        sweets.add(new Sweet("Pack2", SweetType.ICE_CREAM));
        sweets.add(new Sweet("Pack3", SweetType.CHOCOLATE));
    }

    public List<Sweet> getAll() {
        return sweets;
    }

    public Sweet getByName(String name) {

        for(Sweet sweet: sweets) {
            if(sweet.getName().equals(name)) {
                return sweet;
            }
        }
        return new Sweet("Default", SweetType.BISCUITS);
    }

    public static SweetList getInstance() {
        if(instance == null) {
            instance = new SweetList();
        }
        return instance;
    }
}
// SweetList.getInstance().fsajkhfjsak