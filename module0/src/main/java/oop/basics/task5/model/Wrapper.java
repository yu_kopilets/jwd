package oop.basics.task5.model;

public class Wrapper {

    private String name;
    private WrapperType type;

    public Wrapper(String name, WrapperType type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public WrapperType getType() {
        return type;
    }

    public void setType(WrapperType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Wrapper{" +
                "name='" + name + '\'' +
                ", type=" + type +
                '}';
    }
}
