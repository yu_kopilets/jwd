package oop.basics.task5.model;

public class Sweet {

    private String name;
    private SweetType type;

    public Sweet(String name, SweetType type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SweetType getType() {
        return type;
    }

    public void setType(SweetType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Sweet{" +
                "name='" + name + '\'' +
                ", type=" + type +
                '}';
    }
}
