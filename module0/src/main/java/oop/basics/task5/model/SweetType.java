package oop.basics.task5.model;

public enum SweetType {
    CHOCOLATE,
    BISCUITS,
    ICE_CREAM
}
