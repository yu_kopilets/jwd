package oop.basics.task5.model;

import java.util.List;

public class Gift {

    private List<Sweet> sweets;
    private List<Wrapper> wrappers;

    public Gift(List<Sweet> sweets, List<Wrapper> wrappers) {
        this.sweets = sweets;
        this.wrappers = wrappers;
    }

    public List<Sweet> getSweets() {
        return sweets;
    }

    public void setSweets(List<Sweet> sweets) {
        this.sweets = sweets;
    }

    public List<Wrapper> getWrappers() {
        return wrappers;
    }

    public void setWrappers(List<Wrapper> wrappers) {
        this.wrappers = wrappers;
    }

    @Override
    public String toString() {
        return "Gift{" +
                "sweets=" + sweets +
                ", wrappers=" + wrappers +
                '}';
    }
}
