package oop.basics.task5.controller;

public interface AppCommand {

    void execute(String query);
}
