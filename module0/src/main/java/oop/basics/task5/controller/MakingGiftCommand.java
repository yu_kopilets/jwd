package oop.basics.task5.controller;

import oop.basics.task5.model.Gift;
import oop.basics.task5.model.Sweet;
import oop.basics.task5.model.Wrapper;
import oop.basics.task5.service.SimpleGiftService;
import oop.basics.task5.service.SimpleSweetService;
import oop.basics.task5.service.SimpleWrapperService;

import java.util.ArrayList;
import java.util.List;

public class MakingGiftCommand implements AppCommand {

    public void execute(String query) {

        SimpleGiftService giftService = new SimpleGiftService();
        SimpleSweetService sweetService = new SimpleSweetService();
        SimpleWrapperService wrapperService = new SimpleWrapperService();

        List<Sweet> sweets;
        List<Wrapper> wrappers;

        if(query.equals("all")) {

            sweets = sweetService.getAll();
            wrappers = wrapperService.getAll();
        } else {

            sweets = new ArrayList<Sweet>();
            sweets.add(sweetService.getByName(query));

            wrappers = new ArrayList<Wrapper>();
            wrappers.add(wrapperService.getByName(query));
        }

        Gift gift = giftService.createGift(sweets, wrappers);
        System.out.println("Your present is " + gift.toString());
    }
}
