package oop.basics.task5.controller;

public class AppController {

    private AppCommand command;

    public AppController(AppCommand command) {
        this.command = command;
    }

    public void handleUserQuery(String query) {
        command.execute(query);
    }
}
