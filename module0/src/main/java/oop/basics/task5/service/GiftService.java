package oop.basics.task5.service;

import oop.basics.task5.model.Gift;
import oop.basics.task5.model.Sweet;
import oop.basics.task5.model.Wrapper;

import java.util.List;

public interface GiftService {

    Gift createGift(List<Sweet> sweets, List<Wrapper> wrappers);
}
