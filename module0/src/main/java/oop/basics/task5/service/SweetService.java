package oop.basics.task5.service;

import oop.basics.task5.model.Sweet;

import java.util.List;

public interface SweetService {

    List<Sweet> getAll();
    Sweet getByName(String name);
}
