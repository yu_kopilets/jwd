package oop.basics.task5.service;

import oop.basics.task5.dao.WrapperList;
import oop.basics.task5.model.Wrapper;

import java.util.List;

public class SimpleWrapperService implements WrapperService {
    public List<Wrapper> getAll() {
        return WrapperList.getInstance().getAll();
    }

    public Wrapper getByName(String name) {
        return WrapperList.getInstance().getByName(name);
    }
}
