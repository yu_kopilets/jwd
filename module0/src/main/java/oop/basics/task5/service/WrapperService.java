package oop.basics.task5.service;

import oop.basics.task5.model.Wrapper;

import java.util.List;

public interface WrapperService {

    List<Wrapper> getAll();
    Wrapper getByName(String name);
}
