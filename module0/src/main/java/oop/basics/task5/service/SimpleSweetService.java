package oop.basics.task5.service;

import oop.basics.task5.dao.SweetList;
import oop.basics.task5.model.Sweet;

import java.util.List;

public class SimpleSweetService implements SweetService {
    public List<Sweet> getAll() {
        return SweetList.getInstance().getAll();
    }

    public Sweet getByName(String name) {
        return SweetList.getInstance().getByName(name);
    }
}
