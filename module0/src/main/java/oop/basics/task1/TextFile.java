package oop.basics.task1;

public class TextFile extends File {

    private String text = "";

    public TextFile(Directory directory, String fileName) {
        super(directory, fileName);
    }

    public void addLine(String line) {
        text += line;
    }

    public void rename(String name) {
        super.setFileName(name);
    }

    public void printText() {
        System.out.println(text);
    }

    public void remove() {
        text = "";
    }
}
