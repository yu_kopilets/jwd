package oop.basics.task1;

public class Directory {

    private String currentlyDirectory;

    public Directory(String currentlyDirectory) {
        this.currentlyDirectory = currentlyDirectory;
    }

    public String getCurrentlyDirectory() {
        return currentlyDirectory;
    }

    public void setCurrentlyDirectory(String currentlyDirectory) {
        this.currentlyDirectory = currentlyDirectory;
    }

    @Override
    public String toString() {
        return "Directory{" +
                "currentlyDirectory='" + currentlyDirectory + '\'' +
                '}';
    }
}
