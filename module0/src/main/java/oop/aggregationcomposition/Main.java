package oop.aggregationcomposition;

import oop.aggregationcomposition.task1.*;
import oop.aggregationcomposition.task2.Car;
import oop.aggregationcomposition.task2.Engine;
import oop.aggregationcomposition.task2.SimpleCarService;
import oop.aggregationcomposition.task2.Wheel;
import oop.aggregationcomposition.task3.*;
import oop.aggregationcomposition.task4.Account;
import oop.aggregationcomposition.task4.Client;
import oop.aggregationcomposition.task4.SimpleClientService;
import oop.aggregationcomposition.task5.Nutrition;
import oop.aggregationcomposition.task5.Tour;
import oop.aggregationcomposition.task5.TourList;
import oop.aggregationcomposition.task5.TourType;

public class Main {

    public static void main(String[] args) {

        // task 1
        Text text = new Text();
        SimpleTextService textService = new SimpleTextService(text);
        WordValidator wordValidator = new WordValidator();
        String str = "Hello";

        if(wordValidator.isValid(str)) {
            textService.addText(new Word(str));
        }
        textService.addText(new Sentence(" my dear!"));
        textService.printText();

        System.out.println();

        // task 2
        Car car = new Car(100, new Engine() {
            public boolean start() {
                return true;
            }
        }, Wheel.TYPE_A, Wheel.TYPE_B);

        SimpleCarService carService = new SimpleCarService(car);
        carService.move(80);
        carService.refuel(100);
        System.out.println(carService.toString());

        System.out.println();

        // task 3
        Government government = new Government(new City("Minsk"), new State[]{
                new State("Vitebsk", new Area[]{
                        new Area("Vileyka", new City("Vileyka"))
                })
        });

        SimpleGovernmentService govService = new SimpleGovernmentService(government);
        govService.showCapital();
        govService.showAreasCenters();
        govService.statesQuantity();

        System.out.println();

        // task 4
        Client client = new Client(new Account[]{
                new Account(120, 12.5),
                new Account(123, -12.5),
                new Account(125, 10),
                new Account(126, 320.5)
        });
        SimpleClientService clientService = new SimpleClientService(client);

        clientService.commonAccountsSum();
        clientService.blockAccount(120);
        clientService.findAccount(120);
        clientService.commonAccountsSum();

        System.out.println();

        // task 5
        TourList tourList = new TourList(new Tour[]{
                new Tour("Sea", TourType.SEA, 10, Nutrition.ALL_INCLUSIVE),
                new Tour("Sea-Turkey", TourType.SEA, 7, Nutrition.ALL_INCLUSIVE),
                new Tour("Greece", TourType.CRUISE, 12, Nutrition.ALL_INCLUSIVE),
                new Tour("Milan", TourType.SHOPPING, 3, Nutrition.HALF_BOARD)
        });
        tourList.findTourByDays(10);
        tourList.findTourByType(TourType.SEA);
        tourList.findTourByName("Milan");
        tourList.findTourByNutrition(Nutrition.HALF_BOARD);
    }
}
