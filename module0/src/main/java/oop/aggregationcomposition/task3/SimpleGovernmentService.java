package oop.aggregationcomposition.task3;

public class SimpleGovernmentService implements GovernmentService {

    private Government gov;

    public SimpleGovernmentService(Government gov) {
        this.gov = gov;
    }

    public void showCapital() {
        System.out.println(gov.getCapital().toString());
    }

    public void statesQuantity() {
        System.out.println(gov.getStates().length);
    }

    public void showAreasCenters() {
        for (int i = 0; i < gov.getStates().length; i++) {
            for (int j = 0; j < gov.getStates()[i].getAreas().length; j++) {
                System.out.println(gov.getStates()[i].getAreas()[j].toString());
            }
        }
    }

    public Government getGov() {
        return gov;
    }

    public void setGov(Government gov) {
        this.gov = gov;
    }
}
