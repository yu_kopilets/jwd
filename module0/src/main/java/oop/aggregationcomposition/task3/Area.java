package oop.aggregationcomposition.task3;

public class Area {

    private String areaName;
    private City areaCenter;

    public Area(String areaName, City areaCenter) {
        this.areaName = areaName;
        this.areaCenter = areaCenter;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public City getAreaCenter() {
        return areaCenter;
    }

    public void setAreaCenter(City areaCenter) {
        this.areaCenter = areaCenter;
    }

    @Override
    public String toString() {
        return "Area{" +
                "areaName='" + areaName + '\'' +
                ", areaCenter=" + areaCenter +
                '}';
    }
}
