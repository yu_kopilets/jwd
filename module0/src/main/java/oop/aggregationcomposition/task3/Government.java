package oop.aggregationcomposition.task3;

import java.util.Arrays;

public class Government {

    private City capital;
    private State[] states;

    public Government(City capital, State[] states) {
        this.capital = capital;
        this.states = states;
    }

    public City getCapital() {
        return capital;
    }

    public void setCapital(City capital) {
        this.capital = capital;
    }

    public State[] getStates() {
        return states;
    }

    public void setStates(State[] states) {
        this.states = states;
    }

    @Override
    public String toString() {
        return "Government{" +
                "capital=" + capital +
                ", states=" + Arrays.toString(states) +
                '}';
    }
}
