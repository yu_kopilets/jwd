package oop.aggregationcomposition.task3;

import oop.aggregationcomposition.task3.Area;

import java.util.Arrays;

public class State {

    private String stateName;
    private Area[] areas;

    public State(String stateName, Area[] areas) {
        this.stateName = stateName;
        this.areas = areas;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public Area[] getAreas() {
        return areas;
    }

    public void setAreas(Area[] areas) {
        this.areas = areas;
    }

    @Override
    public String toString() {
        return "State{" +
                "stateName='" + stateName + '\'' +
                ", areas=" + Arrays.toString(areas) +
                '}';
    }
}
