package oop.aggregationcomposition.task3;

public interface GovernmentService {

    void showCapital();
    void statesQuantity();
    void showAreasCenters();
}
