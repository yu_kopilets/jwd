package oop.aggregationcomposition.task4;

public interface ClientSummaryService {

    void commonAccountsSum();
    void positiveAccountsSum();
    void negativeAccountsSum();
}
