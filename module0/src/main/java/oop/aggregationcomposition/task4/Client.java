package oop.aggregationcomposition.task4;

public class Client {

    private Account[] accounts;

    public Client(Account[] accounts) {
        this.accounts = accounts;
    }

    public Account[] getAccounts() {
        return accounts;
    }

    public void setAccounts(Account[] accounts) {
        this.accounts = accounts;
    }
}
