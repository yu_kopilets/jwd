package oop.aggregationcomposition.task4;

public class Account {

    private final int accountNumber;
    private double money;
    private boolean blockStatus = false;

    public Account(int accountNumber, double money) {
        this.accountNumber = accountNumber;
        this.money = money;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public boolean isBlockStatus() {
        return blockStatus;
    }

    public void setBlockStatus(boolean blockStatus) {
        this.blockStatus = blockStatus;
    }

    public int getAccountNumber() {
        return accountNumber;
    }

    @Override
    public String toString() {
        return "Account{" +
                "accountNumber=" + accountNumber +
                ", money=" + money +
                ", blockStatus=" + blockStatus +
                '}';
    }
}
