package oop.aggregationcomposition.task4;

public class SimpleClientService implements ClientService, ClientSummaryService {

    private Client client;

    public SimpleClientService(Client client) {
        this.client = client;
    }

    public void findAccount(int accountNumber) {
        for(Account account: client.getAccounts()) {
            if(account.getAccountNumber() == accountNumber) {
                System.out.println("Found account " + account.toString());
            }
        }
    }

    public void blockAccount(int accountNumber) {
        for(Account account: client.getAccounts()) {
            if(account.getAccountNumber() == accountNumber) {
                account.setBlockStatus(true);
            }
        }
    }

    public void unblockAccount(int accountNumber) {
        for(Account account: client.getAccounts()) {
            if(account.getAccountNumber() == accountNumber) {
                account.setBlockStatus(false);
            }
        }
    }

    public void commonAccountsSum() {

        double sum = 0;
        for(Account account: client.getAccounts()) {
            sum += account.getMoney();
        }
        System.out.println("Sum of accounts is " + sum);
    }

    public void positiveAccountsSum() {

        double sum = 0;
        for(Account account: client.getAccounts()) {
            if(account.getMoney() > 0) {
                sum += account.getMoney();
            }
        }
        System.out.println("Sum of positives accounts is " + sum);
    }

    public void negativeAccountsSum() {

        double sum = 0;
        for(Account account: client.getAccounts()) {
            if(account.getMoney() < 0) {
                sum += account.getMoney();
            }
        }
        System.out.println("Sum of negatives accounts is " + sum);
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}
