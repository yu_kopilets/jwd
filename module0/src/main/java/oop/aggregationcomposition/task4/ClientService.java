package oop.aggregationcomposition.task4;

public interface ClientService {

    public void findAccount(int accountNumber);
    void blockAccount(int accountNumber);
    void unblockAccount(int accountNumber);
}
