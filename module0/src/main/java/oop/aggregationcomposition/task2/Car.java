package oop.aggregationcomposition.task2;

import java.util.ArrayList;
import java.util.List;

public class Car {

    private int fuel = 0;
    private List<Wheel> wheels;
    private Engine engine;

    public Car(int fuel, Engine engine, Wheel ... wheels) {

        this.fuel = fuel;
        this.engine = engine;
        this.wheels = new ArrayList<Wheel>();
        for(Wheel wheel: wheels) {
            this.wheels.add(wheel);
        }
    }

    public int getFuel() {
        return fuel;
    }

    public void setFuel(int fuel) {
        this.fuel = fuel;
    }

    public List<Wheel> getWheels() {
        return wheels;
    }

    public void setWheels(List<Wheel> wheels) {
        this.wheels = wheels;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    @Override
    public String toString() {
        return "Car{" +
                "fuel=" + fuel +
                '}';
    }
}
