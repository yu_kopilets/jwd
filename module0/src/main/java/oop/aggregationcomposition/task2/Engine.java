package oop.aggregationcomposition.task2;

public interface Engine {

    boolean start();
}
