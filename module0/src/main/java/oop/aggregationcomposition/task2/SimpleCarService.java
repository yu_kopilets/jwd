package oop.aggregationcomposition.task2;

public class SimpleCarService implements CarService {

    Car car;

    public SimpleCarService(Car car) {
        this.car = car;
    }

    public boolean move(int distance) {

        if(car.getFuel() - distance > 0) {
            car.setFuel(car.getFuel() - distance);
            System.out.println(distance + " passed");
            return true;
        } else {
            System.out.println("Refuel your car");
            return false;
        }
    }

    public void refuel(int fuel) {
        car.setFuel(car.getFuel() + fuel);
        System.out.println(fuel + " added");
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    @Override
    public String toString() {
        return "SimpleCarService{" +
                "car=" + car +
                '}';
    }
}
