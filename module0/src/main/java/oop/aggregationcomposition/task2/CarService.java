package oop.aggregationcomposition.task2;

public interface CarService {

    boolean move(int distance);
    void refuel(int fuel);
}
