package oop.aggregationcomposition.task5;

public enum TourType {

    SHOPPING,
    SEA,
    CRUISE
}
