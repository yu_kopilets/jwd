package oop.aggregationcomposition.task5;

public class TourList implements TourService {

    private final Tour[] tours;

    public TourList(Tour[] tours) {
        this.tours = tours;
    }

    public void findTourByName(String tourName) {

        System.out.println("Tours by name:");
        for(Tour tour: tours) {
            if(tour.getTourName().equals(tourName)) {
                System.out.println(tour.toString());
            }
        }
    }

    public void findTourByType(TourType tourType) {

        System.out.println("Tours by type:");
        for(Tour tour: tours) {
            if(tour.getTourType() == tourType) {
                System.out.println(tour.toString());
            }
        }
    }

    public void findTourByDays(int days) {

        System.out.println("Tours by days:");
        for(Tour tour: tours) {
            if(tour.getDays() == days) {
                System.out.println(tour.toString());
            }
        }
    }

    public void findTourByNutrition(Nutrition nutrition) {

        System.out.println("Tours by nutrition:");
        for(Tour tour: tours) {
            if(tour.getNutrition().equals(nutrition)) {
                System.out.println(tour.toString());
            }
        }
    }
}
