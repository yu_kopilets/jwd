package oop.aggregationcomposition.task5;

public class Tour {

    private String tourName;
    private TourType tourType;
    private int days;
    private Nutrition nutrition;

    public Tour(String tourName, TourType tourType, int days, Nutrition nutrition) {
        this.tourName = tourName;
        this.tourType = tourType;
        this.days = days;
        this.nutrition = nutrition;
    }

    public String getTourName() {
        return tourName;
    }

    public void setTourName(String tourName) {
        this.tourName = tourName;
    }

    public TourType getTourType() {
        return tourType;
    }

    public void setTourType(TourType tourType) {
        this.tourType = tourType;
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }

    public Nutrition getNutrition() {
        return nutrition;
    }

    public void setNutrition(Nutrition nutrition) {
        this.nutrition = nutrition;
    }

    @Override
    public String toString() {
        return "Tour{" +
                "tourName='" + tourName + '\'' +
                ", tourType=" + tourType +
                ", days=" + days +
                ", nutrition='" + nutrition + '\'' +
                '}';
    }
}
