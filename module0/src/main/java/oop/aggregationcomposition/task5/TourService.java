package oop.aggregationcomposition.task5;

public interface TourService {

    void findTourByName(String tourName);
    void findTourByType(TourType tourType);
    void findTourByDays(int days);
    void findTourByNutrition(Nutrition nutrition);
}
