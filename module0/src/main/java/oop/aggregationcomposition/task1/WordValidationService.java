package oop.aggregationcomposition.task1;

public interface WordValidationService {

    boolean isValid(String word);
}
