package oop.aggregationcomposition.task1;

public interface TextService {

    void printText();
    void addText(Sentence sentence);
    void addText(Word word);
}
