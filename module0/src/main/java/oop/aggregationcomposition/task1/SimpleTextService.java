package oop.aggregationcomposition.task1;

public class SimpleTextService implements TextService {

    private Text text;

    public SimpleTextService(Text text) {
        this.text = text;
    }

    public void printText() {
        System.out.println(text.getText());
    }

    public void addText(Sentence sentence) {
        text.setText(text.getText() + sentence.getSentence());
    }

    public void addText(Word word) {
        text.setText(text.getText() + word.getWord());
    }

    public Text getText() {
        return text;
    }

    public void setText(Text text) {
        this.text = text;
    }
}
