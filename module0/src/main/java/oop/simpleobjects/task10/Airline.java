package oop.simpleobjects.task10;

public class Airline {

    private String destination;
    private int lineNumber;
    private String planeType;
    private String departureTime;
    private int weakDay;

    public Airline(String destination, int lineNumber, String planeType, String departureTime, int weakDay) {
        this.destination = destination;
        this.lineNumber = lineNumber;
        this.planeType = planeType;
        this.departureTime = departureTime;
        this.weakDay = weakDay;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public int getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(int lineNumber) {
        this.lineNumber = lineNumber;
    }

    public String getPlaneType() {
        return planeType;
    }

    public void setPlaneType(String planeType) {
        this.planeType = planeType;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public int getWeakDay() {
        return weakDay;
    }

    public void setWeakDay(int weakDay) {
        this.weakDay = weakDay;
    }

    @Override
    public String toString() {
        return "Airline{" +
                "destination='" + destination + '\'' +
                ", lineNumber=" + lineNumber +
                ", planeType='" + planeType + '\'' +
                ", departureTime='" + departureTime + '\'' +
                ", weakDay=" + weakDay +
                '}';
    }
}
