package oop.simpleobjects.task10;

public class AirlineList implements AirlineService{

    private Airline[] airlines;

    public AirlineList(Airline[] airlines) {
        this.airlines = airlines;
    }

    public void showByDestination(String destination) {

        System.out.println("Lines by picked destination:");
        for(Airline airline: airlines) {
            if(airline.getDestination().equals(destination)) {
                System.out.println(airline.toString());
            }
        }
        System.out.println();
    }

    public void showByDay(int weakDay) {

        System.out.println("Lines by picked weak's day:");
        for(Airline airline: airlines) {
            if(airline.getWeakDay() == weakDay) {
                System.out.println(airline.toString());
            }
        }
        System.out.println();
    }
}
