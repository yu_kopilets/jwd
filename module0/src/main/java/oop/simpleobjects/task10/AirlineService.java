package oop.simpleobjects.task10;

public interface AirlineService {

    void showByDestination(String destination);
    void showByDay(int weakDay);
}
