package oop.simpleobjects.task4;

public interface TrainService {

    void sortByNumber();
    void showTrain(int trainNumber);
    void sortByDestination();
}
