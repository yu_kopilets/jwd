package oop.simpleobjects.task4;

import oop.simpleobjects.task4.Train;

public class TrainList implements TrainService{

    private final Train[] trains;

    public TrainList(Train[] trains) {
        this.trains = trains;
    }


    public void sortByNumber() {

        for (int i = 0; i < trains.length - 1; i++) {
            for (int j = i + 1; j < trains.length; j++) {

                if(trains[j].getTrainNumber() > trains[i].getTrainNumber()) {
                    Train buffer = trains[i];
                    trains[i] = trains[j];
                    trains[j] = buffer;
                }
            }
        }
    }

    public void showTrain(int trainNumber) {

        for(Train train: trains) {
            if(train.getTrainNumber() == trainNumber) {
                System.out.println("The train is - " + train.toString());
                return;
            }
        }
        System.out.println("Train not found");
    }

    public void sortByDestination() {

        for (int i = 0; i < trains.length - 1; i++) {
            for (int j = i + 1; j < trains.length; j++) {

                if(trains[i].getDestination().charAt(0) > trains[j].getDestination().charAt(0)) {
                    Train buffer = trains[i];
                    trains[i] = trains[j];
                    trains[j] = buffer;
                }
            }
        }
    }
}
