package oop.simpleobjects.task5;

public class Counter {

    private int value;
    private int from;
    private int to;

    public Counter() {
        value = 10;
        from = 5;
        to = 15;
    }

    public Counter(int value, int from, int to) {
        this.value = value;
        this.from = from;
        this.to = to;
    }

    public void increase() {
        if(value < to) {
            value++;
        }
    }

    public void reduce() {
        if(value > from) {
            value--;
        }
    }

    public int getValue() {
        return value;
    }
}
