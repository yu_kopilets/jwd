package oop.simpleobjects.task5;

public interface CounterValidationService {

    boolean isValid(int value, int from, int to);
}
