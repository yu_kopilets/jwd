package oop.simpleobjects;

import oop.simpleobjects.task10.Airline;
import oop.simpleobjects.task10.AirlineList;
import oop.simpleobjects.task3.SimpleStudentService;
import oop.simpleobjects.task3.Student;
import oop.simpleobjects.task4.Train;
import oop.simpleobjects.task4.TrainList;
import oop.simpleobjects.task5.Counter;
import oop.simpleobjects.task5.CounterValidator;
import oop.simpleobjects.task6.SimpleTimeService;
import oop.simpleobjects.task6.Time;
import oop.simpleobjects.task7.SimpleTriangleFactory;
import oop.simpleobjects.task7.SimpleTriangleService;
import oop.simpleobjects.task7.Triangle;
import oop.simpleobjects.task8.Customer;
import oop.simpleobjects.task8.CustomerList;
import oop.simpleobjects.task9.Book;
import oop.simpleobjects.task9.BookList;

public class Main {

    public static void main(String[] args) {

        // task 1
        Test1 test1 = new Test1(1, 2);
        System.out.println(test1.larger());

        System.out.println();

        // task 2
        Test2 test2 = new Test2(1, 2);
        test2.setNum1(2);

        System.out.println();

        // task 3
        Student[] students = {
                new Student("Yuriy", 1, new int[]{9, 9, 10, 9, 10, 10}),
                new Student("Vitaliy", 2, new int[]{9, 9, 8, 9, 10, 10}),
                new Student("Yulia", 3, new int[]{9, 9, 10, 9, 10, 10}),
                new Student("Nikita", 4, new int[]{2, 2, 3, 2, 2}),
                new Student("Nikolay", 5, new int[]{9, 9, 10, 9, 10, 10}),
                new Student("Vladimir", 6, new int[]{3, 4, 6, 2, 3}),
                new Student("Genadiy", 7, new int[]{9, 9, 10, 9, 10, 10}),
                new Student("Oleg", 8, new int[]{3, 2, 9, 4, 10}),
                new Student("Misha", 9, new int[]{9, 9, 10, 9, 10, 10}),
                new Student("Vanya", 10, new int[]{9, 9, 10, 9, 10, 10})
        };

        SimpleStudentService studentService = new SimpleStudentService();
        studentService.printStudents(students);

        System.out.println();

        // task 4
        TrainList trainList = new TrainList(new Train[]{
                new Train("London", 110, "10:20"),
                new Train("Liverpool", 20, "11:22"),
                new Train("Manchester", 330, "16:30"),
                new Train("Oxford", 55, "10:00"),
                new Train("York", 99, "22:10"),
        });

        trainList.sortByNumber();
        trainList.showTrain(99);

        System.out.println();

        // task 5
        CounterValidator validator = new CounterValidator();
        Counter counter;

        if(validator.isValid(5, 10, 20)) {
            counter = new Counter(5, 10 , 20);
        } else {
            counter = new Counter();
        }

        counter.increase();
        counter.increase();
        System.out.println(counter.getValue());
        counter.reduce();
        System.out.println(counter.getValue());

        System.out.println();

        // task 6
        Time time = new Time(10, 20, 30);
        SimpleTimeService timeService = new SimpleTimeService(time);
        System.out.println(time.toString());
        timeService.changeTime(1, 44, 151);
        System.out.println(time.toString());

        System.out.println();

        // task 7
        SimpleTriangleFactory triangleFactory = new SimpleTriangleFactory();
        Triangle abc = triangleFactory.create(1, 2, 2);
        SimpleTriangleService triangleService = new SimpleTriangleService(abc);

        System.out.println(triangleService.area());
        System.out.println(triangleService.perimentrul());

        System.out.println();

        //task 8
        CustomerList customerList = new CustomerList(new Customer[]{
                new Customer("Ilya", "12-2", 10, 120),
                new Customer("Petya", "13-3", 22, 130),
                new Customer("Vasya", "44-4", 122, 140)
        });

        customerList.findCardNumber(10, 30);
        customerList.sortByName();

        // task 9
        BookList bookList = new BookList(new Book[]{
                new Book(1, "Yashin", "Minsk", 2010, 12.5),
                new Book(2, "Gerick", "Minsk", 2013, 12.6),
                new Book(3, "Inats", "Minsk", 2011, 12.1),
                new Book(4, "Jecka", "Minsk", 2012, 9.2)
        });
        bookList.showByAuthor("Yashin");
        bookList.showByPublishing("Minsk");
        bookList.showByYear(2011);

        // task 10
        AirlineList airlineList = new AirlineList(new Airline[]{
                new Airline("London", 123, "A", "10-20", 2),
                new Airline("Berlin", 432, "B", "12-22", 3),
                new Airline("New-York", 652, "A", "18-30", 4),
                new Airline("Tokyo", 763, "A", "10-50", 5)
        });
        airlineList.showByDay(2);
        airlineList.showByDestination("Berlin");
    }
}
