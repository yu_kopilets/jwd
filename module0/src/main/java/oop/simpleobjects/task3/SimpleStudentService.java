package oop.simpleobjects.task3;

public class SimpleStudentService implements StudentService {

    public void printStudents(Student[] students) {
        for(Student student : students) {
            if(isWellPerformance(student)) {
                System.out.println(student.toString());
            }
        }
    }

    public boolean isWellPerformance(Student student) {

        for (int i = 0; i < student.getPerformance().length; i++) {
            if(student.getPerformance()[i] < 9) {
                return false;
            }
        }
        return true;
    }
}
