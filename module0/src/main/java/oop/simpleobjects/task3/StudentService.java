package oop.simpleobjects.task3;

public interface StudentService {

    void printStudents(Student[] students);
    boolean isWellPerformance(Student student);
}
