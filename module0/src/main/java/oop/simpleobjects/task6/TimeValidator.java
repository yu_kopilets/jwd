package oop.simpleobjects.task6;

public class TimeValidator implements TimeValidationService {

    public boolean hourIsValid(int hour) {
        if(hour >= 0) {
            return true;
        }
        return false;
    }

    public boolean minuteIsValid(int minute) {
        if(minute >= 0 && minute <= 60) {
            return true;
        }
        return false;
    }

    public boolean secondIsValid(int second) {
        if(second >= 0 && second <= 60) {
            return true;
        }
        return false;
    }
}
