package oop.simpleobjects.task6;

public interface TimeService {

    void changeTime(int hour, int minute, int second);
    void reformTime();
}
