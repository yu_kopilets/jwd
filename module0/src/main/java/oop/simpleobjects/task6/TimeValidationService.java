package oop.simpleobjects.task6;

public interface TimeValidationService {

    boolean hourIsValid(int hour);
    boolean minuteIsValid(int minute);
    boolean secondIsValid(int second);
}
