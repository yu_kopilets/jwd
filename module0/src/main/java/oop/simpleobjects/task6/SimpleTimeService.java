package oop.simpleobjects.task6;

public class SimpleTimeService {

    Time time;

    public SimpleTimeService(Time time) {
        this.time = time;
    }

    public void changeTime(int hour, int minute, int second) {

        time.setSecond(time.getSecond() + second);
        time.setMinute(time.getMinute() + minute);
        time.setHour(time.getHour() + hour);
        reformTime();
    }

    private void reformTime() {

        if(time.getSecond() > 60) {
            time.setMinute(time.getMinute() + (time.getSecond() - time.getSecond() % 60) / 60);
            time.setSecond(time.getSecond() % 60);
        }

        if(time.getMinute() > 60) {
            time.setHour(time.getHour() + (time.getMinute() - time.getMinute() % 60) / 60);
            time.setMinute(time.getMinute() % 60);
        }
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }
}
