package oop.simpleobjects.task8;

public interface CustomerService {

    void findCardNumber(int from, int to);
    void sortByName();
}
