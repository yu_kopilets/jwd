package oop.simpleobjects.task8;

public class CustomerList implements CustomerService{

    private final Customer[] customers;

    public CustomerList(Customer[] customers) {
        this.customers = customers;
    }

    public void findCardNumber(int from, int to) {

        System.out.println("\nFound customers:");
        for(Customer customer: customers) {
            if(customer.getCardNumber() >= from && customer.getCardNumber() <= to) {
                System.out.println(customer.toString());
            }
        }
    }

    public void sortByName() {

        for (int i = 0; i < customers.length - 1; i++) {
            for (int j = i + 1; j < customers.length; j++) {

                if(customers[i].getName().charAt(0) > customers[j].getName().charAt(0)) {
                    Customer buffer = customers[i];
                    customers[i] = customers[j];
                    customers[j] = buffer;
                }
            }
        }

        showList();
    }

    private void showList() {

        System.out.println("\nList of Customers:");
        for(Customer customer: customers) {
            System.out.println(customer.toString());
        }
    }
}
