package oop.simpleobjects.task7;

public class SimpleTriangleService implements TriangleService {

    private Triangle triangle;

    public SimpleTriangleService(Triangle triangle) {
        this.triangle = triangle;
    }

    public double area() {

        double semiper = perimentrul() / 2;
        double a = triangle.getA();
        double b = triangle.getB();
        double c = triangle.getC();

        return Math.sqrt(semiper*(a - semiper)*(b - semiper)*(c - semiper));
    }

    public double perimentrul() {

        double a = triangle.getA();
        double b = triangle.getB();
        double c = triangle.getC();

        return a + b + c;
    }

    public Triangle getTriangle() {
        return triangle;
    }

    public void setTriangle(Triangle triangle) {
        this.triangle = triangle;
    }
}
