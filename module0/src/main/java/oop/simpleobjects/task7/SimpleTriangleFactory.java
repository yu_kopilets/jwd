package oop.simpleobjects.task7;

public class SimpleTriangleFactory implements TriangleFactory {

    public Triangle create(int a, int b, int c) {
        if(a > 0 && b > 0 && c > 0) {
            return new Triangle(a, b, c);
        } else {
            return new Triangle(3, 4, 5);
        }
    }
}
