package oop.simpleobjects.task7;

public interface TriangleFactory {

    Triangle create(int a, int b, int c);
}
