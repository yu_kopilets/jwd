package oop.simpleobjects.task9;

public class Book {

    private int id;
    private String author;
    private String publishing;
    private int year;
    private double price;

    public Book(int id, String author, String publishing, int year, double price) {
        this.id = id;
        this.author = author;
        this.publishing = publishing;
        this.year = year;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublishing() {
        return publishing;
    }

    public void setPublishing(String publishing) {
        this.publishing = publishing;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Book{" +
                "author='" + author + '\'' +
                ", publishing='" + publishing + '\'' +
                ", year=" + year +
                ", price=" + price +
                '}';
    }
}
