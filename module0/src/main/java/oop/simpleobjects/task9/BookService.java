package oop.simpleobjects.task9;

public interface BookService {

    void showByAuthor(String author);
    void showByPublishing(String publishing);
    void showByYear(int year);
}
