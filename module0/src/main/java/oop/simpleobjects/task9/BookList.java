package oop.simpleobjects.task9;

public class BookList implements  BookService{

    private Book[] books;

    public BookList(Book[] books) {
        this.books = books;
    }

    public void showByAuthor(String author) {

        System.out.println("Books by picked author:");
        for(Book book: books) {
            if(book.getAuthor().equals(author)) {
                System.out.println(book.toString());
            }
        }
        System.out.println();
    }

    public void showByPublishing(String publishing) {

        System.out.println("Books by picked publishing:");
        for(Book book: books) {
            if(book.getPublishing().equals(publishing)) {
                System.out.println(book.toString());
            }
        }
        System.out.println();
    }

    public void showByYear(int year) {

        System.out.println("Books latter than picked year:");
        for(Book book: books) {
            if(book.getYear() > year) {
                System.out.println(book.toString());
            }
        }
        System.out.println();
    }
}
