package arrays.multidimensional;

public class Main {

    public static void main(String[] args) {

        // 1 - 10
        task1();
        task2();
        task3(new int[][]{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}});
        task4(new int[][]{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}});
        task5(new int[][]{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}});
        task6(new int[][]{{9, 9, 3}, {4, 5, 6}, {7, 8, 9}});
        task7(new int[][]{{1, 2, 3, 4, 5}, {-1, -2, -3, -4, -5}, {1, 2, -3, -4, -5}, {-1, -2, 3, 4, 5}, {1, 2, -3, 4, 5}});
        task8(new int[][]{{1, 2, 3, 7, 5}, {-1, -2, 7, -4, -5}, {7, 2, -3, -4, -5}, {-1, -2, 3, 4, 7}});
        task9(new int[][]{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}});
        task10(new int[][]{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}, 1,2);

        System.out.println("\n\n");

        // 11 - 20
        task11(new int[][]{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}, {10, 11, 12}});
        int[][] mas = task12(5);
        task13(6);
        task14(6);
        task15(6);
        task16(6);
        task17(6);
        task18(6);
        task19(8);
        task20(8);
    }

    // 1 - 10

    private static void task1(){
        int[][] mas = new int[3][4];
        for (int i = 0; i < mas.length ; i++) {
            for (int j = 0; j < mas[i].length; j++) {
                if(j == 0) {
                    mas[i][j] = 1;
                }
                System.out.print(mas[i][j] + "\t");
            }
            System.out.println();
        }
        System.out.println();
    }

    private static void task2(){

        int [][]mas = new int [2][3];
        for(int i = 0; i < mas.length; i++){
            for (int j = 0; j < mas[i].length; j++) {
                mas[i][j] = (int) Math.random() * 10;
                System.out.print(mas[i][j] + "\t");
            }
            System.out.println();
        }
        System.out.println();
    }

    private static void task3(int[][] mas) {

        System.out.println("First column:");
        for (int i = 0; i < mas.length; i++) {
            System.out.println(mas[i][0]);
        }

        System.out.println("Last column:");
        for (int i = 0; i < mas.length; i++) {
            System.out.println(mas[i][mas[i].length - 1]);
        }
        System.out.println();
    }

    private static void task4(int[][] mas) {

        System.out.println("First string:");
        for (int i = 0; i < mas[0].length; i++) {
            System.out.print(mas[0][i] + "\t");
        }
        System.out.println();

        System.out.println("Last string:");
        for (int i = 0; i < mas[mas.length - 1].length; i++) {
            System.out.print(mas[mas.length - 1][i] + "\t");
        }
        System.out.println();
    }

    private static void task5(int[][] mas){

        for (int i = 1; i < mas.length; i+=2) {
            for (int j = 0; j < mas[i].length; j++) {
                System.out.print(mas[i][j] + "\t");
            }
            System.out.println();
        }
        System.out.println();
    }

    private static void task6(int[][] mas){

        for (int j = 0; j < mas[0].length; j+=2) {

            if (mas[0][j] > mas[mas.length - 1][j]) {

                for (int i = 0; i < mas.length; i++) {
                    System.out.println(mas[i][j]);
                }
                System.out.println();
            }
        }
        System.out.println();
    }

    private static void task7(int[][] mas) {

        int sum = 0;
        for (int i = 0; i < mas.length; i++) {
            for (int j = 0; j < mas[i].length; j++) {

                if(mas[i][j] < 0 && (i+j) % 2 == 1){
                    sum += Math.abs(mas[i][j]);
                }
            }
        }
        System.out.println(sum);
    }

    private static void task8(int[][] mas) {

        int count = 0;
        for (int i = 0; i < mas.length; i++) {
            for (int j = 0; j < mas[i].length; j++) {
                if(mas[i][j] == 7) {
                    count++;
                }
            }
        }
        System.out.println(count);
    }

    private static void task9(int[][] mas){

        for (int i = 0, j = 0; i < mas.length; i++, j++) {
            System.out.println(mas[i][j]);
        }
        System.out.println();

        for (int i = mas.length - 1, j = 0; i >= 0; i--, j++) {
            System.out.println(mas[i][j]);
        }
        System.out.println();
    }

    private static void task10(int[][] mas, int k, int p){

        for(int i = 0; i < mas[k].length; i++){
            System.out.print(mas[k][i] + "\t");
        }
        System.out.println();

        for (int i = 0; i < mas.length; i++) {
            System.out.print(mas[i][p] + "\t");
        }
        System.out.println();
    }

    // 11 - 20

    private static void task11(int[][] mas){

        for (int i = 0; i < mas.length; i++) {
            if(i % 2 == 0){
                for (int j = mas[i].length - 1; j >= 0; j--) {
                    System.out.print(mas[i][j] + "\t");
                }
            }else{
                for (int j = 0; j < mas[i].length; j++) {
                    System.out.print(mas[i][j] + "\t");
                }
            }
            System.out.println();
        }
        System.out.println();
    }

    private static int[][] task12(int n){

        int[][] mas = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {

                if(i == j) {
                    mas[i][j] = i;
                }
                else {
                    mas[i][j] = 0;
                }
                System.out.print(mas[i][j] + "\t");
            }
            System.out.println();
        }
        System.out.println();
        return mas;
    }

    private static void task13(int n){

        int[][] mas = new int[n][n];
        for (int i = 0; i < n; i++) {

            if(i % 2 == 0) {
                for (int j = 0; j < n; j++) {
                    mas[i][j] = j + 1;
                    System.out.print(mas[i][j] + "\t");
                }
            }else{
                for (int j = n - 1; j >= 0; j--) {
                    mas[i][j] = j + 1;
                    System.out.print(mas[i][j] + "\t");
                }
            }
            System.out.println();
        }
        System.out.println();
    }

    private static void task14(int n){

        int[][] mas = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {

                if(n - i == j + 1) {
                    mas[i][j] = i + 1;
                } else {
                    mas[i][j] = 0;
                }
                System.out.print(mas[i][j] + "\t");
            }
            System.out.println();
        }
        System.out.println();
    }

    private static void task15(int n){

        int[][] mas = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {

                if(i == j) {
                    mas[i][j] = n - i;
                } else {
                    mas[i][j] = 0;
                }
                System.out.print(mas[i][j] + "\t");
            }
            System.out.println();
        }
        System.out.println();
    }

    private static void task16(int n){

        int[][] mas = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {

                if(i == j) {
                    mas[i][j] = (i + 1) * (i + 2);
                } else {
                    mas[i][j] = 0;
                }
                System.out.print(mas[i][j] + "\t");
            }
            System.out.println();
        }
        System.out.println();
    }

    private static void task17(int n){

        int[][] mas = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {

                if(i == 0 || j == 0 || i == n - 1 || j == n - 1) {
                    mas[i][j] = 1;
                } else {
                    mas[i][j] = 0;
                }
                System.out.print(mas[i][j] + "\t");
            }
            System.out.println();
        }
        System.out.println();
    }

    private static void task18(int n){

        int[][] mas = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {

                if(n - i >= j + 1) {
                    mas[i][j] = i + 1;
                } else {
                    mas[i][j] = 0;
                }
                System.out.print(mas[i][j] + "\t");
            }
            System.out.println();
        }
        System.out.println();
    }

    private static void task19(int n){

        int[][] mas = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {

                if((j >= i && n - i >= j + 1 && i < n/2) || (j <= i && n - i <= j + 1 && i >= n/2)) {
                    mas[i][j] = 1;
                } else {
                    mas[i][j] = 0;
                }
                System.out.print(mas[i][j] + "\t");
            }
            System.out.println();
        }
        System.out.println();
    }

    private static void task20(int n){

        int[][] mas = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {

                if((j > i && n - i > j + 1 && i < n/2) || (j < i && n - i < j + 1 && i >= n/2)) {
                    mas[i][j] = 0;
                } else {
                    mas[i][j] = 1;
                }
                System.out.print(mas[i][j] + "\t");
            }
            System.out.println();
        }
        System.out.println();
    }
}
