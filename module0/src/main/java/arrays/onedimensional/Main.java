package arrays.onedimensional;

public class Main {

    public static void main(String[] args) {

        // 1 - 10
        System.out.println(task1(2, 3, 2, 5, 6, 4));
        task2(0, 1, 0, 2);
        task3(-2, 3, 5);
        System.out.println(task4(1, 2, 3));
        task5(3, 5);
        task6(-1, 2, 5, 6);
        task7(10, 1, 11, 12);
        int[] mas = {1, 2, 3, 5, -1, 0, 1};
        task8(mas.length, mas);
        task9(1, 2, 3, 10);
        task10(1, 5, 2, 10);

        System.out.println("\n\n");

        // 11 - 20
        task11(2, 0, 5, 2, 1, 5, 2);
        task12(1, 2, 3, 4, 5, 6, 7, 8);
        task13(2, 0, 3, 1, 2, 3, 5);
        task14(mas.length, mas);
        task15(1, 6, 2, 4);
        task17(1, 2, 1, 2, 5);
        task18(1, 4);
        System.out.println(task19(mas.length, mas));
        task20(mas.length, mas);
    }

    // 1 - 10

    private static int task1(int k, int ... values) {
        int sum = 0;
        for (int i = 0; i < values.length; i++) {
            if(values[i] % k == 0) {
                sum += values[k];
            }
        }
        return sum;
    }

    private static void task2(int ... values) {

        int col = 0;
        for (int i = 0; i < values.length; i++) {
            if(values[i] == 0) {
                col++;
            }
        }

        if(col > 0) {
            int[] mas = new int[col];
            for (int i = 0, j = 0; i < values.length; i++) {
                if (values[i] == 0) {
                    mas[j] = i;
                    System.out.print(mas[j] + "\t");
                    j++;
                }
            }
            System.out.println();
        }
    }

    private static void task3(int ... values) {

        for (int i = 0; i < values.length; i++) {
            if(values[i] != 0) {
                if(values[i] > 0) {
                    System.out.println("Положительное");
                    break;
                } else {
                    System.out.println("Отрицательное");
                    break;
                }
            }
        }
    }

    private static boolean task4(int ... values) {
        for (int i = 1; i < values.length; i++) {
            if(values[i] < values[i - 1]) {
                return false;
            }
        }
        return true;
    }

    private static void task5(int ... values) {

        int col = 0;
        for (int i = 0; i < values.length; i++) {
            if(values[i] % 2 == 0) {
                col++;
            }
        }

        if(col == 0) {
            System.out.println("В оследовательности нет четных чисел!");
        } else {
            int[] mas = new int[col];
            for (int i = 0, j = 0; i < values.length; i++, j++) {
                if(values[i] % 2 == 0) {
                    mas[j] = i;
                    j++;
                    System.out.print(mas[j] + "\t");
                }
            }
            System.out.println();
        }
    }

    private static void task6(int ... values) {
        int max = values[0];
        int min = values[0];

        for (int i = 1; i < values.length; i++) {
            if(values[i] < min) {
                min = values[i];
            } else if(values[i] > max) {
                max = values[i];
            }
        }

        System.out.println("Минимальная длина оси = " + (max - min));
    }

    private static void task7(int z, int ... values) {
        int col = 0;
        for (int i = 0; i < values.length; i++) {
            if(values[i] > z) {
                values[i] = z;
                col++;
            }
        }

        System.out.println("Кол-во замен = " + col);
    }

    private static void task8(int n, int[] mas) {

        int countZero = 0;
        int countPos = 0;
        int countNeg = 0;

        for (int i = 0; i < n; i++) {
            if(mas[i] > 0) {
                countPos++;
            } else if(mas[i] == 0) {
                countZero++;
            } else {
                countNeg++;
            }
        }

        System.out.println("Нулевых - " + countZero + ", Положительных - " + countPos + ", Отрицательных - " + countNeg);
    }

    private static void task9(int ... values) {

        int min = values[0];
        int max = values[0];
        int indexMin = 0;
        int indexMax = 0;

        for (int i = 0; i < values.length; i++) {
            if(values[i] > max) {
                max = values[i];
                indexMax = i;
            } else if (values[i] < min) {
                min = values[i];
                indexMin = i;
            }
        }

        if(indexMax != indexMin) {
            int buf = values[indexMin];
            values[indexMin] = values[indexMax];
            values[indexMax] = buf;
        }

        for (int i = 0; i < values.length; i++) {
            System.out.print(values[i] + "\t");
        }

        System.out.println();
    }

    private static void task10(int ... values) {
        for (int i = 0; i < values.length; i++) {
            if(values[i] > i) {
                System.out.print(values[i] + "\t");
            }
        }
        System.out.println();
    }

    // 11 - 20

    private static void task11(int m, int l, int ... values) {

        for (int i = 0; i < values.length; i++) {

            if(values[i] % m == l) {
                System.out.print(values[i] + "\t");
            }
        }
        System.out.println();
    }

    private static void task12(int ... values) {

        for (int i = 0; i < values.length; i++) {

            boolean check = false;
            for (int j = 2; j < i; j++) {
                if(i % j == 0) {
                    check = true;
                    break;
                }
            }

            if(check) {
                System.out.print(values[i] + "\t");
            }
        }
        System.out.println();
    }

    private static void task13(int m, int l, int n, int ... values) {

        for (int i = 0; i < values.length; i++) {

            int k = values[i] % m;
            if(k == 0 && k >= l && k <= n) {
                System.out.print(values[i] + "\t");
            }
        }
        System.out.println();
    }

    private static void task14(int n, int[] mas) {

        int max;
        int min;

        if(n > 1) {
            max = mas[1];
            min = mas[0];

            for (int i = 0; i < n; i += 2) {

                if(mas[i] < min) {
                    min = mas[i];
                }
            }

            for (int i = 1; i < n; i += 2) {

                if(mas[i] > max) {
                    max = mas[i];
                }
            }
        } else {
            max = min = 0;
        }

        System.out.println(min + max);
    }

    private static void task15(int c, int d, int ... values) {

        for (int i = 0; i < values.length; i++) {
            if(values[i] >= c && values[i] <= d) {
                System.out.print(values[i] + "\t");
            }
        }
        System.out.println();
    }

    private static void task16() {
    }

    private static void task17(int ... values) {

        int min = values[0];
        for (int i = 0; i < values.length; i++) {
            if(values[i] < min) {
                min = values[i];
            }
        }

        for (int i = 0; i < values.length; i++) {
            if(values[i] == min) {
                System.out.print(values[i] + "\t");
            }
        }
        System.out.println();
    }

    private static void task18(int cube1, int cube2) {

        int[] cubes = new int[10];
        cubes[0] = cube1;
        cubes[1] = cube2;

        for (int i = 2; i < 10; i++) {
            int sumOfThree = cubes[i - 1] + cubes[i - 2];
            cubes[i] = 10 - sumOfThree;
        }

        for (int i = 0; i < 10; i++) {
            System.out.print(cubes[i] + "\t");
        }
        System.out.println();
    }

    private static int task19(int n, int[] mas) {

        int repeat = 0;
        int value = 0;

        for (int i = 0; i < n - 1; i++) {

            int count = 0;
            for (int j = 1; j < n; j++) {
                if(mas[i] == mas[j]) {
                    count++;
                }
            }

            if(count > repeat) {
                repeat = count;
                value = mas[i];
            } else if(count > 0 && count == repeat && mas[i] < value) {
                value = mas[i];
            }
        }

        return value;
    }

    private static void task20(int n, int[] mas) {

        for (int i = 1; i < n; i += 2) {
            mas[i] = 0;
        }

        for (int i = 0; i < n; i++) {
            System.out.print(mas[i] + "\t");
        }
    }
}
