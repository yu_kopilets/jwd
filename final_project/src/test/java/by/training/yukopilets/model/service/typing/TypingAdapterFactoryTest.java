package by.training.yukopilets.model.service.typing;

import by.training.yukopilets.application.ApplicationContext;
import by.training.yukopilets.model.service.typing.impl.PangramTypingAdapterImpl;
import by.training.yukopilets.model.service.typing.impl.TextTypingAdapterImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class TypingAdapterFactoryTest {

    private TypingAdapterFactory typingAdapterFactory;

    @BeforeClass
    public static void initializeContext() {
        ApplicationContext.initialize();
    }

    @Before
    public void setUp() throws Exception {
        typingAdapterFactory = ApplicationContext.getInstance().getBean(TypingAdapterFactory.class);
    }

    @After
    public void tearDown() throws Exception {
        typingAdapterFactory = null;
    }

    @Test
    public void createTypingAdapter() {
        TypingAdapter pangram = typingAdapterFactory.createTypingAdapter("pangram");
        TypingAdapter text = typingAdapterFactory.createTypingAdapter("text");
        assertEquals(pangram.getClass(), PangramTypingAdapterImpl.class);
        assertEquals(text.getClass(), TextTypingAdapterImpl.class);
    }
}