package by.training.yukopilets.model.dao.connection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class DataSourceImplTest {

    private static final Logger LOGGER = LogManager.getLogger();

    private DataSourceImpl dataSource;

    @BeforeClass
    public static void initialize() throws Exception {
        DataSourceImpl.initialize();
    }

    @Before
    public void setUp() {
        dataSource = DataSourceImpl.getInstance();
    }

    @After
    public void tearDown() throws Exception {
        dataSource = null;
    }

    @Test
    public void getConnection() {
        try {
            List<Connection> connections = new ArrayList<>();
            for (int i = 0; i < 10; i++) {
                Connection connection = dataSource.getConnection();
                assertNotNull(connection);
                connections.add(connection);
            }
            for (Connection connection : connections) {
                connection.close();
            }
        } catch (SQLException throwables) {
            LOGGER.error(throwables);
        }
    }

    @Test
    public void getConnectionOverPoolCapacity() {
        List<Thread> threads = new ArrayList<>();
        for (int i = 0; i < 11; i++) {
            Thread thread = new Thread(() -> {
                try {
                    Connection connection = dataSource.getConnection();
                    assertNotNull(connection);
                    Thread.currentThread().sleep(100);
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                } catch (InterruptedException e) {
                    Thread.interrupted();
                }
            });
            threads.add(thread);
        }

        for (Thread thread : threads) {
            thread.start();
        }
    }

    @Test
    public void getInstance() {
        DataSourceImpl instance = DataSourceImpl.getInstance();
        assert instance == dataSource;
    }
}