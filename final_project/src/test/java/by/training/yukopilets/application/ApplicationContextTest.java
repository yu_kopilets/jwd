package by.training.yukopilets.application;

import by.training.yukopilets.bean.BeanRegistryInfo;
import by.training.yukopilets.exception.AddBeanException;
import by.training.yukopilets.model.dao.mysql.MySQLTextDaoImpl;
import org.junit.*;

import static org.junit.Assert.*;

public class ApplicationContextTest {

    private static ApplicationContext appContext;

    @BeforeClass
    public static void setUp() throws Exception {
        ApplicationContext.initialize();
        appContext = ApplicationContext.getInstance();
    }

    @AfterClass
    public static void tearDown() throws Exception {
        appContext.destroy();
        appContext = null;
    }

    @Test(expected = IllegalStateException.class)
    public void initialize() {
        ApplicationContext.initialize();
    }

    @Test
    public void getInstance() {
        ApplicationContext instance = ApplicationContext.getInstance();
        assertEquals(instance, appContext);
    }

    @Test(expected = AddBeanException.class)
    public void addBeanWithoutAnnotation() {
        BeanRegistryInfo beanRegistryInfo = new BeanRegistryInfo();
        appContext.addBean(beanRegistryInfo);
    }

    @Test
    public void getBean() {
        MySQLTextDaoImpl bean = appContext.getBean(MySQLTextDaoImpl.class);
        assertEquals(bean, appContext.getBean(MySQLTextDaoImpl.class));
    }
}