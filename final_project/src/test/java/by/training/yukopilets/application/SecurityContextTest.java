package by.training.yukopilets.application;

import by.training.yukopilets.controller.command.CommandType;
import by.training.yukopilets.entity.user.User;
import by.training.yukopilets.entity.user.UserRank;
import by.training.yukopilets.entity.user.UserRole;
import org.junit.*;

import static org.junit.Assert.*;

public class SecurityContextTest {

    private static final String SESSION_ID = "node:12345";
    private static SecurityContext securityContext;

    private User user;

    @BeforeClass
    public static void initializeContext() throws Exception {
        SecurityContext.initialize();
        securityContext = SecurityContext.getInstance();
    }

    @AfterClass
    public static void tearDown() throws Exception {
        securityContext = null;
    }

    @Before
    public void setUp() {
        User user = new User(null, "user", "user", null, true, UserRole.USER, UserRank.BEGINNER);
        securityContext.login(SESSION_ID, user);
    }

    @Test(expected = IllegalStateException.class)
    public void initialize() {
        SecurityContext.initialize();
    }

    @Test
    public void login() {
        User user = new User(null, "user2", "user2", null, true, UserRole.USER, UserRank.ADVANCED);
        securityContext.login("testId", user);
        assertEquals(true, securityContext.isLoggedIn());
    }

    @Test
    public void isExecutable() {
        assertEquals(false, securityContext.isExecutable(CommandType.LOGIN));
        assertEquals(true, securityContext.isExecutable(CommandType.LOGOUT));
    }

    @Test
    public void unitIsAccessible() {
        assertEquals(true, securityContext.unitIsAccessible("Unit1"));
        assertEquals(false, securityContext.unitIsAccessible("Unit3"));
    }

    @Test
    public void getInstance() {
        SecurityContext instance = SecurityContext.getInstance();
        assertEquals(instance, securityContext);
    }

    @Test
    public void logout() {
        securityContext.logout(SESSION_ID);
        assertEquals(false, securityContext.isLoggedIn());
    }
}