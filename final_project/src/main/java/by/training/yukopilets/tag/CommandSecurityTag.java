package by.training.yukopilets.tag;

import by.training.yukopilets.application.SecurityContext;
import by.training.yukopilets.controller.command.CommandType;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

public class CommandSecurityTag extends TagSupport {

    private CommandType commandType;

    @Override
    public int doStartTag() throws JspException {
        if (SecurityContext.getInstance().isExecutable(commandType)) {
            return EVAL_BODY_INCLUDE;
        }
        return SKIP_BODY;
    }

    public CommandType getCommandType() {
        return commandType;
    }

    public void setCommandType(CommandType commandType) {
        this.commandType = commandType;
    }
}
