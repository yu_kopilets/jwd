package by.training.yukopilets.tag;

import by.training.yukopilets.application.SecurityContext;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

public class UnitSecurityTag extends TagSupport {

    private String unitName;

    @Override
    public int doStartTag() throws JspException {
        if (SecurityContext.getInstance().unitIsAccessible(unitName)) {
            return EVAL_BODY_INCLUDE;
        }
        return SKIP_BODY;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }
}
