package by.training.yukopilets.tag;

import by.training.yukopilets.application.SecurityContext;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

public class LoggedSecurityTag extends TagSupport {
    @Override
    public int doStartTag() throws JspException {
        if(SecurityContext.getInstance().isLoggedIn()) {
            return EVAL_BODY_INCLUDE;
        }
        return SKIP_BODY;
    }
}
