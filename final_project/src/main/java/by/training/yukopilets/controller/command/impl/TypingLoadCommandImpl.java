package by.training.yukopilets.controller.command.impl;

import by.training.yukopilets.application.ApplicationConstant;
import by.training.yukopilets.bean.Bean;
import by.training.yukopilets.controller.command.Command;
import by.training.yukopilets.exception.ServiceException;
import by.training.yukopilets.model.service.impl.TypingServiceImpl;
import by.training.yukopilets.model.service.input.InputElementIterator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Bean(name = ApplicationConstant.LOAD_TYPING_COMMAND)
public class TypingLoadCommandImpl implements Command {

    private static final Logger LOGGER = LogManager.getLogger();
    private static final String TYPING_TYPE = "type";
    private static final String TYPING_LENGTH = "length";

    private final TypingServiceImpl typingService;

    public TypingLoadCommandImpl(TypingServiceImpl typingService) {
        this.typingService = typingService;
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {

        HttpSession session = request.getSession();
        String typingType = request.getParameter(TYPING_TYPE);
        String typingLength = request.getParameter(TYPING_LENGTH);

        try {
            InputElementIterator currentTyping = typingService.loadTyping(typingType, typingLength);
            if(currentTyping.hasNext()) {
                session.setAttribute("currentTyping", currentTyping);
                request.setAttribute("templateHead", currentTyping.getHead());
                request.setAttribute("inputTemplate", currentTyping.next());
            }
        } catch (ServiceException e) {
            LOGGER.error("Failed to load typing!", e);
        }
        return "typing";
    }
}
