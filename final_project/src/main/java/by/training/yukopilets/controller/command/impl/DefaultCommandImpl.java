package by.training.yukopilets.controller.command.impl;

import by.training.yukopilets.application.ApplicationConstant;
import by.training.yukopilets.bean.Bean;
import by.training.yukopilets.controller.command.Command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Bean(name = ApplicationConstant.DEFAULT_COMMAND)
public class DefaultCommandImpl implements Command {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String urlPattern = request.getPathInfo();
        String viewName = ApplicationConstant.DEFAULT_URL_PATTERN.replace("/", "");

        for (String value : ApplicationConstant.URL_PATTERNS) {
            if (value.equals(urlPattern)) {
                viewName = value.replace("/", "");
            }
        }
        return viewName;
    }
}
