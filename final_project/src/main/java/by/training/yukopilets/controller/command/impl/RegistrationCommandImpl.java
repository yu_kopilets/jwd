package by.training.yukopilets.controller.command.impl;

import by.training.yukopilets.application.ApplicationConstant;
import by.training.yukopilets.bean.Bean;
import by.training.yukopilets.controller.command.Command;
import by.training.yukopilets.exception.ServiceException;
import by.training.yukopilets.model.service.impl.UserServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Bean(name = ApplicationConstant.REGISTRATION_COMMAND)
public class RegistrationCommandImpl implements Command {

    private static final Logger LOGGER = LogManager.getLogger();
    private static final String LOGIN = "login";
    private static final String PASSWORD = "password";
    private static final String EMAIL = "email";

    private final UserServiceImpl userService;

    public RegistrationCommandImpl(UserServiceImpl userService) {
        this.userService = userService;
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {

        String login = request.getParameter(LOGIN);
        String password = request.getParameter(PASSWORD);
        String email = request.getParameter(EMAIL);

        if(login != null && password != null && email != null) {
            try {
                userService.createUser(login, password, email);
                request.setAttribute("userLogin", login);
                return "login";
            } catch (ServiceException e) {
                LOGGER.error("Failed to registration user!", e);
            }
        }
        return "registration";
    }
}
