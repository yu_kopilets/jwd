package by.training.yukopilets.controller.command.impl;

import by.training.yukopilets.application.ApplicationConstant;
import by.training.yukopilets.application.SecurityContext;
import by.training.yukopilets.bean.Bean;
import by.training.yukopilets.controller.command.Command;
import by.training.yukopilets.exception.ServiceException;
import by.training.yukopilets.model.service.impl.UnitServiceImpl;
import by.training.yukopilets.model.service.input.InputElementIterator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Bean(name = ApplicationConstant.LOAD_UNIT_COMMAND)
public class UnitLoadCommandImpl implements Command {

    private static final Logger LOGGER = LogManager.getLogger();
    private static final String UNIT_NAME = "name";

    private final UnitServiceImpl unitService;

    public UnitLoadCommandImpl(UnitServiceImpl unitService) {
        this.unitService = unitService;
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {

        HttpSession session = request.getSession();
        String unitName = request.getParameter(UNIT_NAME);

        if(SecurityContext.getInstance().unitIsAccessible(unitName.replace(" ", ""))) {
            try {
                InputElementIterator currentUnit = unitService.loadUnit(unitName);
                if(currentUnit.hasNext()) {
                    session.setAttribute("currentUnit", currentUnit);
                    request.setAttribute("templateHead", currentUnit.getHead());
                    request.setAttribute("inputTemplate", currentUnit.next());
                }
            } catch (ServiceException e) {
                LOGGER.error("Failed to load unit!", e);
            }
        }
        return "units";
    }
}
