package by.training.yukopilets.controller.command.impl;

import by.training.yukopilets.application.ApplicationConstant;
import by.training.yukopilets.bean.Bean;
import by.training.yukopilets.controller.command.Command;
import by.training.yukopilets.exception.ServiceException;
import by.training.yukopilets.model.service.impl.TextServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@Bean(name = ApplicationConstant.SUBMIT_TEXT_COMMAND)
public class SubmitTextCommandImpl implements Command {

    private static final Logger LOGGER = LogManager.getLogger();
    private static final String TEXT_ID = "textId";

    private final TextServiceImpl textService;

    public SubmitTextCommandImpl(TextServiceImpl textService) {
        this.textService = textService;
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String[] params = request.getParameterValues(TEXT_ID);
        if(params != null) {
            List<Long> identifiers = new ArrayList<>();

            for (String param : params) {
                identifiers.add(Long.parseLong(param));
            }

            try {
                textService.submitText(identifiers);
            } catch (ServiceException e) {
                LOGGER.error("Failed to submit text(s)!");
            }
        }
        return "redirect:user-menu?commandName=showNewTexts";
    }
}
