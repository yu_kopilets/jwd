package by.training.yukopilets.controller.command.impl;

import by.training.yukopilets.application.ApplicationConstant;
import by.training.yukopilets.bean.Bean;
import by.training.yukopilets.controller.command.Command;
import by.training.yukopilets.entity.Text;
import by.training.yukopilets.exception.ServiceException;
import by.training.yukopilets.model.service.impl.TextServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Bean(name = ApplicationConstant.SHOW_OWN_TEXTS_COMMAND)
public class ShowOwnTextsCommandImpl implements Command {

    private static final Logger LOGGER = LogManager.getLogger();

    private final TextServiceImpl textService;

    public ShowOwnTextsCommandImpl(TextServiceImpl textService) {
        this.textService = textService;
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<Text> texts = textService.getAllCurrentUserTexts();
            request.setAttribute("ownTexts", texts);
            request.setAttribute("userMenuView", "own-texts");
        } catch (ServiceException e) {
            LOGGER.error("Failed to get current user's texts!", e);
        }
        return "user-menu";
    }
}
