package by.training.yukopilets.controller.command;

import by.training.yukopilets.application.ApplicationConstant;

import javax.servlet.http.HttpServletRequest;

public class CommandProvider {

    private CommandProvider() {
    }

    public static String getCommandNameFromRequest(HttpServletRequest req) {
        return req.getParameter("commandName") != null
                ? req.getParameter("commandName")
                : ApplicationConstant.DEFAULT_COMMAND;
    }

    public static Command defineCommand(String commandName) {
        Command command = CommandType.DEFAULT.getCommand();
        CommandType[] types = CommandType.values();
        for (CommandType type : types) {
            String typeName = type.name().replaceAll("_", "");
            if(typeName.equalsIgnoreCase(commandName)) {
                command = type.getCommand();
            }
        }
        return command;
    }
}
