package by.training.yukopilets.controller.command.impl;

import by.training.yukopilets.application.ApplicationConstant;
import by.training.yukopilets.bean.Bean;
import by.training.yukopilets.controller.command.Command;
import by.training.yukopilets.entity.Text;
import by.training.yukopilets.exception.ServiceException;
import by.training.yukopilets.model.service.impl.TextServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Bean(name = ApplicationConstant.SHOW_NEW_TEXTS_COMMAND)
public class ShowNewTextsCommandImpl implements Command {

    private static final Logger LOGGER = LogManager.getLogger();

    private final TextServiceImpl textService;

    public ShowNewTextsCommandImpl(TextServiceImpl textService) {
        this.textService = textService;
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<Text> texts = textService.getAllInaccessibleTexts();
            request.setAttribute("newTexts", texts);
            request.setAttribute("userMenuView", "new-texts");
        } catch (ServiceException e) {
            LOGGER.error("Failed to get inaccessible texts!", e);
        }
        return "user-menu";
    }
}
