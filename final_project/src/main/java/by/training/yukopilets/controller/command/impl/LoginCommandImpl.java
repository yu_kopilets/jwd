package by.training.yukopilets.controller.command.impl;

import by.training.yukopilets.application.ApplicationConstant;
import by.training.yukopilets.application.SecurityContext;
import by.training.yukopilets.bean.Bean;
import by.training.yukopilets.controller.command.Command;
import by.training.yukopilets.entity.user.User;
import by.training.yukopilets.exception.ServiceException;
import by.training.yukopilets.model.service.impl.UserServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Bean(name = ApplicationConstant.LOGIN_COMMAND)
public class LoginCommandImpl implements Command {

    private static final Logger LOGGER = LogManager.getLogger();
    private static final String LOGIN = "login";
    private static final String PASSWORD = "password";

    private final UserServiceImpl userService;

    public LoginCommandImpl(UserServiceImpl userService) {
        this.userService = userService;
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {

        String login = request.getParameter(LOGIN);
        String password = request.getParameter(PASSWORD);

        if(login != null && password != null) {
            try {
                User user = userService.getUserByLogin(login, password);
                SecurityContext.getInstance().login(request.getSession().getId(), user);
                return "home";
            } catch (ServiceException e) {
                LOGGER.error("Failed to login user!", e);
            }
        }
        return "login";
    }
}
