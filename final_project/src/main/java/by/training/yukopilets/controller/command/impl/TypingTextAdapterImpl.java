package by.training.yukopilets.controller.command.impl;


import by.training.yukopilets.application.ApplicationConstant;
import by.training.yukopilets.bean.Bean;
import by.training.yukopilets.controller.command.Command;
import by.training.yukopilets.model.service.input.InputElementIterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Bean(name = ApplicationConstant.INPUT_TYPING_COMMAND)
public class TypingTextAdapterImpl implements Command {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {

        HttpSession session = request.getSession();
        InputElementIterator currentTyping = (InputElementIterator) session.getAttribute("currentTyping");

        if(currentTyping != null) {
            if(currentTyping.hasNext()) {
                request.setAttribute("templateHead", currentTyping.getHead());
                request.setAttribute("inputTemplate", currentTyping.next());
            } else {
                String description = currentTyping.getDescription().replaceAll("\\n", "<br>");
                request.setAttribute("templateHead", description);
                session.removeAttribute("currentTyping");
                currentTyping.finishInput();
            }
        }
        return "typing";
    }
}
