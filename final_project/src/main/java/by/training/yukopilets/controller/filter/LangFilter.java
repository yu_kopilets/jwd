package by.training.yukopilets.controller.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;
import java.util.stream.Stream;

@WebFilter(filterName = "lang", servletNames = "index")
public class LangFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {

        String lang = servletRequest.getParameter("lang");
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse resp = (HttpServletResponse) servletResponse;

        Optional<Cookie[]> cookies = Optional.ofNullable(req.getCookies());
        Cookie langCookie = cookies.map(Stream::of)
                .orElse(Stream.empty())
                .filter(cookie -> cookie.getName().equals("lang"))
                .findFirst()
                .orElse(new Cookie("lang", "en"));

        if("en".equalsIgnoreCase(lang) || "ru".equalsIgnoreCase(lang)) {
            langCookie.setValue(lang);
        }

        req.setAttribute("lang", langCookie.getValue());
        resp.addCookie(langCookie);
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
