package by.training.yukopilets.controller.command.impl;

import by.training.yukopilets.application.ApplicationConstant;
import by.training.yukopilets.bean.Bean;
import by.training.yukopilets.controller.command.Command;
import by.training.yukopilets.entity.user.UserHistory;
import by.training.yukopilets.exception.ServiceException;
import by.training.yukopilets.model.service.impl.UserHistoryServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.DecimalFormat;

@Bean(name = ApplicationConstant.USER_HISTORY_COMMAND)
public class UserHistoryCommandImpl implements Command {

    private static final Logger LOGGER = LogManager.getLogger();

    private final UserHistoryServiceImpl userHistoryService;

    public UserHistoryCommandImpl(UserHistoryServiceImpl userHistoryService) {
        this.userHistoryService = userHistoryService;
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        try {
            UserHistory userHistory = userHistoryService.getCurrentUserHistory();
            String formattedSpeed = new DecimalFormat("#0.00").format(userHistory.getAverageSpeed());

            request.setAttribute("typedPangrams", userHistory.getTypedPangrams());
            request.setAttribute("typedTexts", userHistory.getTypedTexts());
            request.setAttribute("typedTypings", userHistory.getTypedTypings());
            request.setAttribute("averageSpeed", formattedSpeed);
            request.setAttribute("userMenuView", "history");
        } catch (ServiceException e) {
            LOGGER.error("Failed to get user history!", e);
        }
        return "user-menu";
    }
}
