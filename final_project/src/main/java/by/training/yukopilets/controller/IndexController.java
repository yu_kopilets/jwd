package by.training.yukopilets.controller;

import by.training.yukopilets.controller.command.Command;
import by.training.yukopilets.controller.command.CommandProvider;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/*", name = "index")
public class IndexController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if(request.getPathInfo() != null) {
            String commandName = CommandProvider.getCommandNameFromRequest(request);
            Command command = CommandProvider.defineCommand(commandName);
            String viewName = command.execute(request, response);
            if (viewName.startsWith("redirect:")) {
                String redirect = viewName.replace("redirect:", "");
                response.sendRedirect(redirect);
            } else {
                request.setAttribute("viewName", viewName);
                request.getRequestDispatcher("/jsp/index.jsp").forward(request, response);
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
