package by.training.yukopilets.controller.command;

import by.training.yukopilets.application.ApplicationContext;
import by.training.yukopilets.controller.command.impl.*;

public enum CommandType {

    DEFAULT(ApplicationContext.getInstance().getBean(DefaultCommandImpl.class)),
    LOGIN(ApplicationContext.getInstance().getBean(LoginCommandImpl.class)),
    LOGOUT(ApplicationContext.getInstance().getBean(LogoutCommandImpl.class)),
    REGISTRATION(ApplicationContext.getInstance().getBean(RegistrationCommandImpl.class)),
    INPUT_TYPING(ApplicationContext.getInstance().getBean(TypingTextAdapterImpl.class)),
    LOAD_TYPING(ApplicationContext.getInstance().getBean(TypingLoadCommandImpl.class)),
    INPUT_UNIT(ApplicationContext.getInstance().getBean(UnitInputCommandImpl.class)),
    LOAD_UNIT(ApplicationContext.getInstance().getBean(UnitLoadCommandImpl.class)),
    USER_HISTORY(ApplicationContext.getInstance().getBean(UserHistoryCommandImpl.class)),
    UPLOAD_TEXT(ApplicationContext.getInstance().getBean(UploadTextCommandImpl.class)),
    SHOW_OWN_TEXTS(ApplicationContext.getInstance().getBean(ShowOwnTextsCommandImpl.class)),
    SHOW_NEW_TEXTS(ApplicationContext.getInstance().getBean(ShowNewTextsCommandImpl.class)),
    SUBMIT_TEXT(ApplicationContext.getInstance().getBean(SubmitTextCommandImpl.class));

    private final Command command;

    CommandType(Command command) {
        this.command = command;
    }

    public Command getCommand() {
        return command;
    }
}
