package by.training.yukopilets.controller.command.impl;

import by.training.yukopilets.application.ApplicationConstant;
import by.training.yukopilets.bean.Bean;
import by.training.yukopilets.controller.command.Command;
import by.training.yukopilets.exception.ServiceException;
import by.training.yukopilets.model.service.impl.TextServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Bean(name = ApplicationConstant.UPLOAD_TEXT_COMMAND)
public class UploadTextCommandImpl implements Command {

    private static final Logger LOGGER = LogManager.getLogger();
    private static final String TEXT_HEAD = "head";
    private static final String TEXT_BODY = "body";
    private static final String TEXT_DESCRIPTION = "description";

    private final TextServiceImpl textService;

    public UploadTextCommandImpl(TextServiceImpl textService) {
        this.textService = textService;
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {

        String head = request.getParameter(TEXT_HEAD);
        String body = request.getParameter(TEXT_BODY);
        String description = request.getParameter(TEXT_DESCRIPTION);

        if (head != null && body != null && description != null) {
            try {
                textService.uploadText(head, body, description);
            } catch (ServiceException e) {
                LOGGER.error("Failed to upload new text!", e);
            }
        }
        return "redirect:user-menu?commandName=showOwnTexts";
    }
}
