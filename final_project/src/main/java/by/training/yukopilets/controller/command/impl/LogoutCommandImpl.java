package by.training.yukopilets.controller.command.impl;

import by.training.yukopilets.application.ApplicationConstant;
import by.training.yukopilets.application.SecurityContext;
import by.training.yukopilets.bean.Bean;
import by.training.yukopilets.controller.command.Command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Bean(name = ApplicationConstant.LOGOUT_COMMAND)
public class LogoutCommandImpl implements Command {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        SecurityContext.getInstance().logout(request.getSession().getId());
        return "home";
    }
}
