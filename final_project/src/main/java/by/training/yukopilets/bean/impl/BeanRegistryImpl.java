package by.training.yukopilets.bean.impl;

import by.training.yukopilets.bean.Bean;
import by.training.yukopilets.bean.BeanRegistry;
import by.training.yukopilets.bean.BeanRegistryInfo;
import by.training.yukopilets.exception.AddBeanException;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Parameter;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Supplier;

public class BeanRegistryImpl implements BeanRegistry {

    private final BeanStore beanStore = new BeanStore();

    @Override
    public <T> void addBean(T bean) {
        BeanRegistryInfo beanInfo = createBeanInfo(bean.getClass());
        beanInfo.setBean(bean);
        beanStore.addBean(beanInfo);
    }

    @Override
    public <T> void addBean(Class<T> beanClass) {
        BeanRegistryInfo beanInfo = createBeanInfo(beanClass);
        Supplier<Object> factory = createBeanFactory(beanInfo);
        beanInfo.setBeanFactory(factory);
        beanStore.addBean(beanInfo);
    }

    private BeanRegistryInfo createBeanInfo(Class<?> clazz) {
        Bean bean = clazz.getAnnotation(Bean.class);
        if (bean == null) {
            throw new AddBeanException(clazz.getName() + " doesn't have @Bean annotation!");
        } else if (clazz.getDeclaredConstructors().length > 1) {
            throw new AddBeanException(clazz.getSimpleName() + " has more then 1 constructor!");
        }

        BeanRegistryInfo beanInfo = new BeanRegistryInfo();
        beanInfo.setClazz(clazz);
        beanInfo.setName(bean.name());
        return beanInfo;
    }

    private Supplier<Object> createBeanFactory(BeanRegistryInfo beanInfo) {
        Class<?> clazz = beanInfo.getClazz();
        Constructor<?> constructor = clazz.getConstructors()[0];

        return () -> {
            if (constructor.getParameterCount() == 0) {
                try {
                    return clazz.newInstance();
                } catch (InstantiationException | IllegalAccessException e) {
                    throw new AddBeanException("Bean factory creating exception (constructor without params)!");
                }
            } else {
                Parameter[] parameters = constructor.getParameters();
                Object[] args = new Object[parameters.length];

                for (int i = 0; i < parameters.length; i++) {
                    Class<?> type = parameters[i].getType();

                    Bean bean = type.getAnnotation(Bean.class);
                    if (bean == null) {
                        throw new AddBeanException("Wrong constructor arg (doesn't have @Bean annotation) - "
                                + clazz.getName());
                    }
                    args[i] = getBean(type);
                }
                try {
                    return constructor.newInstance(args);
                } catch (InstantiationException | InvocationTargetException | IllegalAccessException e) {
                    throw new AddBeanException("Bean factory creating exception (constructor with params)!");
                }
            }
        };
    }

    @Override
    public <T> T getBean(Class<T> beanClass) {
        BeanRegistryInfo beanInfo = createBeanInfo(beanClass);
        Optional<Object> bean = beanStore.getBean(beanInfo);
        if (bean.isPresent()) {
            return (T) bean.get();
        } else {
            addBean(beanClass);
            return (T) beanStore.getBean(beanInfo).get();
        }
    }

    @Override
    public <T> void removeBean(T bean) {
        BeanRegistryInfo beanInfo = createBeanInfo(bean.getClass());
        beanStore.remove(beanInfo);
    }

    @Override
    public void destroy() {
        beanStore.destroy();
    }

    private static class BeanStore {

        private final Map<String, Object> beans = new ConcurrentHashMap<>();

        private void addBean(BeanRegistryInfo beanInfo) {
            if (beans.containsKey(beanInfo.getName())) {
                throw new AddBeanException("Bean " + beanInfo.getName() + " already added!");
            }

            if (beanInfo.getBean() != null) {
                beans.put(beanInfo.getName(), beanInfo.getBean());
            } else {
                Object bean = beanInfo.getBeanFactory().get();
                beans.put(beanInfo.getName(), bean);
            }
        }

        private Optional<Object> getBean(BeanRegistryInfo beanInfo) {
            if (beans.containsKey(beanInfo.getName())) {
                return Optional.of(beans.get(beanInfo.getName()));
            }
            return Optional.empty();
        }

        private void remove(BeanRegistryInfo beanInfo) {
            beans.remove(beanInfo.getName());
        }

        private void destroy() {
            beans.clear();
        }
    }
}
