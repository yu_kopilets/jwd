package by.training.yukopilets.bean;

import java.util.function.Supplier;

public class BeanRegistryInfo {

    private String name;
    private Class<?> clazz;
    private Supplier<Object> beanFactory;
    private Object bean;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Class<?> getClazz() {
        return clazz;
    }

    public void setClazz(Class<?> clazz) {
        this.clazz = clazz;
    }

    public Supplier<Object> getBeanFactory() {
        return beanFactory;
    }

    public void setBeanFactory(Supplier<Object> beanFactory) {
        this.beanFactory = beanFactory;
    }

    public Object getBean() {
        return bean;
    }

    public void setBean(Object bean) {
        this.bean = bean;
    }
}
