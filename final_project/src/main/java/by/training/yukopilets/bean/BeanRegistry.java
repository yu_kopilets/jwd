package by.training.yukopilets.bean;

public interface BeanRegistry {

    <T> void addBean(T bean);

    <T> void addBean(Class<T> beanClass);

    <T> T getBean(Class<T> beanClass);

    <T> void removeBean(T bean);

    void destroy();
}
