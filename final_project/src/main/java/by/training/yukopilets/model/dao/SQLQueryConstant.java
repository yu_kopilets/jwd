package by.training.yukopilets.model.dao;

public class SQLQueryConstant {

    public static final String TABLE_USER = "user_account";
    public static final String TABLE_UNIT = "unit";
    public static final String TABLE_TEXT = "text";
    public static final String TABLE_PANGRAM = "pangram";

    public static final String INSERT_QUERY = "INSERT INTO {0} ({1}) VALUES ({2})";
    public static final String UPDATE_QUERY = "UPDATE {0} SET {1} WHERE id = ?";
    public static final String SELECT_BY_ID_QUERY = "SELECT * FROM {0} WHERE id = ?";
    public static final String SELECT_ALL_QUERY = "SELECT * FROM {0}";
    public static final String DELETE_QUERY = "DELETE FROM {0} WHERE id = ?";

    public static final String INSERT_USER = "INSERT INTO user_account ({0}) VALUES ({1})";
    public static final String INSERT_USER_HAS_ROLE =
            "INSERT INTO user_account_has_role " +
                    "(user_account_id, role_id) VALUES ({0})";
    public static final String INSERT_TYPED_TEXT =
            "INSERT INTO user_history_has_text " +
                    "(user_account_id, text_id, typed_title, typed_date, typed_speed) VALUES (?,?,?,?,?)";
    public static final String INSERT_TYPED_PANGRAM =
            "INSERT INTO user_history_has_pangram " +
                    "(user_account_id, pangram_id, typed_title, typed_date, typed_speed) VALUES (?,?,?,?,?)";

    public static final String UPDATE_TEXT_ACCESSIBLE = "UPDATE text SET is_accessible = '1' WHERE id = ?";

    public static final String SELECT_UNIT_BY_HEAD = "SELECT * FROM unit WHERE unit_head = ?";
    public static final String SELECT_UNIT_BY_ID = "SELECT * FROM exercise WHERE unit_id = ?";
    public static final String SELECT_TEXT_BY_LENGTH = "SELECT * FROM text WHERE is_accessible = 1 AND text_length {0} ?";
    public static final String SELECT_TEXT_BY_USER_ID = "SELECT * FROM text WHERE user_account_id = ?";
    public static final String SELECT_TEXT_BY_ACCESSIBLE = "SELECT * FROM text WHERE is_accessible = ?";
    public static final String SELECT_PANGRAM_BY_LENGTH = "SELECT * FROM pangram WHERE pangram_length {0} ?";
    public static final String SELECT_USER_BY_LOGIN = "SELECT * FROM user_account WHERE user_login = ?";
    public static final String SELECT_USER_HAS_ROLE = "SELECT * FROM user_account_has_role WHERE user_account_id = ?";
    public static final String SELECT_TYPED_TEXT_BY_USER_ID = "SELECT * FROM user_history_has_text WHERE user_account_id = ?";
    public static final String SELECT_TYPED_PANGRAM_BY_USER_ID = "SELECT * FROM user_history_has_pangram WHERE user_account_id = ?";

    private SQLQueryConstant() {
    }
}
