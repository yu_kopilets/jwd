package by.training.yukopilets.model.service;

import by.training.yukopilets.exception.ServiceException;
import by.training.yukopilets.model.service.input.InputElementIterator;

public interface TypingService {
    InputElementIterator loadTyping(String typingType, String typingLength) throws ServiceException;
}
