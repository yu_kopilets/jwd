package by.training.yukopilets.model.dao;

import by.training.yukopilets.entity.Unit;
import by.training.yukopilets.exception.DaoException;
import by.training.yukopilets.exception.TransactionException;

import java.util.Optional;

public interface UnitDao {
    Optional<Unit> findUnitByName(String unitName) throws TransactionException, DaoException;
}
