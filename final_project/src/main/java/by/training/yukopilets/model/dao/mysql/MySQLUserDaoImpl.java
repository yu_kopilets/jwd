package by.training.yukopilets.model.dao.mysql;

import by.training.yukopilets.application.ApplicationConstant;
import by.training.yukopilets.bean.Bean;
import by.training.yukopilets.entity.user.User;
import by.training.yukopilets.entity.user.UserRank;
import by.training.yukopilets.entity.user.UserRole;
import by.training.yukopilets.exception.DaoException;
import by.training.yukopilets.exception.TransactionException;
import by.training.yukopilets.model.dao.AbstractCRUDDao;
import by.training.yukopilets.model.dao.SQLQueryConstant;
import by.training.yukopilets.model.dao.UserDao;
import by.training.yukopilets.model.dao.connection.Transaction;
import by.training.yukopilets.model.dao.connection.TransactionImpl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.MessageFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@Bean(name = ApplicationConstant.USER_DAO)
public class MySQLUserDaoImpl extends AbstractCRUDDao<User> implements UserDao {

    private final Transaction transaction;

    public MySQLUserDaoImpl() {
        super(SQLQueryConstant.TABLE_USER);
        transaction = new TransactionImpl();
    }

    @Override
    public void save(User user) throws DaoException {
        try {
            transaction.begin();
            transaction.doWithTransaction(connection -> {
                List<String> columnNames = getColumnNames();
                String columns = String.join(", ", columnNames);
                String values = columnNames.stream()
                        .map(column -> "?")
                        .collect(Collectors.joining(", "));

                String sql = MessageFormat.format(SQLQueryConstant.INSERT_USER, columns, values);
                try (PreparedStatement statement = connection.prepareStatement(sql, new String[]{"id"})) {
                    fillStatement(statement, user);
                    statement.execute();
                    ResultSet resultSet = statement.getGeneratedKeys();
                    if(resultSet.next()) {
                        user.setId(resultSet.getLong(1));
                    }
                }
            });

            saveUserRole(user);
            transaction.commit();
        } catch (TransactionException e) {
            try {
                transaction.rollback();
            } catch (TransactionException ex) {
                throw new DaoException("Failed to rollback transaction!", e);
            }
            throw new DaoException(e.getMessage(), e);
        }
    }

    private void saveUserRole(User user) throws TransactionException {
        transaction.doWithTransaction(connection -> {
            Statement statement = connection.createStatement();
            String values = user.getId() + "," + user.getRole().getId();
            String sql = MessageFormat.format(SQLQueryConstant.INSERT_USER_HAS_ROLE, values);
            statement.execute(sql);
        });
    }

    public Optional<User> findUserByLogin(String login) throws DaoException {
        try {
            AtomicReference<User> result = new AtomicReference<>();
            transaction.begin();
            transaction.doWithTransaction(connection -> {
                try (PreparedStatement statement =
                             connection.prepareStatement(SQLQueryConstant.SELECT_USER_BY_LOGIN)) {
                    statement.setString(1, login);
                    ResultSet resultSet = statement.executeQuery();

                    if (resultSet.next()) {
                        result.set(processResultSet(resultSet));
                    }
                }
            });

            Set<UserRole> roles = findUserRoles(result.get().getId());

            if (roles.size() > 1) {
                result.get().setRole(UserRole.MODERATOR);
            } else {
                result.get().setRole(UserRole.USER);
            }

            transaction.commit();
            return Optional.of(result.get());
        } catch (TransactionException e) {
            try {
                transaction.rollback();
            } catch (TransactionException ex) {
                throw new DaoException("Failed to rollback transaction!", e);
            }
            throw new DaoException(e.getMessage(), e);
        }
    }

    private Set<UserRole> findUserRoles(Long userId) throws TransactionException {
        Set<UserRole> roles = new HashSet<>();
        transaction.doWithTransaction(connection -> {
            try (PreparedStatement statement =
                         connection.prepareStatement(SQLQueryConstant.SELECT_USER_HAS_ROLE)) {
                statement.setLong(1, userId);
                ResultSet resultSet = statement.executeQuery();

                while (resultSet.next()) {
                    roles.add(UserRole.fromId(Integer.parseInt(resultSet.getString("role_id"))));
                }
            }
        });
        return roles;
    }

    @Override
    protected List<String> getColumnNames() {
        List<String> columnNames = new ArrayList<>();
        columnNames.add("user_login");
        columnNames.add("user_password");
        columnNames.add("user_email");
        columnNames.add("is_active");
        columnNames.add("user_rank_id");
        return columnNames;
    }

    @Override
    protected void fillStatement(PreparedStatement statement, User entity) throws SQLException {
        int i = 1;
        statement.setString(i++, entity.getLogin());
        statement.setString(i++, entity.getPassword());
        statement.setString(i++, entity.getEmail());
        statement.setBoolean(i++, entity.isActive());
        statement.setInt(i, entity.getRank().getId());
    }

    @Override
    protected User processResultSet(ResultSet resultSet) throws SQLException {
        return new User(
                resultSet.getLong("id"),
                resultSet.getString("user_login"),
                resultSet.getString("user_password"),
                resultSet.getString("user_email"),
                resultSet.getBoolean("is_active"),
                null,
                UserRank.fromId(resultSet.getInt("user_rank_id"))
        );
    }
}
