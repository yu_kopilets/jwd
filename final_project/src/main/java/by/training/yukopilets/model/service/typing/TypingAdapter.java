package by.training.yukopilets.model.service.typing;

import by.training.yukopilets.exception.ServiceException;
import by.training.yukopilets.model.dao.TypingEntity;

import java.util.List;
import java.util.Random;

public interface TypingAdapter {

    List<TypingEntity> request(String typingLength) throws ServiceException;

    default String findSignFromLength(String typingLength, Random random) {
        String findSign;
        switch (typingLength) {
            case "short":
                findSign = "<";
                break;

            case "long":
                findSign = ">=";
                break;

            default:
                int rand = random.nextInt(2) + 1;
                if(rand == 1) {
                    findSign = "<";
                } else {
                    findSign = ">=";
                }
        }
        return findSign;
    }
}
