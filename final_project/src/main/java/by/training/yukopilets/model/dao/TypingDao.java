package by.training.yukopilets.model.dao;

import by.training.yukopilets.exception.DaoException;

import java.util.List;

public interface TypingDao<T extends TypingEntity> {
    List<T> findAllByLength(String findSign, int length) throws DaoException;
}
