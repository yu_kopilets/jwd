package by.training.yukopilets.model.service.input.impl;

import by.training.yukopilets.application.SecurityContext;
import by.training.yukopilets.entity.Pangram;
import by.training.yukopilets.entity.Text;
import by.training.yukopilets.entity.TypedPangram;
import by.training.yukopilets.entity.TypedText;
import by.training.yukopilets.entity.user.User;
import by.training.yukopilets.exception.ServiceException;
import by.training.yukopilets.model.service.impl.UserHistoryServiceImpl;
import by.training.yukopilets.model.service.input.InputElementObserver;
import by.training.yukopilets.model.dao.TypingEntity;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDate;
import java.util.Date;
import java.util.Optional;

public class InputTypingObserverImpl implements InputElementObserver {

    private static final Logger LOGGER = LogManager.getLogger();

    private final UserHistoryServiceImpl userHistoryService;
    private final TypingEntity typingEntity;
    private final long startTime;

    public InputTypingObserverImpl(UserHistoryServiceImpl userHistoryService, TypingEntity typingEntity) {
        this.userHistoryService = userHistoryService;
        this.typingEntity = typingEntity;
        Date date = new Date();
        startTime = date.getTime();
    }

    @Override
    public void observe() {
        Optional<User> currentUser = SecurityContext.getInstance().getCurrentUser();
        if (currentUser.isPresent()) {
            Date date = new Date();
            long endTime = date.getTime();
            double speed = (endTime - startTime) / 1000.0;
            LocalDate typedDate = LocalDate.now();

            try {
                if (typingEntity instanceof Text) {
                    TypedText typedText = new TypedText(
                            currentUser.get().getId(),
                            ((Text) typingEntity).getId(),
                            typingEntity.getHead(),
                            typedDate.toString(),
                            speed);
                    userHistoryService.saveTypedText(typedText);
                } else if (typingEntity instanceof Pangram) {
                    TypedPangram typedPangram = new TypedPangram(
                            currentUser.get().getId(),
                            ((Pangram) typingEntity).getId(),
                            typingEntity.getHead(),
                            typedDate.toString(),
                            speed);
                    userHistoryService.saveTypedPangram(typedPangram);
                }
            } catch (ServiceException e) {
                LOGGER.error("Failed to save typed typing!", e);
            }
        }
    }
}
