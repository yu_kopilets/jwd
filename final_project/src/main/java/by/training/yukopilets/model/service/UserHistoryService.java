package by.training.yukopilets.model.service;

import by.training.yukopilets.entity.TypedPangram;
import by.training.yukopilets.entity.TypedText;
import by.training.yukopilets.entity.user.UserHistory;
import by.training.yukopilets.exception.ServiceException;

public interface UserHistoryService {

    void saveTypedText(TypedText typedText) throws ServiceException;

    void saveTypedPangram(TypedPangram typedPangram) throws ServiceException;

    UserHistory getCurrentUserHistory() throws ServiceException;
}
