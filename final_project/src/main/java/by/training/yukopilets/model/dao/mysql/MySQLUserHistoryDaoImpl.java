package by.training.yukopilets.model.dao.mysql;

import by.training.yukopilets.application.ApplicationConstant;
import by.training.yukopilets.bean.Bean;
import by.training.yukopilets.entity.TypedPangram;
import by.training.yukopilets.entity.TypedText;
import by.training.yukopilets.entity.user.UserHistory;
import by.training.yukopilets.exception.DaoException;
import by.training.yukopilets.model.dao.SQLQueryConstant;
import by.training.yukopilets.model.dao.UserHistoryDao;
import by.training.yukopilets.model.dao.connection.ConnectionManager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Bean(name = ApplicationConstant.USER_HISTORY_DAO)
public class MySQLUserHistoryDaoImpl implements UserHistoryDao {

    @Override
    public void saveTypedText(TypedText typedText) throws DaoException {
        try {
            ConnectionManager.getInstance().doWithConnection(connection -> {
                try (PreparedStatement statement =
                             connection.prepareStatement(SQLQueryConstant.INSERT_TYPED_TEXT)) {
                    int i = 1;
                    statement.setLong(i++, typedText.getUserId());
                    statement.setLong(i++, typedText.getTextId());
                    statement.setString(i++, typedText.getTypedTitle());
                    statement.setString(i++, typedText.getTypedDate());
                    statement.setDouble(i, typedText.getTypedSpeed());
                    statement.executeUpdate();
                }
            });
        } catch (SQLException e) {
            throw new DaoException("Failed to complete action with connection!", e);
        }
    }

    @Override
    public void saveTypedPangram(TypedPangram typedPangram) throws DaoException {
        try {
            ConnectionManager.getInstance().doWithConnection(connection -> {
                try (PreparedStatement statement =
                             connection.prepareStatement(SQLQueryConstant.INSERT_TYPED_PANGRAM)) {
                    int i = 1;
                    statement.setLong(i++, typedPangram.getUserId());
                    statement.setLong(i++, typedPangram.getPangramId());
                    statement.setString(i++, typedPangram.getTypedTitle());
                    statement.setString(i++, typedPangram.getTypedDate());
                    statement.setDouble(i, typedPangram.getTypedSpeed());
                    statement.executeUpdate();
                }
            });
        } catch (SQLException e) {
            throw new DaoException("Failed to complete action with connection!", e);
        }
    }

    @Override
    public UserHistory findUserHistoryById(Long userId) throws DaoException {
        List<TypedText> typedTexts = findAllTypedTexts(userId);
        List<TypedPangram> typedPangrams = findAllTypedPangrams(userId);
        UserHistory userHistory = new UserHistory(userId, typedTexts, typedPangrams);
        return userHistory;
    }

    public List<TypedText> findAllTypedTexts(Long userId) throws DaoException {
        try {
            List<TypedText> result = new ArrayList<>();

            ConnectionManager.getInstance().doWithConnection(connection -> {
                try (PreparedStatement statement =
                             connection.prepareStatement(SQLQueryConstant.SELECT_TYPED_TEXT_BY_USER_ID)) {
                    statement.setLong(1, userId);
                    ResultSet resultSet = statement.executeQuery();
                    while (resultSet.next()) {
                        TypedText typedText = new TypedText(
                                resultSet.getLong("user_account_id"),
                                resultSet.getLong("text_id"),
                                resultSet.getString("typed_title"),
                                resultSet.getString("typed_date"),
                                resultSet.getDouble("typed_speed")
                        );
                        result.add(typedText);
                    }
                }
            });
            return result;
        } catch (SQLException e) {
            throw new DaoException("Failed to complete action with connection!", e);
        }
    }

    public List<TypedPangram> findAllTypedPangrams(Long userId) throws DaoException {
        try {
            List<TypedPangram> result = new ArrayList<>();

            ConnectionManager.getInstance().doWithConnection(connection -> {
                try (PreparedStatement statement =
                             connection.prepareStatement(SQLQueryConstant.SELECT_TYPED_PANGRAM_BY_USER_ID)) {
                    statement.setLong(1, userId);
                    ResultSet resultSet = statement.executeQuery();
                    while (resultSet.next()) {
                        TypedPangram typedPangram = new TypedPangram(
                                resultSet.getLong("user_account_id"),
                                resultSet.getLong("pangram_id"),
                                resultSet.getString("typed_title"),
                                resultSet.getString("typed_date"),
                                resultSet.getDouble("typed_speed")
                        );
                        result.add(typedPangram);
                    }
                }
            });
            return result;
        } catch (SQLException e) {
            throw new DaoException("Failed to complete action with connection!", e);
        }
    }
}
