package by.training.yukopilets.model.service.typing;

import by.training.yukopilets.application.ApplicationConstant;
import by.training.yukopilets.bean.Bean;
import by.training.yukopilets.model.service.typing.impl.PangramTypingAdapterImpl;
import by.training.yukopilets.model.service.typing.impl.TextTypingAdapterImpl;

import java.util.Random;

@Bean(name = ApplicationConstant.TYPING_ADAPTER_FACTORY)
public class TypingAdapterFactory {

    private final PangramTypingAdapterImpl pangramTypingAdapter;
    private final TextTypingAdapterImpl textTypingAdapter;
    private final Random random;

    public TypingAdapterFactory(PangramTypingAdapterImpl pangramTypingAdapter, TextTypingAdapterImpl textTypingAdapter) {
        this.pangramTypingAdapter = pangramTypingAdapter;
        this.textTypingAdapter = textTypingAdapter;
        random = new Random();
    }

    public TypingAdapter createTypingAdapter(String typingType) {
        TypingAdapter adapter;
        switch (typingType) {
            case "pangram":
                adapter = pangramTypingAdapter;
                break;

            case "text":
                adapter = textTypingAdapter;
                break;

            default:
                int rand = random.nextInt(2) + 1;
                if (rand == 1) {
                    adapter = pangramTypingAdapter;
                } else {
                    adapter = textTypingAdapter;
                }
        }
        return adapter;
    }
}
