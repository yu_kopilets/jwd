package by.training.yukopilets.model.service.impl;

import by.training.yukopilets.application.ApplicationConstant;
import by.training.yukopilets.application.ApplicationContext;
import by.training.yukopilets.bean.Bean;
import by.training.yukopilets.entity.Exercise;
import by.training.yukopilets.exception.DaoException;
import by.training.yukopilets.exception.ServiceException;
import by.training.yukopilets.exception.TransactionException;
import by.training.yukopilets.model.dao.connection.Transaction;
import by.training.yukopilets.model.dao.connection.TransactionImpl;
import by.training.yukopilets.model.dao.mysql.MySQLExerciseDaoImpl;
import by.training.yukopilets.model.dao.mysql.MySQLUnitDaoImpl;
import by.training.yukopilets.entity.Unit;
import by.training.yukopilets.model.service.UnitService;
import by.training.yukopilets.model.service.input.CurrentInputElement;
import by.training.yukopilets.model.service.input.InputElementIterator;
import by.training.yukopilets.model.service.input.impl.InputUnitObserverImpl;

import java.util.ArrayDeque;
import java.util.List;
import java.util.Optional;
import java.util.Queue;

@Bean(name = ApplicationConstant.UNIT_SERVICE)
public class UnitServiceImpl implements UnitService {

    private final MySQLUnitDaoImpl unitDao;
    private final MySQLExerciseDaoImpl exerciseDao;
    private final Transaction transaction;

    public UnitServiceImpl(MySQLUnitDaoImpl unitDao, MySQLExerciseDaoImpl exerciseDao) {
        this.unitDao = unitDao;
        this.exerciseDao = exerciseDao;
        transaction = new TransactionImpl();
    }

    public InputElementIterator loadUnit(String unitName) throws ServiceException {
        try {
            transaction.begin(unitDao, exerciseDao);
            Optional<Unit> unit = unitDao.findUnitByName(unitName);
            if (!unit.isPresent()) {
                throw new ServiceException("Failed to find unit " + unitName);
            }
            List<Exercise> exercisesByUnitId = exerciseDao.getExercisesByUnitId(unit.get().getId());
            transaction.commit();
            transaction.end();

            Queue<String> exercises = new ArrayDeque<>();
            exercisesByUnitId.forEach(exercise -> exercises.add(exercise.getBody()));
            UserServiceImpl userService = ApplicationContext.getInstance().getBean(UserServiceImpl.class);
            InputUnitObserverImpl unitObserver = new InputUnitObserverImpl(userService, unit.get());
            String description = unit.get().getHead() + " done!";

            InputElementIterator currentUnit = new CurrentInputElement(
                    unit.get().getHead(),
                    exercises,
                    description,
                    unitObserver
            );
            return currentUnit;
        } catch (TransactionException e) {
            try {
                transaction.rollback();
            } catch (TransactionException rollbackTransaction) {
                throw new ServiceException("Transaction fail to rollback!", rollbackTransaction);
            }
            try {
                transaction.end();
            } catch (TransactionException endTransaction) {
                throw new ServiceException("Transaction fail to end!", endTransaction);
            }
            throw new ServiceException("Transaction failed!", e);
        } catch (DaoException e) {
            throw new ServiceException("Failed to find unit or exercises!", e);
        }
    }
}
