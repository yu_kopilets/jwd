package by.training.yukopilets.model.dao.connection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

public class DataSourceImpl implements DataSource {

    private static final Logger LOGGER = LogManager.getLogger();

    private DataSourceConfig dsConfig;
    private BlockingQueue<Connection> availableConnections;
    private List<Connection> usedConnections;

    private DataSourceImpl() {
    }

    public static void initialize() {
        DatasourceHolder.instance.initDatasourceConfig();
        DatasourceHolder.instance.initConnections();
    }

    private void initDatasourceConfig() {
        dsConfig = new DataSourceConfig("datasource.properties", 10);
        try {
            Class.forName(dsConfig.getDriverClassName()).newInstance();
        } catch (ClassNotFoundException e) {
            LOGGER.error("Driver class not found!", e);
        } catch (IllegalAccessException e) {
            LOGGER.error("Illegal access to Driver class!", e);
        } catch (InstantiationException e) {
            LOGGER.error("Instantiation exception in driver registration!", e);
        }
    }

    private void initConnections() {
        availableConnections = new LinkedBlockingDeque<>();
        usedConnections = new ArrayList<>();
        for (int i = 0; i < dsConfig.getPoolCapacity(); i++) {
            try {
                availableConnections.add(
                        DriverManager.getConnection(
                                dsConfig.getUrl(),
                                dsConfig.getUsername(),
                                dsConfig.getPassword()
                        )
                );
            } catch (SQLException e) {
                LOGGER.error("Failed to create connections in datasource!", e);
            }
        }
    }

    @Override
    public Connection getConnection() throws SQLException {
        try {
            Connection connection = availableConnections.take();
            usedConnections.add(connection);
            return createProxyConnection(connection);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new SQLException("Failed to get connection!", e);
        }
    }

    private Connection createProxyConnection(Connection connection) {
        return (Connection) Proxy.newProxyInstance(connection.getClass().getClassLoader(),
                new Class[]{Connection.class},
                (proxy, method, args) -> {
                    if ("close".equals(method.getName())) {
                        releaseConnection(connection);
                        return null;
                    } else {
                        return method.invoke(connection, args);
                    }
                });
    }

    public void releaseConnection(Connection connection) {
        try {
            if (!connection.getAutoCommit()) {
                connection.setAutoCommit(true);
            }
            if (usedConnections.contains(connection)) {
                usedConnections.remove(connection);
                availableConnections.put(connection);
            }
        } catch (SQLException e) {
            LOGGER.error("Fail in setting autocommit!", e);
        } catch (InterruptedException e) {
            LOGGER.error("Failed to put connection in available que!", e);
            Thread.currentThread().interrupt();
        }
    }

    public static DataSourceImpl getInstance() {
        return DatasourceHolder.instance;
    }

    private static class DatasourceHolder {
        private static final DataSourceImpl instance = new DataSourceImpl();
    }
}
