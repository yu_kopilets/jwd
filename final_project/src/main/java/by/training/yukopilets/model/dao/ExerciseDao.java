package by.training.yukopilets.model.dao;

import by.training.yukopilets.entity.Exercise;
import by.training.yukopilets.exception.DaoException;
import by.training.yukopilets.exception.TransactionException;

import java.sql.SQLException;
import java.util.List;

public interface ExerciseDao {
    List<Exercise> getExercisesByUnitId(Long unitId) throws DaoException, SQLException, TransactionException;
}
