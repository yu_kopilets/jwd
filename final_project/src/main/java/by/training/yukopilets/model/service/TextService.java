package by.training.yukopilets.model.service;

import by.training.yukopilets.entity.Text;
import by.training.yukopilets.exception.ServiceException;

import java.util.List;

public interface TextService {

    void uploadText(String head, String body, String description) throws ServiceException;

    void submitText(List<Long> identifiers) throws ServiceException;

    List<Text> getAllCurrentUserTexts() throws ServiceException;

    List<Text> getAllInaccessibleTexts() throws ServiceException;
}
