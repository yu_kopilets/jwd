package by.training.yukopilets.model.dao;

public interface TypingEntity {

    String getHead();

    void setHead(String head);

    String getBody();

    void setBody(String body);

    String getDescription();
}
