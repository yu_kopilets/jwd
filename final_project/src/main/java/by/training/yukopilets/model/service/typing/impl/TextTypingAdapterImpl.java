package by.training.yukopilets.model.service.typing.impl;

import by.training.yukopilets.application.ApplicationConstant;
import by.training.yukopilets.bean.Bean;
import by.training.yukopilets.entity.Text;
import by.training.yukopilets.exception.DaoException;
import by.training.yukopilets.exception.ServiceException;
import by.training.yukopilets.model.dao.TypingEntity;
import by.training.yukopilets.model.dao.mysql.MySQLTextDaoImpl;
import by.training.yukopilets.model.service.typing.TypingAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Bean(name = ApplicationConstant.TEXT_TYPING_ADAPTER)
public class TextTypingAdapterImpl implements TypingAdapter {

    private final MySQLTextDaoImpl textDao;
    private final Random random;

    public TextTypingAdapterImpl(MySQLTextDaoImpl textDao) {
        this.textDao = textDao;
        random = new Random();
    }

    @Override
    public List<TypingEntity> request(String typingLength) throws ServiceException {
        try {
            int length = 350;
            String findSign = findSignFromLength(typingLength, random);
            List<Text> texts = textDao.findAllByLength(findSign, length);
            List<TypingEntity> typings = new ArrayList<>(texts);
            return typings;
        } catch (DaoException e) {
            throw new ServiceException("Failed to find texts!", e);
        }
    }
}
