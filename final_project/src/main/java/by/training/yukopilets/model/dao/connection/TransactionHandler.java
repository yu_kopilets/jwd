package by.training.yukopilets.model.dao.connection;

public interface TransactionHandler {

    void setTransaction(Transaction transaction);

    void releaseTransaction();
}
