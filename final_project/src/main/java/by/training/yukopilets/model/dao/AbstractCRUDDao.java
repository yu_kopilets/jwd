package by.training.yukopilets.model.dao;

import by.training.yukopilets.exception.DaoException;
import by.training.yukopilets.model.dao.connection.ConnectionManager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public abstract class AbstractCRUDDao<T extends IdentifiedEntity> implements CRUDDao<T> {

    private final ConnectionManager connectionManager = ConnectionManager.getInstance();
    private final String tableName;

    public AbstractCRUDDao(String tableName) {
        this.tableName = tableName;
    }

    @Override
    public void save(T entity) throws DaoException {
        try {
            connectionManager.doWithConnection(connection -> {

                List<String> columnNames = getColumnNames();
                String columns = String.join(", ", columnNames);
                String values = columnNames.stream()
                        .map(column -> "?")
                        .collect(Collectors.joining(", "));

                String sql = MessageFormat.format(SQLQueryConstant.INSERT_QUERY, tableName, columns, values);
                try (PreparedStatement statement = connection.prepareStatement(sql)) {
                    fillStatement(statement, entity);
                    statement.executeUpdate();
                }
            });
        } catch (SQLException e) {
            throw new DaoException("Failed to complete action with connection!", e);
        }
    }

    @Override
    public void update(T entity) throws DaoException {
        try {
            connectionManager.doWithConnection(connection -> {

                List<String> columnNames = getColumnNames();
                String columns = columnNames.stream()
                        .map(column -> column + " = ?")
                        .collect(Collectors.joining(", "));

                String sql = MessageFormat.format(SQLQueryConstant.UPDATE_QUERY, tableName, columns);
                try (PreparedStatement statement = connection.prepareStatement(sql)) {
                    fillStatement(statement, entity);
                    statement.executeUpdate();
                }
            });
        } catch (SQLException e) {
            throw new DaoException("Failed to complete action with connection!", e);
        }
    }

    @Override
    public Optional<T> findById(Long id) throws DaoException {
        try {
            AtomicReference<T> result = new AtomicReference<>();
            connectionManager.doWithConnection(connection -> {

                String sql = MessageFormat.format(SQLQueryConstant.SELECT_BY_ID_QUERY, tableName);
                try (PreparedStatement statement = connection.prepareStatement(sql)) {
                    statement.setLong(1, id);
                    ResultSet resultSet = statement.executeQuery();
                    if (resultSet.next()) {
                        result.set(processResultSet(resultSet));
                    }
                }
            });
            return Optional.of(result.get());
        } catch (SQLException e) {
            throw new DaoException("Failed to complete action with connection!", e);
        }
    }

    @Override
    public List<T> findAll() throws DaoException {
        try {
            List<T> result = new ArrayList<>();
            connectionManager.doWithConnection(connection -> {

                String sql = MessageFormat.format(SQLQueryConstant.SELECT_ALL_QUERY, tableName);
                try (PreparedStatement statement = connection.prepareStatement(sql)) {
                    ResultSet resultSet = statement.executeQuery();
                    if (resultSet.next()) {
                        result.add(processResultSet(resultSet));
                    }
                }
            });
            return result;
        } catch (SQLException e) {
            throw new DaoException("Failed to complete action with connection!", e);
        }
    }

    @Override
    public void delete(T entity) throws DaoException {
        try {
            connectionManager.doWithConnection(connection -> {

                String sql = MessageFormat.format(SQLQueryConstant.DELETE_QUERY, tableName);
                try (PreparedStatement statement = connection.prepareStatement(sql)) {
                    statement.setLong(1, entity.getId());
                    statement.executeUpdate();
                }
            });
        } catch (SQLException e) {
            throw new DaoException("Failed to complete action with connection!", e);
        }
    }

    abstract protected List<String> getColumnNames();

    abstract protected void fillStatement(PreparedStatement statement, T entity) throws SQLException;

    abstract protected T processResultSet(ResultSet resultSet) throws SQLException;
}
