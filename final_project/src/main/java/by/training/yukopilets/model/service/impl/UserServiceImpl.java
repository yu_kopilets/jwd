package by.training.yukopilets.model.service.impl;

import by.training.yukopilets.application.ApplicationConstant;
import by.training.yukopilets.bean.Bean;
import by.training.yukopilets.exception.DaoException;
import by.training.yukopilets.exception.ServiceException;
import by.training.yukopilets.entity.user.User;
import by.training.yukopilets.entity.user.UserRank;
import by.training.yukopilets.entity.user.UserRole;
import by.training.yukopilets.model.dao.mysql.MySQLUserDaoImpl;
import by.training.yukopilets.model.service.UserService;

import java.util.Optional;

@Bean(name = ApplicationConstant.USER_SERVICE)
public class UserServiceImpl implements UserService {

    private final MySQLUserDaoImpl userDao;

    public UserServiceImpl(MySQLUserDaoImpl userDao) {
        this.userDao = userDao;
    }

    public void createUser(String login, String password, String email) throws ServiceException {
        try {
            User user = new User(null, login, password, email, true, UserRole.USER, UserRank.BEGINNER);
            userDao.save(user);
        } catch (DaoException e) {
            throw new ServiceException("Failed to save new user!", e);
        }
    }

    public void updateUser(User user) throws ServiceException {
        try {
            userDao.update(user);
        } catch (DaoException e) {
            throw new ServiceException("Failed to update user!", e);
        }
    }

    public User getUserByLogin(String login, String password) throws ServiceException {
        try {
            Optional<User> user = userDao.findUserByLogin(login);
            if(!user.isPresent() || !password.equals(user.get().getPassword())) {
                throw new ServiceException("Incorrect login or password!");
            }
            return user.get();
        } catch (DaoException e) {
            throw new ServiceException("Failed to find user by login!", e);
        }
    }
}
