package by.training.yukopilets.model.service;

import by.training.yukopilets.entity.user.User;
import by.training.yukopilets.exception.ServiceException;

public interface UserService {

    void createUser(String login, String password, String email) throws ServiceException;

    void updateUser(User user) throws ServiceException;

    User getUserByLogin(String login, String password) throws ServiceException;
}
