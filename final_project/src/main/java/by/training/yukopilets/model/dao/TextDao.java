package by.training.yukopilets.model.dao;

import by.training.yukopilets.entity.Text;
import by.training.yukopilets.exception.DaoException;

import java.util.List;

public interface TextDao {

    void updateTextAccessible(List<Long> identifiers) throws DaoException;

    List<Text> findAllByUserId(Long userId) throws DaoException;

    List<Text> findAllByAccessible(Boolean isAccessible) throws DaoException;
}
