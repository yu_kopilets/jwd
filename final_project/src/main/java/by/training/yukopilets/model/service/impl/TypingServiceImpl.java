package by.training.yukopilets.model.service.impl;

import by.training.yukopilets.application.ApplicationConstant;
import by.training.yukopilets.application.ApplicationContext;
import by.training.yukopilets.bean.Bean;
import by.training.yukopilets.exception.ServiceException;
import by.training.yukopilets.model.service.TypingService;
import by.training.yukopilets.model.service.input.CurrentInputElement;
import by.training.yukopilets.model.service.input.InputElementIterator;
import by.training.yukopilets.model.service.input.impl.InputTypingObserverImpl;
import by.training.yukopilets.model.service.typing.TypingAdapter;
import by.training.yukopilets.model.service.typing.TypingAdapterFactory;
import by.training.yukopilets.model.dao.TypingEntity;

import java.util.*;

@Bean(name = ApplicationConstant.TYPING_SERVICE)
public class TypingServiceImpl implements TypingService {

    private final TypingAdapterFactory adapterFactory;
    private final Random random;

    public TypingServiceImpl(TypingAdapterFactory adapterFactory) {
        this.adapterFactory = adapterFactory;
        random = new Random();
    }

    public InputElementIterator loadTyping(String typingType, String typingLength) throws ServiceException {
        TypingAdapter adapter = adapterFactory.createTypingAdapter(typingType);
        List<TypingEntity> typings = adapter.request(typingLength);
        TypingEntity typing = getRandomTyping(typings);

        Queue<String> body = new ArrayDeque<>(Arrays.asList(typing.getBody().split("\\n")));
        UserHistoryServiceImpl userHistoryService = ApplicationContext.getInstance().getBean(UserHistoryServiceImpl.class);
        InputTypingObserverImpl typingObserver = new InputTypingObserverImpl(userHistoryService, typing);

        InputElementIterator currentTyping = new CurrentInputElement(
                typing.getHead(),
                body,
                typing.getDescription(),
                typingObserver
        );
        return currentTyping;
    }

    private TypingEntity getRandomTyping(List<TypingEntity> typings) {
        int bound = typings.size();
        int rand = random.nextInt(bound);
        return typings.get(rand);
    }
}
