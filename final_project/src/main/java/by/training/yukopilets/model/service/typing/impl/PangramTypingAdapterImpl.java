package by.training.yukopilets.model.service.typing.impl;

import by.training.yukopilets.application.ApplicationConstant;
import by.training.yukopilets.bean.Bean;
import by.training.yukopilets.entity.Pangram;
import by.training.yukopilets.exception.DaoException;
import by.training.yukopilets.exception.ServiceException;
import by.training.yukopilets.model.dao.TypingEntity;
import by.training.yukopilets.model.dao.mysql.MySQLPangramDaoImpl;
import by.training.yukopilets.model.service.typing.TypingAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Bean(name = ApplicationConstant.PANGRAM_TYPING_ADAPTER)
public class PangramTypingAdapterImpl implements TypingAdapter {

    private final MySQLPangramDaoImpl pangramDao;
    private final Random random;

    public PangramTypingAdapterImpl(MySQLPangramDaoImpl pangramDao) {
        this.pangramDao = pangramDao;
        random = new Random();
    }

    @Override
    public List<TypingEntity> request(String typingLength) throws ServiceException {
        try {
            int length = 80;
            String findSign = findSignFromLength(typingLength, random);
            List<Pangram> pangrams = pangramDao.findAllByLength(findSign, length);
            List<TypingEntity> typings = new ArrayList<>(pangrams);
            return typings;
        } catch (DaoException e) {
            throw new ServiceException("Failed to find pangrams!", e);
        }
    }
}
