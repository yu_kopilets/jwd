package by.training.yukopilets.model.dao.connection;

import java.sql.Connection;
import java.sql.SQLException;

public class ConnectionManager {

    private static final ConnectionManager INSTANCE = new ConnectionManager();

    private ConnectionManager() {
    }

    public void doWithConnection(ConnectionConsumer consumer) throws SQLException {
        Connection connection = getConnection();
        consumer.accept(connection);
        connection.close();
    }

    private Connection getConnection() throws SQLException {
        return DataSourceImpl.getInstance().getConnection();
    }

    public static ConnectionManager getInstance() {
        return INSTANCE;
    }
}
