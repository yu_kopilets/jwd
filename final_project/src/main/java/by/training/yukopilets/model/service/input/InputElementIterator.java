package by.training.yukopilets.model.service.input;

import java.util.Iterator;

/**
 * An InputElementIterator contains methods for providing typing <i>InputElement</i> samples to user.
 * Big texts or units have long typing body. For convenience, the body is divided into several parts
 * and provided in turn.
 *
 * @author Yuriy Kopilets
 * @see Iterator
 */
public interface InputElementIterator extends Iterator<String> {

    /**
     * This method is used to get a typing sample's head.
     *
     * @return head of typing sample
     */
    String getHead();

    /**
     * This method is used to get a typing sample's description.
     * Description need to show information about typing sample after input.
     *
     * @return description of typing sample
     */
    String getDescription();

    /**
     * Return <tt>true</tt> if there are any body's part to input.
     *
     * @return <tt>true</tt> if there are any body's part to input
     */
    @Override
    boolean hasNext();

    /**
     * Return next part of body.
     *
     * @return Next part of body is it exists
     */
    @Override
    String next();

    /**
     * This method is used when input is finish.
     *
     */
    void finishInput();
}
