package by.training.yukopilets.model.service.input;

public interface InputElementObserver {
    void observe();
}
