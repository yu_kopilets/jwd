package by.training.yukopilets.model.dao.mysql;

import by.training.yukopilets.application.ApplicationConstant;
import by.training.yukopilets.bean.Bean;
import by.training.yukopilets.entity.Pangram;
import by.training.yukopilets.exception.DaoException;
import by.training.yukopilets.model.dao.AbstractCRUDDao;
import by.training.yukopilets.model.dao.SQLQueryConstant;
import by.training.yukopilets.model.dao.TypingDao;
import by.training.yukopilets.model.dao.connection.ConnectionManager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

@Bean(name = ApplicationConstant.PANGRAM_DAO)
public class MySQLPangramDaoImpl extends AbstractCRUDDao<Pangram> implements TypingDao<Pangram> {

    public MySQLPangramDaoImpl() {
        super(SQLQueryConstant.TABLE_PANGRAM);
    }

    public List<Pangram> findAllByLength(String findSign, int length) throws DaoException {
        try {
            List<Pangram> result = new ArrayList<>();

            ConnectionManager.getInstance().doWithConnection(connection -> {

                String sql = MessageFormat.format(SQLQueryConstant.SELECT_PANGRAM_BY_LENGTH, findSign);

                try (PreparedStatement statement = connection.prepareStatement(sql)) {
                    statement.setInt(1, length);
                    ResultSet resultSet = statement.executeQuery();
                    while (resultSet.next()) {
                        result.add(processResultSet(resultSet));
                    }
                }
            });
            return result;
        } catch (SQLException e) {
            throw new DaoException("Failed to complete action with connection!", e);
        }
    }

    @Override
    protected List<String> getColumnNames() {
        List<String> columnNames = new ArrayList<>();
        columnNames.add("pangram_length");
        columnNames.add("pangram_head");
        columnNames.add("pangram_body");
        return columnNames;
    }

    @Override
    protected void fillStatement(PreparedStatement statement, Pangram entity) throws SQLException {
        int i = 1;
        statement.setInt(i++, entity.getLength());
        statement.setString(i++, entity.getHead());
        statement.setString(i, entity.getBody());
    }

    @Override
    protected Pangram processResultSet(ResultSet resultSet) throws SQLException {
        return new Pangram(
                resultSet.getLong("id"),
                resultSet.getInt("pangram_length"),
                resultSet.getString("pangram_head"),
                resultSet.getString("pangram_body")
        );
    }
}
