package by.training.yukopilets.model.service.impl;

import by.training.yukopilets.application.ApplicationConstant;
import by.training.yukopilets.application.SecurityContext;
import by.training.yukopilets.bean.Bean;
import by.training.yukopilets.entity.Text;
import by.training.yukopilets.entity.user.User;
import by.training.yukopilets.entity.user.UserRole;
import by.training.yukopilets.exception.DaoException;
import by.training.yukopilets.exception.ServiceException;
import by.training.yukopilets.model.dao.mysql.MySQLTextDaoImpl;
import by.training.yukopilets.model.service.TextService;

import java.util.List;
import java.util.Optional;

@Bean(name = ApplicationConstant.TEXT_SERVICE)
public class TextServiceImpl implements TextService {

    private final MySQLTextDaoImpl textDao;

    public TextServiceImpl(MySQLTextDaoImpl textDao) {
        this.textDao = textDao;
    }

    @Override
    public void uploadText(String head, String body, String description) throws ServiceException {
        try {
            Optional<User> currentUser = SecurityContext.getInstance().getCurrentUser();
            if(currentUser.isPresent()) {
                Text text = new Text(
                        null,
                        currentUser.get().getId(),
                        body.length(),
                        head,
                        body,
                        description,
                        false
                );
                textDao.save(text);
            } else {
                throw new ServiceException("User isn't logged in!");
            }
        } catch (DaoException e) {
            throw new ServiceException("Failed to save text!", e);
        }
    }

    @Override
    public void submitText(List<Long> identifiers) throws ServiceException {
        try {
            textDao.updateTextAccessible(identifiers);
        } catch (DaoException e) {
            throw new ServiceException("Failed to update text accessible!", e);
        }
    }

    @Override
    public List<Text> getAllCurrentUserTexts() throws ServiceException {
        try {
            Optional<User> currentUser = SecurityContext.getInstance().getCurrentUser();
            if (currentUser.isPresent()) {
                List<Text> texts = textDao.findAllByUserId(currentUser.get().getId());
                return texts;
            } else {
                throw new ServiceException("Current user isn't logged in!");
            }
        } catch (DaoException e) {
            throw new ServiceException("Failed to find user's texts!", e);
        }
    }

    @Override
    public List<Text> getAllInaccessibleTexts() throws ServiceException {
        try {
            Optional<User> currentUser = SecurityContext.getInstance().getCurrentUser();
            if (currentUser.isPresent() && currentUser.get().getRole() == UserRole.MODERATOR) {
                List<Text> texts = textDao.findAllByAccessible(false);
                return texts;
            } else {
                throw new ServiceException("User isn't moderator!");
            }
        } catch (DaoException e) {
            throw new ServiceException("Failed to find inaccessible texts!", e);
        }
    }
}
