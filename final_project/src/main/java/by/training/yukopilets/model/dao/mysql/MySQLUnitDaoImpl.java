package by.training.yukopilets.model.dao.mysql;

import by.training.yukopilets.application.ApplicationConstant;
import by.training.yukopilets.bean.Bean;
import by.training.yukopilets.entity.Unit;
import by.training.yukopilets.entity.user.UserRank;
import by.training.yukopilets.exception.DaoException;
import by.training.yukopilets.exception.TransactionException;
import by.training.yukopilets.model.dao.AbstractCRUDDao;
import by.training.yukopilets.model.dao.SQLQueryConstant;
import by.training.yukopilets.model.dao.UnitDao;
import by.training.yukopilets.model.dao.connection.Transaction;
import by.training.yukopilets.model.dao.connection.TransactionHandler;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

@Bean(name = ApplicationConstant.UNIT_DAO)
public class MySQLUnitDaoImpl extends AbstractCRUDDao<Unit> implements UnitDao, TransactionHandler {

    private Transaction transaction;

    public MySQLUnitDaoImpl() {
        super(SQLQueryConstant.TABLE_UNIT);
    }

    public Optional<Unit> findUnitByName(String unitName) throws DaoException {

        if (transaction == null) {
            throw new DaoException();
        }

        try {
            AtomicReference<Unit> result = new AtomicReference<>();

            transaction.doWithTransaction(connection -> {
                try (PreparedStatement statement =
                             connection.prepareStatement(SQLQueryConstant.SELECT_UNIT_BY_HEAD)) {
                    statement.setString(1, unitName);

                    ResultSet resultSet = statement.executeQuery();
                    if (resultSet.next()) {
                        result.set(processResultSet(resultSet));
                    }
                }
            });
            return Optional.of(result.get());
        } catch (TransactionException e) {
            throw new DaoException(e.getMessage(), e);
        }
    }

    @Override
    protected List<String> getColumnNames() {
        List<String> columnNames = new ArrayList<>();
        columnNames.add("user_rank_id");
        columnNames.add("unit_head");
        return columnNames;
    }

    @Override
    protected void fillStatement(PreparedStatement statement, Unit entity) throws SQLException {
        int i = 1;
        statement.setLong(i++, entity.getUserRank().getId());
        statement.setString(i, entity.getHead());
    }

    @Override
    protected Unit processResultSet(ResultSet resultSet) throws SQLException {
        return new Unit(
                resultSet.getLong("id"),
                resultSet.getString("unit_head"),
                UserRank.fromId(resultSet.getInt("user_rank_id"))
        );
    }

    @Override
    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    @Override
    public void releaseTransaction() {
        transaction = null;
    }
}
