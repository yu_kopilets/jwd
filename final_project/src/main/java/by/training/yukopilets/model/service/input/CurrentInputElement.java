package by.training.yukopilets.model.service.input;

import java.util.Queue;

/**
 * Realization of <tt>InputElementIterator</tt> methods for providing typings to user.
 * Based on <tt>Queue</tt> with separated parts of typing's body.
 *
 * @author Yuriy Kopilets
 * @see InputElementIterator
 */
public class CurrentInputElement implements InputElementIterator {

    private final String head;
    private final Queue<String> body;
    private final String description;
    private final InputElementObserver observer;

    public CurrentInputElement(String head, Queue<String> body, String description, InputElementObserver observer) {
        this.head = head;
        this.body = body;
        this.description = description;
        this.observer = observer;
    }

    /**
     * This method is used to get a typing sample's head.
     *
     * @return head of typing sample
     */
    @Override
    public String getHead() {
        return head;
    }

    /**
     * This method is used to get a typing sample's description.
     * Description need to show information about typing sample after input.
     *
     * @return description of typing sample
     */
    @Override
    public String getDescription() {
        return description;
    }

    /**
     * Return <tt>true</tt> if there are any body's part to input.
     *
     * @return <tt>true</tt> if there are any body's part to input
     */
    @Override
    public boolean hasNext() {
        return !body.isEmpty();
    }

    /**
     * Return next part of body.
     *
     * @return Next part of body is it exists
     */
    @Override
    public String next() {
        return body.poll();
    }

    /**
     * This method is used when input is finish.
     *
     */
    @Override
    public void finishInput() {
        observer.observe();
    }
}
