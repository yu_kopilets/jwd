package by.training.yukopilets.model.dao.mysql;

import by.training.yukopilets.application.ApplicationConstant;
import by.training.yukopilets.bean.Bean;
import by.training.yukopilets.entity.Text;
import by.training.yukopilets.exception.DaoException;
import by.training.yukopilets.model.dao.AbstractCRUDDao;
import by.training.yukopilets.model.dao.SQLQueryConstant;
import by.training.yukopilets.model.dao.TextDao;
import by.training.yukopilets.model.dao.TypingDao;
import by.training.yukopilets.model.dao.connection.ConnectionManager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

@Bean(name = ApplicationConstant.TEXT_DAO)
public class MySQLTextDaoImpl extends AbstractCRUDDao<Text> implements TypingDao<Text>, TextDao {

    public MySQLTextDaoImpl() {
        super(SQLQueryConstant.TABLE_TEXT);
    }

    public List<Text> findAllByLength(String findSign, int length) throws DaoException {
        try {
            List<Text> result = new ArrayList<>();

            ConnectionManager.getInstance().doWithConnection(connection -> {

                String sql = MessageFormat.format(SQLQueryConstant.SELECT_TEXT_BY_LENGTH, findSign);

                try (PreparedStatement statement = connection.prepareStatement(sql)) {
                    statement.setInt(1, length);
                    ResultSet resultSet = statement.executeQuery();
                    while (resultSet.next()) {
                        result.add(processResultSet(resultSet));
                    }
                }
            });
            return result;
        } catch (SQLException e) {
            throw new DaoException("Failed to complete action with connection!", e);
        }
    }

    @Override
    public void updateTextAccessible(List<Long> identifiers) throws DaoException {
        try {
            ConnectionManager.getInstance().doWithConnection(connection -> {
                try (PreparedStatement statement =
                             connection.prepareStatement(SQLQueryConstant.UPDATE_TEXT_ACCESSIBLE)) {
                    for (Long id : identifiers) {
                        statement.setLong(1, id);
                        statement.executeUpdate();
                    }
                }
            });
        } catch (SQLException e) {
            throw new DaoException("Failed to complete action with connection!", e);
        }
    }

    @Override
    public List<Text> findAllByUserId(Long userId) throws DaoException {
        try {
            List<Text> result = new ArrayList<>();

            ConnectionManager.getInstance().doWithConnection(connection -> {
                try (PreparedStatement statement =
                             connection.prepareStatement(SQLQueryConstant.SELECT_TEXT_BY_USER_ID)) {
                    statement.setLong(1, userId);
                    ResultSet resultSet = statement.executeQuery();
                    while (resultSet.next()) {
                        result.add(processResultSet(resultSet));
                    }
                }
            });
            return result;
        } catch (SQLException e) {
            throw new DaoException("Failed to complete action with connection!", e);
        }
    }

    @Override
    public List<Text> findAllByAccessible(Boolean isAccessible) throws DaoException {
        try {
            List<Text> result = new ArrayList<>();

            ConnectionManager.getInstance().doWithConnection(connection -> {
                try (PreparedStatement statement =
                             connection.prepareStatement(SQLQueryConstant.SELECT_TEXT_BY_ACCESSIBLE)) {
                    statement.setBoolean(1, isAccessible);
                    ResultSet resultSet = statement.executeQuery();
                    while (resultSet.next()) {
                        result.add(processResultSet(resultSet));
                    }
                }
            });
            return result;
        } catch (SQLException e) {
            throw new DaoException("Failed to complete action with connection!", e);
        }
    }

    @Override
    protected List<String> getColumnNames() {
        List<String> columnNames = new ArrayList<>();
        columnNames.add("user_account_id");
        columnNames.add("text_length");
        columnNames.add("text_head");
        columnNames.add("text_body");
        columnNames.add("text_description");
        columnNames.add("is_accessible");
        return columnNames;
    }

    @Override
    protected void fillStatement(PreparedStatement statement, Text entity) throws SQLException {
        int i = 1;
        statement.setLong(i++, entity.getUserId());
        statement.setInt(i++, entity.getLength());
        statement.setString(i++, entity.getHead());
        statement.setString(i++, entity.getBody());
        statement.setString(i++, entity.getDescription());
        statement.setBoolean(i, entity.isAccessible());
    }

    @Override
    protected Text processResultSet(ResultSet resultSet) throws SQLException {
        return new Text(
                resultSet.getLong("id"),
                resultSet.getLong("user_account_id"),
                resultSet.getInt("text_length"),
                resultSet.getString("text_head"),
                resultSet.getString("text_body"),
                resultSet.getString("text_description"),
                resultSet.getBoolean("is_accessible")
        );
    }
}
