package by.training.yukopilets.model.service;

import by.training.yukopilets.exception.ServiceException;
import by.training.yukopilets.model.service.input.InputElementIterator;

public interface UnitService {
    InputElementIterator loadUnit(String unitName) throws ServiceException;
}
