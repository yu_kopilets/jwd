package by.training.yukopilets.model.dao.connection;

import by.training.yukopilets.exception.TransactionException;

public interface Transaction {

    void begin() throws TransactionException;

    void begin(TransactionHandler... transactionHandlers) throws TransactionException;

    void commit() throws TransactionException;

    void rollback() throws TransactionException;

    void end() throws TransactionException;

    void doWithTransaction(ConnectionConsumer consumer) throws TransactionException;
}
