package by.training.yukopilets.model.dao.connection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class DataSourceConfig {

    private static final Logger LOGGER = LogManager.getLogger();

    private String driverClassName;
    private String url;
    private String username;
    private String password;
    private int poolCapacity;

    public DataSourceConfig(String configName, int poolCapacity) {
        this.poolCapacity = poolCapacity;
        try {
            Properties properties = new Properties();
            InputStream is = getClass().getClassLoader().getResourceAsStream(configName);
            properties.load(is);

            driverClassName = properties.getProperty("dataSource.driverClassName");
            url = properties.getProperty("dataSource.url");
            username = properties.getProperty("dataSource.username");
            password = properties.getProperty("dataSource.password");
        } catch (FileNotFoundException e) {
            LOGGER.error("Datasource properties file not found!", e);
        } catch (IOException e) {
            LOGGER.error("Fail in properties file reading!", e);
        }
    }

    public String getDriverClassName() {
        return driverClassName;
    }

    public String getUrl() {
        return url;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public int getPoolCapacity() {
        return poolCapacity;
    }
}
