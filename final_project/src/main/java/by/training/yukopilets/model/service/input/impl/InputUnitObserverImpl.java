package by.training.yukopilets.model.service.input.impl;

import by.training.yukopilets.application.SecurityContext;
import by.training.yukopilets.entity.Unit;
import by.training.yukopilets.entity.user.User;
import by.training.yukopilets.entity.user.UserRank;
import by.training.yukopilets.exception.ServiceException;
import by.training.yukopilets.model.service.impl.UserServiceImpl;
import by.training.yukopilets.model.service.input.InputElementObserver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Optional;

public class InputUnitObserverImpl implements InputElementObserver {

    private static final Logger LOGGER = LogManager.getLogger();

    private final UserServiceImpl userService;
    private final Unit currentUnit;

    public InputUnitObserverImpl(UserServiceImpl userService, Unit currentUnit) {
        this.userService = userService;
        this.currentUnit = currentUnit;
    }

    @Override
    public void observe() {
        Optional<User> currentUser = SecurityContext.getInstance().getCurrentUser();
        if(currentUser.isPresent() && currentUser.get().getRank() != UserRank.MASTER
                && currentUnit.getUserRank() != UserRank.MASTER) {
            UserRank newUserRank = UserRank.fromId(currentUnit.getUserRank().getId() + 1);
            currentUser.get().setRank(newUserRank);
            try {
                userService.updateUser(currentUser.get());
            } catch (ServiceException e) {
                LOGGER.error("Failed to update user rank!", e);
            }
        }
    }
}
