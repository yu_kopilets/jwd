package by.training.yukopilets.model.dao.connection;

import by.training.yukopilets.exception.TransactionException;

import java.sql.Connection;
import java.sql.SQLException;

public class TransactionImpl implements Transaction {

    private TransactionHandler[] transactionHandlers;
    private Connection currentConnection;

    @Override
    public void begin() throws TransactionException {
        if(currentConnection != null) {
            throw new TransactionException("Transaction is already running!");
        }

        try {
            currentConnection = getConnection();
            currentConnection.setAutoCommit(false);
        } catch (SQLException e) {
            throw new TransactionException("Getting connection exception (for begin transaction).", e);
        }
    }

    @Override
    public void begin(TransactionHandler... transactionHandlers) throws TransactionException {
        if(transactionHandlers.length < 1) {
            throw new TransactionException("There aren't any transaction handlers!");
        }

        try {
            currentConnection = getConnection();
            currentConnection.setAutoCommit(false);
        } catch (SQLException e) {
            throw new TransactionException("Getting connection exception (for begin transaction).", e);
        }

        this.transactionHandlers = transactionHandlers;

        for (TransactionHandler transactionHandler : transactionHandlers) {
            transactionHandler.setTransaction(this);
        }
    }

    @Override
    public void commit() throws TransactionException {
        if(currentConnection == null) {
            throw new TransactionException("Transaction isn't running! Call the \"begin method\".");
        }

        try {
            currentConnection.commit();
            currentConnection.close();
            currentConnection = null;
        } catch (SQLException e) {
            throw new TransactionException("Transaction commit exception!", e);
        }
    }

    @Override
    public void rollback() throws TransactionException {
        if(currentConnection == null) {
            throw new TransactionException("Transaction isn't running! Call the \"begin method\".");
        }
        try {
            currentConnection.rollback();
        } catch (SQLException e) {
            throw new TransactionException("Transaction rollback exception!", e);
        } finally {
            try {
                currentConnection.close();
                currentConnection = null;
            } catch (SQLException e) {
                throw new TransactionException("Failed to close connection!", e);
            }
        }
    }

    @Override
    public void end() throws TransactionException {
        if(transactionHandlers == null) {
            throw new TransactionException("There aren't any transaction handlers!");
        }

        for (TransactionHandler transactionHandler : transactionHandlers) {
            transactionHandler.releaseTransaction();
        }
        transactionHandlers = null;
    }

    public void doWithTransaction(ConnectionConsumer consumer) throws TransactionException {
        try {
            consumer.accept(currentConnection);
        } catch (SQLException e) {
            throw new TransactionException("Failed to complete action in transaction!", e);
        }
    }

    private Connection getConnection() throws SQLException {
        return DataSourceImpl.getInstance().getConnection();
    }
}
