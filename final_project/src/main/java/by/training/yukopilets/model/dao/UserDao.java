package by.training.yukopilets.model.dao;

import by.training.yukopilets.entity.user.User;
import by.training.yukopilets.exception.DaoException;

import java.util.Optional;

public interface UserDao {

    void save(User user) throws DaoException;

    Optional<User> findUserByLogin(String login) throws DaoException;
}
