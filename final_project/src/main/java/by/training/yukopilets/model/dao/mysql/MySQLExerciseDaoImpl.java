package by.training.yukopilets.model.dao.mysql;

import by.training.yukopilets.application.ApplicationConstant;
import by.training.yukopilets.bean.Bean;
import by.training.yukopilets.entity.Exercise;
import by.training.yukopilets.exception.DaoException;
import by.training.yukopilets.exception.TransactionException;
import by.training.yukopilets.model.dao.ExerciseDao;
import by.training.yukopilets.model.dao.SQLQueryConstant;
import by.training.yukopilets.model.dao.connection.Transaction;
import by.training.yukopilets.model.dao.connection.TransactionHandler;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

@Bean(name = ApplicationConstant.EXERCISE_DAO)
public class MySQLExerciseDaoImpl implements ExerciseDao, TransactionHandler {

    private Transaction transaction;

    @Override
    public List<Exercise> getExercisesByUnitId(Long unitId) throws DaoException {
        if(transaction == null) {
            throw new DaoException();
        }

        try {
            List<Exercise> exercises = new ArrayList<>();

            transaction.doWithTransaction(connection -> {
                try (PreparedStatement statement =
                             connection.prepareStatement(SQLQueryConstant.SELECT_UNIT_BY_ID)) {
                    statement.setLong(1, unitId);
                    ResultSet resultSet = statement.executeQuery();

                    while (resultSet.next()) {
                        exercises.add(new Exercise(
                                resultSet.getLong("id"),
                                resultSet.getString("exercise_body")
                        ));
                    }
                }
            });
            return exercises;
        } catch (TransactionException e) {
            throw new DaoException(e.getMessage(), e);
        }
    }

    @Override
    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    @Override
    public void releaseTransaction() {
        transaction = null;
    }
}
