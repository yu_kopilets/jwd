package by.training.yukopilets.model.dao;

import by.training.yukopilets.entity.TypedPangram;
import by.training.yukopilets.entity.TypedText;
import by.training.yukopilets.entity.user.UserHistory;
import by.training.yukopilets.exception.DaoException;

import java.util.List;

public interface UserHistoryDao {

    void saveTypedText(TypedText typedText) throws DaoException;

    void saveTypedPangram(TypedPangram typedPangram) throws DaoException;

    UserHistory findUserHistoryById(Long userId) throws DaoException;

    List<TypedText> findAllTypedTexts(Long userId) throws DaoException;

    List<TypedPangram> findAllTypedPangrams(Long userId) throws DaoException;
}
