package by.training.yukopilets.model.service.impl;

import by.training.yukopilets.application.ApplicationConstant;
import by.training.yukopilets.application.SecurityContext;
import by.training.yukopilets.bean.Bean;
import by.training.yukopilets.entity.TypedPangram;
import by.training.yukopilets.entity.TypedText;
import by.training.yukopilets.entity.user.User;
import by.training.yukopilets.entity.user.UserHistory;
import by.training.yukopilets.exception.DaoException;
import by.training.yukopilets.exception.ServiceException;
import by.training.yukopilets.model.dao.mysql.MySQLUserHistoryDaoImpl;
import by.training.yukopilets.model.service.UserHistoryService;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Bean(name = ApplicationConstant.USER_HISTORY_SERVICE)
public class UserHistoryServiceImpl implements UserHistoryService {

    private final MySQLUserHistoryDaoImpl userHistoryDao;

    public UserHistoryServiceImpl(MySQLUserHistoryDaoImpl userHistoryDao) {
        this.userHistoryDao = userHistoryDao;
    }

    public void saveTypedText(TypedText typedText) throws ServiceException {
        try {
            userHistoryDao.saveTypedText(typedText);
        } catch (DaoException e) {
            throw new ServiceException("Failed to save typed text!", e);
        }
    }

    public void saveTypedPangram(TypedPangram typedPangram) throws ServiceException {
        try {
            userHistoryDao.saveTypedPangram(typedPangram);
        } catch (DaoException e) {
            throw new ServiceException("Failed to save typed pangram!", e);
        }
    }

    @Override
    public UserHistory getCurrentUserHistory() throws ServiceException {
        try {
            Optional<User> currentUser = SecurityContext.getInstance().getCurrentUser();
            if (currentUser.isPresent()) {
                UserHistory userHistory = userHistoryDao.findUserHistoryById(currentUser.get().getId());
                List<TypedPangram> typedPangrams = userHistory.getTypedPangrams();
                List<TypedText> typedTexts = userHistory.getTypedTexts();

                double averageSpeed = countAverageSpeed(typedPangrams, typedTexts);
                userHistory.setTypedTypings(typedPangrams.size() + typedTexts.size());
                userHistory.setAverageSpeed(averageSpeed);

                typedPangrams = typedPangrams.stream()
                        .sorted(Comparator.comparing(TypedPangram::getTypedDate))
                        .collect(Collectors.toList());
                typedTexts = typedTexts.stream()
                        .sorted(Comparator.comparing(TypedText::getTypedDate))
                        .collect(Collectors.toList());
                Collections.reverse(typedPangrams);
                Collections.reverse(typedTexts);

                userHistory.setTypedPangrams(typedPangrams);
                userHistory.setTypedTexts(typedTexts);
                return userHistory;
            }
            throw new ServiceException("User isn't logged in!");
        } catch (DaoException e) {
            throw new ServiceException("Failed to get user history!", e);
        }
    }

    private double countAverageSpeed(List<TypedPangram> typedPangrams, List<TypedText> typedTexts) {
        double averageSpeed;
        if(!typedPangrams.isEmpty() && !typedTexts.isEmpty()) {
            double averagePangramsSpeed = typedPangrams.stream()
                    .mapToDouble(TypedPangram::getTypedSpeed)
                    .average()
                    .getAsDouble();
            double averageTextsSpeed = typedTexts.stream()
                    .mapToDouble(TypedText::getTypedSpeed)
                    .average()
                    .getAsDouble();
            averageSpeed = averagePangramsSpeed + averageTextsSpeed / 2;
        } else if(!typedPangrams.isEmpty()) {
            averageSpeed = typedPangrams.stream()
                    .mapToDouble(TypedPangram::getTypedSpeed)
                    .average()
                    .getAsDouble();
        } else if(!typedTexts.isEmpty()) {
            averageSpeed = typedTexts.stream()
                    .mapToDouble(TypedText::getTypedSpeed)
                    .average()
                    .getAsDouble();
        } else {
            averageSpeed = 0;
        }
        return averageSpeed;
    }
}
