package by.training.yukopilets.model.dao;

public interface IdentifiedEntity {

    Long getId();

    void setId(Long id);
}
