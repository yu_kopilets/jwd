package by.training.yukopilets.model.dao;

import by.training.yukopilets.exception.DaoException;

import java.util.List;
import java.util.Optional;

public interface CRUDDao<T extends IdentifiedEntity> {

    void save(T entity) throws DaoException;

    void update(T entity) throws DaoException;

    Optional<T> findById(Long id) throws DaoException;

    List<T> findAll() throws DaoException;

    void delete(T entity) throws DaoException;
}
