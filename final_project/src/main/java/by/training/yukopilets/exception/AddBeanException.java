package by.training.yukopilets.exception;

public class AddBeanException extends RuntimeException {
    public AddBeanException() {
    }

    public AddBeanException(String message) {
        super(message);
    }

    public AddBeanException(String message, Throwable cause) {
        super(message, cause);
    }

    public AddBeanException(Throwable cause) {
        super(cause);
    }
}
