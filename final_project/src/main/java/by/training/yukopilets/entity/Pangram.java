package by.training.yukopilets.entity;

import by.training.yukopilets.model.dao.IdentifiedEntity;
import by.training.yukopilets.model.dao.TypingEntity;

import java.util.Objects;

public class Pangram implements IdentifiedEntity, TypingEntity {

    private Long id;
    private Integer length;
    private String head;
    private String body;

    public Pangram(Long id, Integer length, String head, String body) {
        this.id = id;
        this.length = length;
        this.head = head;
        this.body = body;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public String getDescription() {
        return head + " is successful!";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Pangram pangram = (Pangram) o;
        return Objects.equals(id, pangram.id) &&
                Objects.equals(length, pangram.length) &&
                Objects.equals(head, pangram.head) &&
                Objects.equals(body, pangram.body);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, length, head, body);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Pangram{");
        sb.append("id=").append(id);
        sb.append(", length=").append(length);
        sb.append(", head='").append(head).append('\'');
        sb.append(", body='").append(body).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
