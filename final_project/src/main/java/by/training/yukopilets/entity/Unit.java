package by.training.yukopilets.entity;

import by.training.yukopilets.entity.user.UserRank;
import by.training.yukopilets.model.dao.IdentifiedEntity;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class Unit implements IdentifiedEntity {

    private Long id;
    private String head;
    private UserRank userRank;
    private List<Exercise> exercises;

    public Unit(Long id, String head, UserRank userRank) {
        this.id = id;
        this.head = head;
        this.userRank = userRank;
    }

    public Unit(Long id, String head, UserRank userRank, List<Exercise> exercises) {
        this.id = id;
        this.head = head;
        this.userRank = userRank;
        this.exercises = exercises;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public UserRank getUserRank() {
        return userRank;
    }

    public void setUserRank(UserRank userRank) {
        this.userRank = userRank;
    }

    public List<Exercise> getExercises() {
        return Collections.unmodifiableList(exercises);
    }

    public void setExercises(List<Exercise> exercises) {
        this.exercises = exercises;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Unit unit = (Unit) o;
        return Objects.equals(id, unit.id) &&
                Objects.equals(head, unit.head) &&
                userRank == unit.userRank &&
                Objects.equals(exercises, unit.exercises);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, head, userRank, exercises);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Unit{");
        sb.append("id=").append(id);
        sb.append(", head='").append(head).append('\'');
        sb.append(", userRank=").append(userRank);
        sb.append(", exercises=").append(exercises);
        sb.append('}');
        return sb.toString();
    }
}
