package by.training.yukopilets.entity;

import java.util.Objects;

public class TypedPangram {

    private Long userId;
    private Long pangramId;
    private String typedTitle;
    private String typedDate;
    private Double typedSpeed;

    public TypedPangram(Long userId, Long pangramId, String typedTitle, String typedDate, Double typedSpeed) {
        this.userId = userId;
        this.pangramId = pangramId;
        this.typedTitle = typedTitle;
        this.typedDate = typedDate;
        this.typedSpeed = typedSpeed;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getPangramId() {
        return pangramId;
    }

    public void setPangramId(Long pangramId) {
        this.pangramId = pangramId;
    }

    public String getTypedTitle() {
        return typedTitle;
    }

    public void setTypedTitle(String typedTitle) {
        this.typedTitle = typedTitle;
    }

    public String getTypedDate() {
        return typedDate;
    }

    public void setTypedDate(String typedDate) {
        this.typedDate = typedDate;
    }

    public Double getTypedSpeed() {
        return typedSpeed;
    }

    public void setTypedSpeed(Double typedSpeed) {
        this.typedSpeed = typedSpeed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TypedPangram that = (TypedPangram) o;
        return Objects.equals(userId, that.userId) &&
                Objects.equals(pangramId, that.pangramId) &&
                Objects.equals(typedTitle, that.typedTitle) &&
                Objects.equals(typedDate, that.typedDate) &&
                Objects.equals(typedSpeed, that.typedSpeed);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, pangramId, typedTitle, typedDate, typedSpeed);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("TypedPangram{");
        sb.append("userId=").append(userId);
        sb.append(", pangramId=").append(pangramId);
        sb.append(", typedTitle='").append(typedTitle).append('\'');
        sb.append(", typedDate='").append(typedDate).append('\'');
        sb.append(", typedSpeed=").append(typedSpeed);
        sb.append('}');
        return sb.toString();
    }
}
