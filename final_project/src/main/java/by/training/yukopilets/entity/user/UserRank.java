package by.training.yukopilets.entity.user;

public enum UserRank {

    BEGINNER(1, "Beginner"),
    INTERMEDIATE(2, "Intermediate"),
    ADVANCED(3, "Advanced"),
    MASTER(4, "Master");

    private final Integer id;
    private final String name;

    UserRank(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public static UserRank fromId(Integer id) {
        UserRank userRank = BEGINNER;
        for (UserRank value : UserRank.values()) {
            if(value.getId().equals(id)) {
                userRank = value;
            }
        }
        return userRank;
    }
}
