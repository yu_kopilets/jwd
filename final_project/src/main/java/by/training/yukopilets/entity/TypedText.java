package by.training.yukopilets.entity;

import java.util.Objects;

public class TypedText {

    private Long userId;
    private Long textId;
    private String typedTitle;
    private String typedDate;
    private Double typedSpeed;

    public TypedText(Long userId, Long textId, String typedTitle, String typedDate, Double typedSpeed) {
        this.userId = userId;
        this.textId = textId;
        this.typedTitle = typedTitle;
        this.typedDate = typedDate;
        this.typedSpeed = typedSpeed;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getTextId() {
        return textId;
    }

    public void setTextId(Long textId) {
        this.textId = textId;
    }

    public String getTypedTitle() {
        return typedTitle;
    }

    public void setTypedTitle(String typedTitle) {
        this.typedTitle = typedTitle;
    }

    public String getTypedDate() {
        return typedDate;
    }

    public void setTypedDate(String typedDate) {
        this.typedDate = typedDate;
    }

    public Double getTypedSpeed() {
        return typedSpeed;
    }

    public void setTypedSpeed(Double typedSpeed) {
        this.typedSpeed = typedSpeed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TypedText typedText = (TypedText) o;
        return Objects.equals(userId, typedText.userId) &&
                Objects.equals(textId, typedText.textId) &&
                Objects.equals(typedTitle, typedText.typedTitle) &&
                Objects.equals(typedDate, typedText.typedDate) &&
                Objects.equals(typedSpeed, typedText.typedSpeed);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, textId, typedTitle, typedDate, typedSpeed);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("TypedText{");
        sb.append("userId=").append(userId);
        sb.append(", textId=").append(textId);
        sb.append(", typedTitle='").append(typedTitle).append('\'');
        sb.append(", typedDate='").append(typedDate).append('\'');
        sb.append(", typedSpeed=").append(typedSpeed);
        sb.append('}');
        return sb.toString();
    }
}
