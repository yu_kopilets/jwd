package by.training.yukopilets.entity.user;

import by.training.yukopilets.entity.TypedPangram;
import by.training.yukopilets.entity.TypedText;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class UserHistory {

    private Long userId;
    private int typedTypings;
    private double averageSpeed;
    private List<TypedText> typedTexts;
    private List<TypedPangram> typedPangrams;

    public UserHistory(Long userId, List<TypedText> typedTexts, List<TypedPangram> typedPangrams) {
        this.userId = userId;
        this.typedTexts = typedTexts;
        this.typedPangrams = typedPangrams;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public int getTypedTypings() {
        return typedTypings;
    }

    public void setTypedTypings(int typedTypings) {
        this.typedTypings = typedTypings;
    }

    public double getAverageSpeed() {
        return averageSpeed;
    }

    public void setAverageSpeed(double averageSpeed) {
        this.averageSpeed = averageSpeed;
    }

    public List<TypedText> getTypedTexts() {
        return Collections.unmodifiableList(typedTexts);
    }

    public void setTypedTexts(List<TypedText> typedTexts) {
        this.typedTexts = typedTexts;
    }

    public List<TypedPangram> getTypedPangrams() {
        return Collections.unmodifiableList(typedPangrams);
    }

    public void setTypedPangrams(List<TypedPangram> typedPangrams) {
        this.typedPangrams = typedPangrams;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UserHistory that = (UserHistory) o;
        return typedTypings == that.typedTypings &&
                Double.compare(that.averageSpeed, averageSpeed) == 0 &&
                Objects.equals(userId, that.userId) &&
                Objects.equals(typedTexts, that.typedTexts) &&
                Objects.equals(typedPangrams, that.typedPangrams);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, typedTypings, averageSpeed, typedTexts, typedPangrams);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("UserHistory{");
        sb.append("userId=").append(userId);
        sb.append(", typedTypings=").append(typedTypings);
        sb.append(", averageSpeed=").append(averageSpeed);
        sb.append(", typedTexts=").append(typedTexts);
        sb.append(", typedPangrams=").append(typedPangrams);
        sb.append('}');
        return sb.toString();
    }
}
