package by.training.yukopilets.entity.user;

public enum UserRole {

    VISITOR(0),
    USER(1),
    MODERATOR(2);

    private final Integer id;

    UserRole(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public static UserRole fromId(Integer id) {
        UserRole userRole = VISITOR;
        for (UserRole value : UserRole.values()) {
            if(value.getId().equals(id)) {
                userRole = value;
            }
        }
        return userRole;
    }
}
