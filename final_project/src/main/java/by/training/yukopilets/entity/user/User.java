package by.training.yukopilets.entity.user;

import by.training.yukopilets.model.dao.IdentifiedEntity;

import java.util.Objects;

public class User implements IdentifiedEntity {

    private Long id;
    private String login;
    private String password;
    private String email;
    private Boolean isActive;
    private UserRole role;
    private UserRank rank;

    public User(Long id, String login, String password, String email, Boolean isActive, UserRole role, UserRank rank) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.email = email;
        this.isActive = isActive;
        this.role = role;
        this.rank = rank;
    }

    public User(UserRole role) {
        this.role = role;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean isActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public UserRank getRank() {
        return rank;
    }

    public void setRank(UserRank rank) {
        this.rank = rank;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        User user = (User) o;
        return Objects.equals(id, user.id) &&
                Objects.equals(login, user.login) &&
                Objects.equals(password, user.password) &&
                Objects.equals(email, user.email) &&
                Objects.equals(isActive, user.isActive) &&
                role == user.role &&
                rank == user.rank;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, login, password, email, isActive, role, rank);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("User{");
        sb.append("id=").append(id);
        sb.append(", login='").append(login).append('\'');
        sb.append(", password='").append(password).append('\'');
        sb.append(", email='").append(email).append('\'');
        sb.append(", isActive=").append(isActive);
        sb.append(", role=").append(role);
        sb.append(", rank=").append(rank);
        sb.append('}');
        return sb.toString();
    }
}
