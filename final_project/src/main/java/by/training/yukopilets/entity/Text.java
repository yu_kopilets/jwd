package by.training.yukopilets.entity;

import by.training.yukopilets.model.dao.IdentifiedEntity;
import by.training.yukopilets.model.dao.TypingEntity;

import java.util.Objects;

public class Text implements IdentifiedEntity, TypingEntity {

    private Long id;
    private Long userId;
    private Integer length;
    private String head;
    private String body;
    private String description;
    private Boolean isAccessible;

    public Text(Long id, Long userId, Integer length, String head, String body, String description, Boolean isAccessible) {
        this.id = id;
        this.userId = userId;
        this.length = length;
        this.head = head;
        this.body = body;
        this.description = description;
        this.isAccessible = isAccessible;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    @Override
    public String getHead() {
        return head;
    }

    @Override
    public void setHead(String head) {
        this.head = head;
    }

    @Override
    public String getBody() {
        return body;
    }

    @Override
    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean isAccessible() {
        return isAccessible;
    }

    public void setAccessible(Boolean accessible) {
        isAccessible = accessible;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Text text = (Text) o;
        return Objects.equals(id, text.id) &&
                Objects.equals(userId, text.userId) &&
                Objects.equals(length, text.length) &&
                Objects.equals(head, text.head) &&
                Objects.equals(body, text.body) &&
                Objects.equals(description, text.description) &&
                Objects.equals(isAccessible, text.isAccessible);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, userId, length, head, body, description, isAccessible);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Text{");
        sb.append("id=").append(id);
        sb.append(", userId=").append(userId);
        sb.append(", length=").append(length);
        sb.append(", head='").append(head).append('\'');
        sb.append(", body='").append(body).append('\'');
        sb.append(", description='").append(description).append('\'');
        sb.append(", isAccessible=").append(isAccessible);
        sb.append('}');
        return sb.toString();
    }
}
