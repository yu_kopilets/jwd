package by.training.yukopilets.application;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@WebListener
public class ApplicationListener implements ServletContextListener {

    private static final Logger LOGGER = LogManager.getLogger();

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        SecurityContext.initialize();
        ApplicationContext.initialize();
        LOGGER.info("Contexts have been initialized!");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        ApplicationContext.getInstance().destroy();
        LOGGER.info("Contexts have been destroyed!");
    }
}
