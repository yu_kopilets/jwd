package by.training.yukopilets.application;

import by.training.yukopilets.bean.BeanRegistry;
import by.training.yukopilets.bean.impl.BeanRegistryImpl;
import by.training.yukopilets.controller.command.impl.*;
import by.training.yukopilets.model.dao.connection.DataSourceImpl;
import by.training.yukopilets.model.dao.mysql.*;
import by.training.yukopilets.model.service.impl.TypingServiceImpl;
import by.training.yukopilets.model.service.impl.UnitServiceImpl;
import by.training.yukopilets.model.service.impl.UserHistoryServiceImpl;
import by.training.yukopilets.model.service.impl.UserServiceImpl;

import java.util.concurrent.locks.ReentrantLock;

public class ApplicationContext implements BeanRegistry {

    private static final ReentrantLock LOCK = new ReentrantLock();
    private static ApplicationContext instance;

    private final BeanRegistry beanRegistry = new BeanRegistryImpl();

    private ApplicationContext() {
    }

    public static void initialize() {
        LOCK.lock();
        try {
            if(instance == null) {
                instance = new ApplicationContext();
                DataSourceImpl.initialize();
                instance.initBeans();
            } else {
                throw new IllegalStateException("ApplicationContext is already initialized!");
            }
        } finally {
            LOCK.unlock();
        }
    }

    private void initBeans() {
        beanRegistry.addBean(MySQLUserDaoImpl.class);
        beanRegistry.addBean(MySQLUnitDaoImpl.class);
        beanRegistry.addBean(MySQLTextDaoImpl.class);
        beanRegistry.addBean(MySQLPangramDaoImpl.class);
        beanRegistry.addBean(MySQLExerciseDaoImpl.class);
        beanRegistry.addBean(MySQLUserHistoryDaoImpl.class);

        beanRegistry.addBean(UserServiceImpl.class);
        beanRegistry.addBean(UnitServiceImpl.class);
        beanRegistry.addBean(TypingServiceImpl.class);
        beanRegistry.addBean(UserHistoryServiceImpl.class);

        beanRegistry.addBean(DefaultCommandImpl.class);
        beanRegistry.addBean(LoginCommandImpl.class);
        beanRegistry.addBean(LogoutCommandImpl.class);
        beanRegistry.addBean(RegistrationCommandImpl.class);
        beanRegistry.addBean(TypingTextAdapterImpl.class);
        beanRegistry.addBean(TypingLoadCommandImpl.class);
        beanRegistry.addBean(UnitInputCommandImpl.class);
        beanRegistry.addBean(UnitLoadCommandImpl.class);
        beanRegistry.addBean(UserHistoryCommandImpl.class);
        beanRegistry.addBean(UploadTextCommandImpl.class);
        beanRegistry.addBean(ShowOwnTextsCommandImpl.class);
        beanRegistry.addBean(ShowNewTextsCommandImpl.class);
        beanRegistry.addBean(SubmitTextCommandImpl.class);
    }

    public static ApplicationContext getInstance() {
        if(instance == null) {
            throw new IllegalStateException("ApplicationContext isn't initialized!");
        }
        return instance;
    }

    @Override
    public <T> void addBean(T bean) {
        beanRegistry.addBean(bean);
    }

    @Override
    public <T> void addBean(Class<T> beanClass) {
        beanRegistry.addBean(beanClass);
    }

    @Override
    public <T> T getBean(Class<T> beanClass) {
        return beanRegistry.getBean(beanClass);
    }

    @Override
    public <T> void removeBean(T bean) {
        beanRegistry.removeBean(bean);
    }

    @Override
    public void destroy() {
        beanRegistry.destroy();
    }
}
