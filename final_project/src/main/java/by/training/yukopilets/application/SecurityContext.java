package by.training.yukopilets.application;

import by.training.yukopilets.controller.command.CommandType;
import by.training.yukopilets.entity.user.User;
import by.training.yukopilets.entity.user.UserRank;
import by.training.yukopilets.entity.user.UserRole;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;

public class SecurityContext {

    private static final ReentrantLock LOCK = new ReentrantLock();
    private static SecurityContext instance;

    private final Map<String, Optional<User>> users;
    private final ThreadLocal<Optional<User>> currentUser;
    private final Properties properties;

    private SecurityContext() {
        users = new ConcurrentHashMap<>(1000);
        currentUser = new ThreadLocal<>();
        properties = new Properties();
        try (InputStream is = SecurityContext.class.getResourceAsStream("/security.properties")) {
            properties.load(is);
        } catch (IOException e) {
            throw new IllegalStateException("Failed to read security properties", e);
        }
    }

    public static void initialize() {
        LOCK.lock();
        try {
            if(instance == null) {
                instance = new SecurityContext();
            } else {
                throw new IllegalStateException("SecurityContext is already initialized!");
            }
        } finally {
            LOCK.unlock();
        }
    }

    public void login(String sessionId, User user) {
        users.put(sessionId, Optional.of(user));
        if(users.get(sessionId).isPresent()) {
            currentUser.set(users.get(sessionId));
        } else {
            currentUser.set(Optional.empty());
        }
    }

    public void logout(String sessionId) {
        users.remove(sessionId);
        currentUser.set(Optional.empty());
    }

    public Optional<User> getCurrentUser() {
        return currentUser.get();
    }

    public void setCurrentUserRole(String sessionId) {
        if(users.containsKey(sessionId)) {
            if(users.get(sessionId).isPresent()) {
                currentUser.set(users.get(sessionId));
            } else {
                currentUser.set(Optional.empty());
            }
        } else {
            currentUser.set(Optional.empty());
        }
    }

    public boolean isExecutable(CommandType commandType) {
        Optional<User> user = getCurrentUser();
        UserRole userRole = UserRole.VISITOR;
        if(user.isPresent()) {
            userRole = user.get().getRole();
        }
        return isExecutable(userRole, commandType);
    }

    public boolean isExecutable(UserRole userRole, CommandType commandType) {
        String commandToRoles = properties.getProperty("command." + commandType.name());
        List<String> roles = Optional.ofNullable(commandToRoles)
                .map(s -> Arrays.asList(s.split(",")))
                .orElseGet(ArrayList::new);

        return roles.stream().anyMatch(role -> role.equalsIgnoreCase(userRole.name()));
    }

    public boolean unitIsAccessible(String unitName) {
        Optional<User> user = getCurrentUser();
        UserRank userRank;
        if(user.isPresent()) {
            userRank = user.get().getRank();
        } else {
            userRank = UserRank.BEGINNER;
        }

        String unitToRanks = properties.getProperty("unit." + unitName);
        List<String> ranks = Optional.ofNullable(unitToRanks)
                .map(s -> Arrays.asList(s.split(",")))
                .orElseGet(ArrayList::new);

        return ranks.stream().anyMatch(role -> role.equalsIgnoreCase(userRank.name()));
    }

    public boolean isLoggedIn() {
        Optional<User> user = getCurrentUser();
        return user.isPresent();
    }

    public static SecurityContext getInstance() {
        if(instance == null) {
            throw new IllegalStateException("SecurityContext isn't initialized!");
        }
        return instance;
    }
}
