package by.training.yukopilets.application;

import java.util.HashSet;
import java.util.Set;

public class ApplicationConstant {

    public static final String DEFAULT_COMMAND = "default";
    public static final String LOGIN_COMMAND = "login";
    public static final String LOGOUT_COMMAND = "logout";
    public static final String REGISTRATION_COMMAND = "registration";
    public static final String INPUT_TYPING_COMMAND = "inputTyping";
    public static final String LOAD_TYPING_COMMAND = "loadTyping";
    public static final String INPUT_UNIT_COMMAND = "inputUnit";
    public static final String LOAD_UNIT_COMMAND = "loadUnit";
    public static final String USER_HISTORY_COMMAND = "history";
    public static final String UPLOAD_TEXT_COMMAND = "uploadText";
    public static final String SHOW_OWN_TEXTS_COMMAND = "showOwnTexts";
    public static final String SHOW_NEW_TEXTS_COMMAND = "showNewTexts";
    public static final String SUBMIT_TEXT_COMMAND = "submitText";

    public static final Set<String> URL_PATTERNS;
    public static final String DEFAULT_URL_PATTERN = "/home";

    public static final String USER_SERVICE = "userService";
    public static final String UNIT_SERVICE = "unitService";
    public static final String TEXT_SERVICE = "textService";
    public static final String TYPING_SERVICE = "typingService";
    public static final String USER_HISTORY_SERVICE = "userHistoryService";

    public static final String PANGRAM_TYPING_ADAPTER = "pangramTypingAdapter";
    public static final String TEXT_TYPING_ADAPTER = "textTypingAdapter";
    public static final String TYPING_ADAPTER_FACTORY = "typingAdapterFactory";

    public static final String USER_DAO = "userDao";
    public static final String USER_HISTORY_DAO = "userHistoryDao";
    public static final String UNIT_DAO = "unitDao";
    public static final String TEXT_DAO = "textDao";
    public static final String PANGRAM_DAO = "pangramDao";
    public static final String EXERCISE_DAO = "exerciseDao";

    static {
        URL_PATTERNS = new HashSet<>();
        URL_PATTERNS.add("/login");
        URL_PATTERNS.add("/registration");
        URL_PATTERNS.add("/template");
        URL_PATTERNS.add("/typing");
        URL_PATTERNS.add("/units");
        URL_PATTERNS.add("/user-menu");
    }

    private ApplicationConstant() {
    }
}
