<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<div class="login-clean" style="background-color: transparent; margin-top: 5%">
    <form method="post" action="registration">
        <h2 class="sr-only">Login Form</h2>
        <input type="hidden" name="commandName" value="registration">
        <div class="illustration"><i class="icon ion-ios-locked-outline"></i></div>
        <div class="form-group"><input class="form-control" id="login" type="text" name="login" placeholder="<fmt:message key="logIn.login"/>"></div>
        <div class="form-group"><input class="form-control" id="password" type="password" name="password" placeholder="<fmt:message key="logIn.password"/>"></div>
        <div class="form-group"><input class="form-control" id="confirm" type="password" placeholder="<fmt:message key="logIn.confirm"/>"></div>
        <div class="form-group"><input class="form-control" id="email" type="email" name="email" placeholder="<fmt:message key="logIn.email"/>"></div>
        <div class="form-group"><button class="btn btn-primary btn-block" id="submitLogin" type="submit"><fmt:message key="menu.singUp"/></button></div>
    </form>
</div>