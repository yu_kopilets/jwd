<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="by.training.yukopilets.application.SecurityContext" %>
<%@ page import="by.training.yukopilets.controller.command.CommandType" %>
<%@ taglib prefix="s" uri="securityTags" %>

<s:checkLoggedIn>
    <div>
        <div class="container" style="margin: 3%">
            <div class="row">
                <div class="col-md-4">
                    <ul class="list-unstyled">
                        <li><a href="?commandName=userHistory" style="color: rgb(0,0,0);"><fmt:message key="user.menu.history"/></a></li>
                        <li><a href="?commandName=showOwnTexts" style="color: rgb(0,0,0);"><fmt:message key="user.menu.own-texts"/></a></li>
                        <s:checkCommand commandType="${CommandType.SHOW_NEW_TEXTS}">
                            <li><a href="?commandName=showNewTexts" style="color: rgb(157,157,157);"><fmt:message key="user.menu.moderation"/></a></li>
                        </s:checkCommand>
                    </ul>
                </div>
                <div class="col-md-8 text-center" style="border: 1px groove black; padding: 4px">
                    <c:choose>
                        <c:when test="${userMenuView != null}">
                            <jsp:include page="user-menu/${userMenuView}.jsp"/>
                        </c:when>
                        <c:otherwise>
                            <c:set var = "currentUser" scope = "page" value = "${SecurityContext.getInstance().getCurrentUser()}"/>
                            <div><label><fmt:message key="profile.login"/>&nbsp;</label><label>${currentUser.get().getLogin()}</label></div>
                            <div><label><fmt:message key="profile.email"/>&nbsp;</label><label>${currentUser.get().getEmail()}</label></div>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
        </div>
    </div>
</s:checkLoggedIn>
