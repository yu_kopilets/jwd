<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="container">
    <h1 class="text-center">Template</h1>
    <p style="font-size: 18px;">Proper hand placement is a very important part of the training and use of touch typing.
        In addition to the initial position of the fingers, you must also remember that each finger has its own area for
        typing.</p>
    <p style="font-size: 18px;"><br>The default location of your left hand is&nbsp;<strong><span
            style="text-decoration: underline;">asdf</span></strong>&nbsp;and the right is&nbsp;<strong><span
            style="text-decoration: underline;">jkl;</span></strong>. Each finger has its own
        typing space. The picture on the right shows all the fingers and their colors, as well as the keys on the
        keyboard with the same colors. With the correct placement of the fingers on the keyboard, each finger should
        only press keys with the
        same color as the color of the finger. For example, the middle finger of the left hand is green, which means it
        should press the green keys (<strong><span style="text-decoration: underline;">3edc</span></strong>). Thumbs
        press only space.<br><br></p>
</div>
<div>
    <div class="container">
        <div class="row">
            <div class="col-md-6"><img class="img-fluid" src="static/assets/img/start-position.png"></div>
            <div class="col-md-6"><img class="img-fluid" src="static/assets/img/fingers-areas.png"></div>
        </div>
    </div>
</div>
<div class="container">
    <p class="lead text-center" style="font-size: 22px;">You can train the position of the hands and typing with each
        finger individually in the <a href="units"><strong>hushkey - units</strong></a> mode on our website.</p>
</div>