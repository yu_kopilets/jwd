<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<p><label><fmt:message key="profile.history.num"/> ${typedTypings}</label></p>
<p><label><fmt:message key="profile.history.averageSpeed"/>&nbsp;${averageSpeed}</label></p>
<table border=1 width=100% cellpadding=5>
    <thead>
    <tr>
        <th><fmt:message key="profile.history.title"/></th>
        <th><fmt:message key="profile.history.date"/></th>
        <th><fmt:message key="profile.history.speed"/></th>
    </tr>
    </thead>

    <tbody>
    <c:forEach var="text" items="${typedTexts}">
        <tr>
            <td><c:out value="${text.typedTitle}"/></td>
            <td><c:out value="${text.typedDate}"/></td>
            <td><c:out value="${text.typedSpeed}"/></td>
        </tr>
    </c:forEach>
    <c:forEach var="pangram" items="${typedPangrams}">
        <tr>
            <td><c:out value="${pangram.typedTitle}"/></td>
            <td><c:out value="${pangram.typedDate}"/></td>
            <td><c:out value="${pangram.typedSpeed}"/></td>
        </tr>
    </c:forEach>
    </tbody>
</table>
