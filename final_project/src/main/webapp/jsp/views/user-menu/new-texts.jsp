<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="by.training.yukopilets.controller.command.CommandType" %>
<%@ taglib prefix="s" uri="securityTags" %>

<s:checkCommand commandType="${CommandType.SHOW_NEW_TEXTS}">
    <form method="post" action="user-menu">
        <input type="hidden" name="commandName" value="submitText">
        <table border=1 width=100% cellpadding=5>
            <thead>
            <tr>
                <th><fmt:message key="profile.head"/></th>
                <th><fmt:message key="profile.body"/></th>
                <th><fmt:message key="profile.description"/></th>
                <th></th>
            </tr>
            </thead>

            <tbody>
            <c:forEach var="text" items="${newTexts}">
                <tr>
                    <td><c:out value="${text.head}"/></td>
                    <td><c:out value="${text.body}"/></td>
                    <td><c:out value="${text.description}"/></td>
                    <td><input type="checkbox" name="textId" value="${text.id}"></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        <button class="btn btn-primary" type="submit" style="margin-top: 2%"><fmt:message key="profile.newTexts.submit"/></button>
    </form>
</s:checkCommand>
