<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div>
    <form method="post" action="user-menu" style="background-color: #f8f8f8; padding: 10px">
        <input type="hidden" name="commandName" value="uploadText">
        <p><label><fmt:message key="profile.ownTexts.textHead"/> </label>
            <input class="form-control" type="text" name="head">
        </p>
        <p><label><fmt:message key="profile.ownTexts.textBody"/> </label>
            <textarea class="form-control" name="body" rows="10" style="resize: none;"></textarea>
        </p>
        <p><label><fmt:message key="profile.ownTexts.textDescription"/> </label>
            <textarea class="form-control" name="description" rows="5" style="resize: none;"></textarea>
        </p>
        <button class="btn btn-primary" type="submit"><fmt:message key="profile.ownTexts.add"/></button>
    </form>
</div>
<table border=1 width=100% cellpadding=5>
    <thead>
    <tr>
        <th><fmt:message key="profile.head"/></th>
        <th><fmt:message key="profile.body"/></th>
        <th><fmt:message key="profile.description"/></th>
        <th><fmt:message key="profile.ownTexts.length"/></th>
    </tr>
    </thead>

    <tbody>
    <c:forEach var="text" items="${ownTexts}">
        <tr>
            <td><c:out value="${text.head}"/></td>
            <td><c:out value="${text.body}"/></td>
            <td><c:out value="${text.description}"/></td>
            <td><c:out value="${text.length}"/></td>
        </tr>
    </c:forEach>
    </tbody>
</table>
