<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="container">
    <h1 class="text-center">Touch typing</h1>
    <p style="font-size: 18px;"><br><strong>Touch typing</strong>&nbsp;(also called&nbsp;<strong>touch type</strong>&nbsp;or&nbsp;<strong>touch
        keyboarding</strong>) is a style of&nbsp;typing. Although the phrase refers to typing without using the sense of&nbsp;sight&nbsp;to
        find the keys—specifically, a touch typist will know their location on the keyboard through&nbsp;muscle
        memory—the term is often used to refer to a specific form of touch typing that involves placing the eight
        fingers in a horizontal row along
        the middle of the keyboard (the&nbsp;<em>home row</em>) and having them reach for specific other keys. (Under
        this usage, typists who do not look at the keyboard but do not use home row either are referred to as hybrid
        typists.) Both two-handed
        touch typing and&nbsp;one-handed touch typing&nbsp;are possible.<br><br></p>
    <h2>Benefits of&nbsp;touch typing<br></h2>
    <ul style="font-size: 18px;margin: 3%;">
        <li>Touch typing can reduce typing time by several times. You can effectively save the time you spend due to
            incorrect typing.<br></li>
        <li>Touch typing allows you to focus on the text and its spelling, because you do not need to be distracted by
            the keyboard.
        </li>
        <li>Your eyes are resting during touch typing, because you do not need to constantly look from the monitor to
            the keyboard and vice versa. The reason is that the distance from the keyboard to the screen is usually
            different, there is constant
            refocusing.
        </li>
        <li>You keep your head constantly straight, which improves posture. Touch typing positively affects our
            health!
        </li>
    </ul>
    <h2>What you have to know<br></h2>
    <p class="text-left" style="font-size: 18px;"><br>The default location of your left hand is&nbsp;<span
            style="text-decoration: underline;">asdf</span>&nbsp;and the right is&nbsp;<span
            style="text-decoration: underline;">jkl;</span>.<br></p><img class="img-fluid" id="start-position"
                                                                         src="static/assets/img/start-position.png">
    <p
            style="font-size: 18px;"><br>The main idea of touch typing is that every finger has its own area on the
        keyboard. In this way, your brain divides every word into different finger responsibilities to speed up your
        typing. The most important thing is that even if you accidentally
        looked at the keyboard you should never type a letter with an inappropriate finger.<br></p><img
        class="img-fluid" id="fingers-areas" src="static/assets/img/fingers-areas.png">
    <h2>What is Hushkey?</h2>
    <p class="lead" style="font-size: 22px;"><br>Hushkey is a place where you can practice your typing skills. The site
        has online simulators for <a href="units">training finger positioning</a> and for <a href="typing">training
            typing</a> of random texts and pangrams (sentence using
        every letter of a given&nbsp;alphabet&nbsp;at least once).</p>
</div>