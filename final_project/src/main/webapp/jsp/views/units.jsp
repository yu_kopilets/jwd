<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="securityTags" %>
<div style="padding-top: 10%;">
    <div class="container">
        <div class="row">
            <div class="col-md-4" id="sidebar">
                <ul>
                    <s:checkUnit unitName="Unit1">
                        <li><a href="units?commandName=loadUnit&name=Unit 1">Unit 1</a></li>
                    </s:checkUnit>

                    <s:checkUnit unitName="Unit2">
                        <li><a href="units?commandName=loadUnit&name=Unit 2">Unit 2</a></li>
                    </s:checkUnit>

                    <s:checkUnit unitName="Unit3">
                        <li><a href="units?commandName=loadUnit&name=Unit 3">Unit 3</a></li>
                    </s:checkUnit>

                    <s:checkUnit unitName="Unit4">
                        <li><a href="units?commandName=loadUnit&name=Unit 4">Unit 4</a></li>
                    </s:checkUnit>

                    <s:checkUnit unitName="Unit5">
                        <li><a href="units?commandName=loadUnit&name=Unit 5">Unit 5</a></li>
                    </s:checkUnit>

                    <s:checkUnit unitName="Unit6">
                        <li><a href="units?commandName=loadUnit&name=Unit 6">Unit 6</a></li>
                    </s:checkUnit>

                    <s:checkUnit unitName="Unit7">
                        <li><a href="units?commandName=loadUnit&name=Unit 7">Unit 7</a></li>
                    </s:checkUnit>

                    <s:checkUnit unitName="Unit8">
                        <li><a href="units?commandName=loadUnit&name=Unit 8">Unit 8</a></li>
                    </s:checkUnit>

                    <s:checkUnit unitName="Unit9">
                        <li><a href="units?commandName=loadUnit&name=Unit 9">Unit 9</a></li>
                    </s:checkUnit>
                </ul>
            </div>
            <div class="col-md-8">
                <form class="text-center" id="input_form" method="post" action="units">
                    <c:choose>
                        <c:when test="${templateHead != null}">
                            <p><label><c:out value="${templateHead}"/></label></p>
                        </c:when>
                        <c:otherwise>
                            <p><label><fmt:message key="units.message"/></label></p>
                        </c:otherwise>
                    </c:choose>
                    <div>
                        <div id="bright_template"></div><!--
					    ---><div id="input_cursor"></div><!--
						---><div id="input_template"><c:out value="${inputTemplate}"/></div>
                    </div>
                    <p><textarea class="form-control" id="input_area" rows="4" cols="45" disabled="true"></textarea></p>
                    <input class="form-control" type="hidden" name="commandName" value="inputUnit"></form>
            </div>
        </div>
    </div>
</div>