<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="login-clean" style="background-color: transparent; margin-top: 5%">
    <form method="post" action="login">
        <h2 class="sr-only">Login Form</h2>
        <input type="hidden" name="commandName" value="login">
        <div class="illustration"><i class="icon ion-ios-locked-outline"></i></div>
        <div class="form-group"><input class="form-control" type="text" name="login" placeholder="<fmt:message key="logIn.login"/>"
        <c:if test="${userLogin != null}">
                  value="${userLogin}"
        </c:if>
        ></div>
        <div class="form-group"><input class="form-control" type="password" name="password" placeholder="<fmt:message key="logIn.password"/>"></div>
        <div class="form-group"><button class="btn btn-primary btn-block" type="submit"><fmt:message key="menu.login"/></button></div>
        <a class="forgot" href="registration"><fmt:message key="menu.singUp"/></a></form>
</div>