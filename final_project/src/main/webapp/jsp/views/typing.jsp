<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="container">
    <div class="text-center" style="margin: 5%;">
        <form action="typing" method="post">
            <input class="form-control" type="hidden" name="commandName" value="loadTyping">
            <p><fmt:message key="typing.type"/>&nbsp;
            <input type="radio" id="typePangram" name="type" value="pangram">&nbsp;<fmt:message key="typing.pangram"/>;&nbsp;
            <input type="radio" id="typeText" name="type" value="text">&nbsp;<fmt:message key="typing.text"/>;&nbsp;
            <input type="radio" id="typeRandom" name="type" value="random" checked="true">&nbsp;<fmt:message key="typing.random"/>
            </p>
            <p><fmt:message key="typing.length"/>&nbsp;
                <input type="radio" id="lengthLong" name="length" value="short">&nbsp;<fmt:message key="typing.short"/>;&nbsp;
                <input type="radio" id="lengthShort" name="length" value="long">&nbsp;<fmt:message key="typing.long"/>;&nbsp;
                <input type="radio" id="lengthRandom" name="length" value="random" checked="true">&nbsp;<fmt:message key="typing.random"/>
            </p>
            <button class="btn btn-primary" type="submit"><fmt:message key="typing.load"/></button>
        </form>
    </div>
    <div>
        <form class="text-center" id="input_form" method="post" action="typing">
            <c:choose>
                <c:when test="${templateHead != null}">
                    <p><label>${templateHead}</label></p>
                </c:when>
                <c:otherwise>
                    <p><label><fmt:message key="typing.message"/></label></p>
                </c:otherwise>
            </c:choose>
            <div>
                <div id="bright_template"></div><!--
                ---><div id="input_cursor"></div><!--
                ---><div id="input_template"><c:out value="${inputTemplate}"/></div>
            </div>
            <p><textarea class="form-control" id="input_area" rows="5" cols="45" disabled="true"></textarea></p>
            <input class="form-control" type="hidden" name="commandName" value="inputTyping">
            <button class="btn btn-primary" type="submit"><fmt:message key="typing.skip"/></button>
        </form>
    </div>
</div>