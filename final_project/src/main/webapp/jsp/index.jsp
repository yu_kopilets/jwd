<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Hushkey</title>
    <link rel="stylesheet" href="static/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="static/assets/fonts/fontawesome-all.min.css">
    <link rel="stylesheet" href="static/assets/fonts/ionicons.min.css">
    <link rel="stylesheet" href="static/assets/css/Login-Form-Clean.css">
    <link rel="stylesheet" href="static/assets/css/Login-Form-Dark.css">
    <link rel="stylesheet" href="static/assets/css/styles.css">
    <link rel="stylesheet" href="static/assets/css/Footer-Basic.css">
    <link rel="stylesheet" href="static/assets/css/Navigation-with-Button.css">
</head>

<body>
    <fmt:setLocale value="${lang}"/>
    <fmt:setBundle basename="/i18n/AppMessages" scope="application"/>

    <div id="navnar">
        <jsp:include page="navbar.jsp"/>
    </div>
    <div id="context" class="container">
        <jsp:include page="views/${viewName}.jsp"/>
    </div>
    <div id="footer">
        <jsp:include page="footer.jsp"/>
    </div>

    <script src="static/assets/js/input.js"></script>
    <script src="static/assets/js/registration.js"></script>
    <script src="static/assets/js/footer.js"></script>
    <script src="static/assets/js/jquery.min.js"></script>
    <script src="static/assets/bootstrap/js/bootstrap.min.js"></script>
</body>

</html>