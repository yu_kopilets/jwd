<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div class="text-center footer-basic" style="background-color: #f8f8f8; ">
    <footer>
        <a class="text-body list-inline-item" href="?lang=en"><fmt:message key="footer.lang.en"/></a>
        <a class="text-body list-inline-item" href="?lang=ru"><fmt:message key="footer.lang.ru"/></a>
        <p style="font-size: 14px; margin-top: 1%"><fmt:message key="footer.description"/></p>
        <ul class="list-inline">
            <li class="list-inline-item"><a href="home"><fmt:message key="menu.home"/></a></li>
            <li class="list-inline-item"><a href="template"><fmt:message key="menu.template"/></a></li>
            <li class="list-inline-item"><a href="units"><fmt:message key="menu.units"/></a></li>
            <li class="list-inline-item"><a href="typing"><fmt:message key="menu.typing"/></a></li>
            <li class="list-inline-item"><a href="lobby"><fmt:message key="menu.lobby"/></a></li>
        </ul>
        <p class="copyright">© 2020</p>
    </footer>
</div>