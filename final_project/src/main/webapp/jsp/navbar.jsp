<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="by.training.yukopilets.controller.command.CommandType" %>
<%@ taglib prefix="s" uri="securityTags" %>

<div style="background-color: #f8f8f8;">
    <div class="container">
        <nav class="navbar navbar-light navbar-expand-md">
            <div class="container-fluid"><a class="navbar-brand" href="home">HUSHKEY</a><button data-toggle="collapse" class="navbar-toggler" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse"
                     id="navcol-1">
                    <ul class="nav navbar-nav mr-auto">
                        <li class="nav-item" role="presentation"></li>
                        <li class="nav-item" role="presentation"><a class="nav-link" href="template"><fmt:message key="menu.template"/></a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link" href="units"><fmt:message key="menu.units"/></a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link" href="typing"><fmt:message key="menu.typing"/></a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link" href="lobby"><fmt:message key="menu.lobby"/></a></li>
                    </ul>

                    <s:checkCommand commandType="${CommandType.LOGIN}">
                        <a class="text-dark" href="login" style="margin-right: 1%;"><fmt:message key="menu.login"/></a>
                    </s:checkCommand>

                    <s:checkCommand commandType="${CommandType.REGISTRATION}">
                        <a class="btn btn-light btn-sm text-secondary border rounded-0" href="registration" role="button"><fmt:message key="menu.singUp"/></a>
                    </s:checkCommand>

                    <s:checkCommand commandType="${CommandType.LOGOUT}">
                        <a class="text-dark list-inline-item" href="?commandName=logout"><fmt:message key="menu.logout"/></a>
                        <a class="btn btn-dark" href="user-menu" style="background-image: url(&quot;static/assets/img/profile.png&quot;);width: 19px;height: 18px;padding: 0;"></a>
                    </s:checkCommand>
                </div>
            </div>
        </nav>
    </div>
</div>