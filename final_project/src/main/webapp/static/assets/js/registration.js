var login = document.getElementById('login');
var password = document.getElementById('password');
var confirm = document.getElementById('confirm');
var email = document.getElementById('email');

var submitLogin = document.getElementById('submitLogin');
submitLogin.disabled = true;

var loginIsCorrect = false;
var passwordIsCorrect = false;
var confirmIsCorrect = false;
var emailIsCorrect = false;

setInterval(function checkLogin() {
	if(login.value == ''){
		loginIsCorrect = false;
		login.style.backgroundColor = '#f7f9fc';
	} else if(login.value.length >= 4) {
		loginIsCorrect = true;
		login.style.backgroundColor = '#f7f9fc';
	} else {
		loginIsCorrect = false;
		login.style.backgroundColor = '#ff000050';
	}
}, 100);

setInterval(function checkPassword() {
	if(password.value == ''){
		passwordIsCorrect = false;
		password.style.backgroundColor = '#f7f9fc';
	} else if(password.value.length >= 4) {
		passwordIsCorrect = true;
		password.style.backgroundColor = '#f7f9fc';
	} else {
		passwordIsCorrect = false;
		password.style.backgroundColor = '#ff000050';
	}
}, 100);

setInterval(function checkConfirm() {
	if(confirm.value == password.value) {
		confirmIsCorrect = true;
		confirm.style.backgroundColor = '#f7f9fc';
	} else {
		confirmIsCorrect = false;
		confirm.style.backgroundColor = '#ff000050';
	}
}, 100);

function emailIsValid(email) {
	const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(email);
}

setInterval(function checkEmail() {
	if(email.value == ''){
		emailIsCorrect = false;
		email.style.backgroundColor = '#f7f9fc';
	} else if(emailIsValid(email.value)) {
		emailIsCorrect = true;
		email.style.backgroundColor = '#f7f9fc';
	} else {
		emailIsCorrect = false;
		email.style.backgroundColor = '#ff000050';
	}
}, 100);

setInterval(function formIsDone() {
	if(loginIsCorrect && passwordIsCorrect && confirmIsCorrect && emailIsCorrect) {
		submitLogin.disabled = false;
	} else {
		submitLogin.disabled = true;
	}
}, 100);
