var inputArea = document.getElementById('input_area');
var inputTemplate = document.getElementById('input_template');
var template = inputTemplate.textContent;
var currentKeyIndex = 0;

var bright = document.getElementById('bright_template')
bright.style.color = '#00ff00';

var cursor = document.getElementById('input_cursor');
var form = document.getElementById('input_form');

document.addEventListener('keydown', function handler(event) {

	if(event.key == ' ') {
		event.preventDefault();
	}

	if(isCorrectKeydown(event)) {
			inputArea.style.backgroundColor = 'white';
			inputArea.textContent += event.key;
			bright.textContent += event.key;
			inputTemplate.textContent = inputTemplate.textContent.replace(event.key, '');

			if(currentKeyIndex == template.length) {
				cursor.style.visibility = 'hidden';
				form.submit();
			}
	} else {
		if(event.key != 'Shift') {
			inputArea.style.backgroundColor = '#ff000050'; //red alfa=0.5
			if(template.length == 0) {
				setTimeout(function updateInputarea() {
					inputArea.style.backgroundColor = 'white';
				}, 1500);
			}
		}
	}	
});

document.addEventListener('keyup', function spaceUp(event) {
	if(event.key == ' ') {
		event.stopPropagation();
	}
});

function isCorrectKeydown(event) {
	
	if(event.code.indexOf('Key') != -1 || event.key == ' ' 
		|| event.key == '.' || event.key == ',' 
		|| event.key == ';' || event.key == ':'
		|| event.key == '\'' || event.key == '"' 
		|| event.key == '?' || event.key == '!' || event.key == '-')
	{
		if(currentKeyIndex < template.length && event.key == template[currentKeyIndex]) {
			currentKeyIndex++;
			return true;
		}
	}
	return false;
}

function changeCursor() {
	
	if(currentKeyIndex < template.length && currentKeyIndex != 0) {
		if(cursor.style.visibility == 'visible') {
			cursor.style.visibility = 'hidden';
		} else {
			cursor.style.visibility = 'visible';
		}
	}
}

setInterval(changeCursor, 500);
