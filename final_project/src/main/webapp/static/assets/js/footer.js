var navbar = document.getElementById("navnar");
var context = document.getElementById("context");
var footer = document.getElementById("footer");
var scrollHeight = document.body.scrollHeight;

var totalHeight = navbar.clientHeight + context.clientHeight + footer.clientHeight;
if(totalHeight < scrollHeight) {
    var deviation = scrollHeight - totalHeight;
    footer.style.marginTop = deviation + "px";
}
