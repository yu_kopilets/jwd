import com.sun.net.httpserver.HttpServer;
import controller.*;
import controller.command.*;
import controller.command.validator.SimpleFileVoucherValidator;
import dao.VoucherDao;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.AddVoucherService;
import service.DisplayVoucherService;
import service.SelectVoucherService;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class Main {

    private static final Logger LOGGER = LogManager.getLogger();

    public static void main(String[] args) throws IOException {
        HttpServer server = HttpServer.create(new InetSocketAddress(8008), 0);
        ThreadPoolExecutor threadPoolExecutor = (ThreadPoolExecutor) Executors.newFixedThreadPool(10);
        SimpleHttpHandler handler = init();

        server.createContext("/test", handler);
        server.setExecutor(threadPoolExecutor);
        server.start();

        LOGGER.info("Server started on " + server.getAddress().toString() + "\n\n");
    }

    private static SimpleHttpHandler init() {
        VoucherDao voucherDao = new VoucherDao();
        DisplayVoucherService displayVoucherService = new DisplayVoucherService(voucherDao);
        SelectVoucherService selectVoucherService = new SelectVoucherService(voucherDao);
        AddVoucherService addVoucherService = new AddVoucherService(voucherDao);

        DisplayCommand displayCommand = new DisplayCommand(displayVoucherService);
        SelectCommand selectCommand = new SelectCommand(selectVoucherService);
        AddCommand addCommand = new AddCommand(addVoucherService, new SimpleFileVoucherValidator());
        SortCommand sortCommand = new SortCommand(displayVoucherService);

        Map<AppCommandName, AppCommand> commands = new EnumMap<>(AppCommandName.class);
        commands.put(AppCommandName.DISPLAY_ALL, displayCommand);
        commands.put(AppCommandName.SELECT, selectCommand);
        commands.put(AppCommandName.ADD, addCommand);
        commands.put(AppCommandName.SORT, sortCommand);

        Map<String, RequestConverter> converters = new HashMap<>();
        converters.put("GET", new GetRequestConverter());
        converters.put("POST", new PostRequestConverter());

        SimpleAppCommandFactory commandFactory = new SimpleAppCommandFactory(commands);
        RequestConverterFactory converterFactory = new RequestConverterFactory(converters);
        return new SimpleHttpHandler(commandFactory, converterFactory);
    }
}
