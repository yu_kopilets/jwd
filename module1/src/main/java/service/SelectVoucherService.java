package service;

import dao.VoucherDao;
import entity.Voucher;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SelectVoucherService implements VoucherService {

    private final VoucherDao voucherDao;

    public SelectVoucherService(VoucherDao voucherDao) {
        this.voucherDao = voucherDao;
    }

    @Override
    public List<Voucher> getVouchers(Map<String, String> requestParams) {
        List<Voucher> vouchers = new ArrayList<>();

        requestParams.forEach((key, value) -> {
            vouchers.add(voucherDao.getItem(key));
        });
        return vouchers;
    }

    public BigDecimal sum(Map<String, String> requestParams) {
        final BigDecimal[] summaryPrice = {new BigDecimal("0")};
        requestParams.forEach((key, value) -> {
            summaryPrice[0] = summaryPrice[0].add(voucherDao.getItem(key).getPrice());
        });
        return summaryPrice[0];
    }
}
