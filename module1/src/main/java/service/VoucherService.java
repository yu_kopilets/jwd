package service;

import entity.Voucher;

import java.util.List;
import java.util.Map;

public interface VoucherService {

    List<Voucher> getVouchers(Map<String, String> requestParams);
}
