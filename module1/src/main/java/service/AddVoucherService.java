package service;

import dao.VoucherDao;
import entity.Voucher;
import entity.VoucherType;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AddVoucherService implements VoucherService {

    private final VoucherDao voucherDao;

    public AddVoucherService(VoucherDao voucherDao) {
        this.voucherDao = voucherDao;
    }

    public void addVouchers(List<String> lines) {

        List<Voucher> newVouchers = new ArrayList<>();
        Voucher voucher;
        for (String line : lines) {
            String[] parsedLine = line.split("[,;]");
            voucher = new Voucher(parsedLine[0], new BigDecimal(parsedLine[1]), VoucherType.getVoucherTypeByName(parsedLine[2]));
            newVouchers.add(voucher);
        }
        voucherDao.addItem(newVouchers);
    }
    
    @Override
    public List<Voucher> getVouchers(Map<String, String> requestParams) {
        return voucherDao.getAllItems();
    }
}
