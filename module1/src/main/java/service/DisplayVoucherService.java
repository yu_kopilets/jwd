package service;

import dao.VoucherDao;
import entity.Voucher;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DisplayVoucherService implements VoucherService {

    private final VoucherDao voucherDao;

    public DisplayVoucherService(VoucherDao voucherDao) {
        this.voucherDao = voucherDao;
    }

    @Override
    public List<Voucher> getVouchers(Map<String, String> requestParams) {
        return voucherDao.getAllItems();
    }

    public List<Voucher> getSortedVouchers() {
        List<Voucher> vouchers = voucherDao.getAllItems();
        vouchers = vouchers.stream()
                .sorted(Comparator.comparing(Voucher::getPrice))
                .collect(Collectors.toList());
        return vouchers;
    }
}
