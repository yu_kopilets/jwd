package entity;

import java.math.BigDecimal;

public class Voucher {

    private String name;
    private BigDecimal price;
    private VoucherType type;

    public Voucher(String name, BigDecimal price, VoucherType type) {
        this.name = name;
        this.price = price;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public VoucherType getType() {
        return type;
    }

    public void setType(VoucherType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Voucher's name is " + name + ". It's a " + type + " for " + price + "$";
    }
}
