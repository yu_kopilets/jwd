package entity;

public enum VoucherType {

    REST,
    TOURISM,
    MEDICINE,
    SHOPPING,
    CRUISE;

    public static VoucherType getVoucherTypeByName(String name) {

        for (VoucherType value : VoucherType.values()) {
            if(value.name().equals(name)) {
                return value;
            }
        }
        return REST;
    }
}
