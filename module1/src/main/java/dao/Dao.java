package dao;

import java.util.List;

public interface Dao<T> {

    List<T> getAllItems();
    T getItem(String itemName);
    void addItem(T item);
    void addItem(List<T> items);
}
