package dao;

import entity.Voucher;
import entity.VoucherType;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class InMemoryVoucherList {

    private static InMemoryVoucherList instance = null;
    private List<Voucher> vouchers;

    private InMemoryVoucherList() {

        vouchers = new ArrayList<>();
        vouchers.add(new Voucher("Egypt-Savanna", new BigDecimal("1200.0"), VoucherType.REST));
        vouchers.add(new Voucher("European Tour", new BigDecimal("1500.0"), VoucherType.TOURISM));
        vouchers.add(new Voucher("Germany", new BigDecimal("5520.0"), VoucherType.MEDICINE));
        vouchers.add(new Voucher("Switzerland's shops", new BigDecimal("4980.0"), VoucherType.SHOPPING));
        vouchers.add(new Voucher("Greece's lands", new BigDecimal("2250.0"), VoucherType.CRUISE));
    }

    public List<Voucher> getVouchers() {
        return vouchers;
    }

    public void addVoucher(Voucher voucher) {
        vouchers.add(voucher);
    }

    public void addVoucher(List<Voucher> vouchers) {
        this.vouchers.addAll(vouchers);
    }

    public static InMemoryVoucherList getInstance() {
        if(instance == null) {
            instance = new InMemoryVoucherList();
        }
        return instance;
    }
}
