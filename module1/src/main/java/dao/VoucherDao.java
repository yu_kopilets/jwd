package dao;

import entity.Voucher;

import java.util.List;

public class VoucherDao implements Dao<Voucher> {

    public List<Voucher> getAllItems() {
        return InMemoryVoucherList.getInstance().getVouchers();
    }

    public Voucher getItem(String itemName) {
        List<Voucher> vouchers = InMemoryVoucherList.getInstance().getVouchers();
        for (Voucher voucher : vouchers) {
            if(voucher.getName().equals(itemName)) {
                return voucher;
            }
        }
        return null;
    }

    @Override
    public void addItem(Voucher item) {
        InMemoryVoucherList.getInstance().addVoucher(item);
    }

    @Override
    public void addItem(List<Voucher> items) {
        InMemoryVoucherList.getInstance().addVoucher(items);
    }
}
