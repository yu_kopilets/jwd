package controller;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import controller.command.AppCommand;
import controller.command.SimpleAppCommandFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

public class SimpleHttpHandler implements HttpHandler {

    private static final Logger LOGGER = LogManager.getLogger();
    private final SimpleAppCommandFactory commandFactory;
    private final RequestConverterFactory converterFactory;

    public SimpleHttpHandler(SimpleAppCommandFactory commandFactory, RequestConverterFactory converterFactory) {
        this.commandFactory = commandFactory;
        this.converterFactory = converterFactory;
    }

    public void handle(HttpExchange httpExchange) throws IOException {
        LOGGER.info("Req: " + httpExchange.getRemoteAddress());
        LOGGER.info("URI: " + httpExchange.getRequestURI());
        LOGGER.info("Method: " + httpExchange.getRequestMethod());

        RequestConverter requestConverter = converterFactory.getConverter(httpExchange.getRequestMethod());
        Map<String, String> requestParams = requestConverter.convert(httpExchange);

        AppCommand command = commandFactory.createCommand(requestParams.get("commandName"));
        requestParams.remove("commandName");
        String htmlResponse = command.execute(requestParams);

        httpExchange.sendResponseHeaders(200, htmlResponse.length());
        OutputStream outputStream = httpExchange.getResponseBody();
        outputStream.write(htmlResponse.getBytes());
        outputStream.flush();
        outputStream.close();
    }
}
