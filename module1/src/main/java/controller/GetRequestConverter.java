package controller;

import com.sun.net.httpserver.HttpExchange;

import java.util.HashMap;
import java.util.Map;

public class GetRequestConverter implements RequestConverter {
    @Override
    public Map<String, String> convert(HttpExchange httpExchange) {
        String query = httpExchange.getRequestURI().getQuery();
        Map<String, String> requestParams = new HashMap<>();

        if(query != null && !query.equals("")) {
            String[] params = query.replaceAll("\\+", " ").split("&");
            for (String param : params) {
                requestParams.put(param.split("=")[0], param.split("=")[1]);
            }
        }
        return requestParams;
    }
}
