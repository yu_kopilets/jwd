package controller.command;

import controller.command.AppCommand;
import entity.Voucher;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.SelectVoucherService;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.MessageFormat;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class SelectCommand implements AppCommand {

    private final SelectVoucherService voucherService;

    public SelectCommand(SelectVoucherService voucherService) {
        this.voucherService = voucherService;
    }

    @Override
    public String execute(Map<String, String> requestParams) {

        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("order.html");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String template = bufferedReader.lines().collect(Collectors.joining());

        List<Voucher> vouchers = voucherService.getVouchers(requestParams);
        StringBuilder vouchersResponse = new StringBuilder();
        for (Voucher voucher : vouchers) {
            vouchersResponse.append("<p><label>" + voucher.toString() + "</label></p>");
        }
        String summary = voucherService.sum(requestParams) + "$";

        return MessageFormat.format(template, vouchersResponse, summary);
    }
}
