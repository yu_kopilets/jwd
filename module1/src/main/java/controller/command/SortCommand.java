package controller.command;

import entity.Voucher;
import service.DisplayVoucherService;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.MessageFormat;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class SortCommand implements AppCommand {

    private final DisplayVoucherService voucherService;

    public SortCommand(DisplayVoucherService voucherService) {
        this.voucherService = voucherService;
    }

    @Override
    public String execute(Map<String, String> requestParams) {

        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("main.html");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String template = bufferedReader.lines().collect(Collectors.joining());

        List<Voucher> vouchers = voucherService.getSortedVouchers();
        StringBuilder vouchersResponse = new StringBuilder();
        for (Voucher voucher : vouchers) {
            vouchersResponse.append("<p><input type=\"checkbox\" name=\"" + voucher.getName() + "\" value=\"checked\">" + voucher.getName() + "</p>");
        }

        return MessageFormat.format(template, vouchersResponse, "Input file's path:");
    }
}
