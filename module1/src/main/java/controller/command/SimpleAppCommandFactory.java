package controller.command;

import java.util.Map;

public class SimpleAppCommandFactory implements AppCommandFactory {

    private final Map<AppCommandName, AppCommand> commands;

    public SimpleAppCommandFactory(Map<AppCommandName, AppCommand> commands) {
        this.commands = commands;
    }

    @Override
    public AppCommand createCommand(String commandName) {
        AppCommandName appCommandName = AppCommandName.fromString(commandName);
        return commands.get(appCommandName);
    }
}
