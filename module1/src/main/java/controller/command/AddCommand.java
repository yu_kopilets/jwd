package controller.command;

import controller.command.validator.FileFormatException;
import controller.command.validator.FileVoucherValidator;
import entity.Voucher;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.AddVoucherService;

import java.io.*;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class AddCommand implements AppCommand {

    private static final Logger LOGGER = LogManager.getLogger();
    private final AddVoucherService voucherService;
    private final FileVoucherValidator validator;

    public AddCommand(AddVoucherService voucherService, FileVoucherValidator validator) {
        this.voucherService = voucherService;
        this.validator = validator;
    }

    @Override
    public String execute(Map<String, String> requestParams) {

        String filePath = requestParams.get("filePath");
        String fileFormat = "";
        String addVoucherResponse = "";

        try(InputStream inputStream = new FileInputStream(filePath)) {
            fileFormat = filePath.substring(filePath.lastIndexOf('.'));
            validator.fileFormatIsValid(fileFormat);
            addVoucherResponse = addVouchersFromFile(inputStream);

        } catch (FileNotFoundException e) {
            LOGGER.error("Wrong file's path from user query", e);
            addVoucherResponse = "File not found!";

        } catch (FileFormatException e) {
            LOGGER.error(e.getMessage(), e);
            addVoucherResponse = "File format must be .txt or .csv";

        } catch (IOException e) {
            LOGGER.error("User's file reading exception", e);

        }

        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("main.html");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String template = bufferedReader.lines().collect(Collectors.joining());

        List<Voucher> vouchers = voucherService.getVouchers(requestParams);
        StringBuilder vouchersResponse = new StringBuilder();
        for (Voucher voucher : vouchers) {
            vouchersResponse.append("<p><input type=\"checkbox\" name=\"" + voucher.getName() + "\" value=\"checked\">" + voucher.getName() + "</p>");
        }

        return MessageFormat.format(template, vouchersResponse, addVoucherResponse);
    }

    public String addVouchersFromFile(InputStream inputStream) {

        String addVoucherResponse = "The file does not have the correct data to add";

        try(BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream))) {
            List<String> correctVouchers = new ArrayList<>();
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                if(validator.fileLineIsValid(line)) {
                    addVoucherResponse = "New vouchers added!";
                    correctVouchers.add(line);
                }
            }
            voucherService.addVouchers(correctVouchers);
        } catch (IOException e) {
            LOGGER.error("User's file reading exception", e);
        }
        return addVoucherResponse;
    }
}
