package controller.command;

public interface AppCommandFactory {

    AppCommand createCommand(String commandName);
}
