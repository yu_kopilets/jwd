package controller.command.validator;

public interface FileVoucherValidator {

    boolean fileFormatIsValid(String fileFormat) throws FileFormatException;
    boolean fileLineIsValid(String fileLine);
}
