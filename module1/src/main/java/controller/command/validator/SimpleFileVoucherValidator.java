package controller.command.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SimpleFileVoucherValidator implements FileVoucherValidator {
    @Override
    public boolean fileFormatIsValid(String fileFormat) throws FileFormatException {
        if(fileFormat.equals(".csv") || fileFormat.equals(".txt")) {
            return true;
        }
        throw new FileFormatException("Format does not match .txt or .csv");
    }

    @Override
    public boolean fileLineIsValid(String fileLine) {
        Pattern pattern = Pattern.compile("[a-zA-Z- ']+[,;][0-9.]+[,;][A-Z]{4,8}\\b");
        Matcher matcher = pattern.matcher(fileLine);
        return matcher.find();
    }
}
