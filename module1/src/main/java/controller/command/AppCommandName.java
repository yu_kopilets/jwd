package controller.command;

public enum AppCommandName {

    DISPLAY_ALL("default"),
    SELECT("pick"),
    ADD("add"),
    SORT("sort");

    private  final String shortCommandName;

    AppCommandName(String shortCommandName) {
        this.shortCommandName = shortCommandName;
    }

    public static AppCommandName fromString(String commandName) {

        for (AppCommandName value : AppCommandName.values()) {
            if(value.shortCommandName.equals(commandName) || value.name().equals(commandName)) {
                return value;
            }
        }
        return DISPLAY_ALL;
    }
}
