package controller.command;

import java.util.Map;

public interface AppCommand {

    String execute(Map<String, String> requestParams);
}
