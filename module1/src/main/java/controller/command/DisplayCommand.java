package controller.command;

import controller.command.AppCommand;
import entity.Voucher;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.DisplayVoucherService;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.MessageFormat;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DisplayCommand implements AppCommand {

    private final DisplayVoucherService voucherService;

    public DisplayCommand(DisplayVoucherService voucherService) {
        this.voucherService = voucherService;
    }

    @Override
    public String execute(Map<String, String> requestParams) {

        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("main.html");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String template = bufferedReader.lines().collect(Collectors.joining());

        List<Voucher> vouchers = voucherService.getVouchers(requestParams);
        StringBuilder vouchersResponse = new StringBuilder();
        for (Voucher voucher : vouchers) {
            vouchersResponse.append("<p><input type=\"checkbox\" name=\"" + voucher.getName() + "\" value=\"checked\">" + voucher.getName() + "</p>");
        }

        return MessageFormat.format(template, vouchersResponse, "Input file's path:");
    }
}
