package controller;

import com.sun.net.httpserver.HttpExchange;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class PostRequestConverter implements RequestConverter {

    private static final Logger LOGGER = LogManager.getLogger();

    @Override
    public Map<String, String> convert(HttpExchange httpExchange) {
        StringBuilder queryBuilder = new StringBuilder();
        String query = "";

        try (InputStream inputStream = httpExchange.getRequestBody();
             InputStreamReader inputStreamReader = new InputStreamReader(inputStream, Charset.forName(StandardCharsets.UTF_8.name()));
             BufferedReader bufferedReader = new BufferedReader(inputStreamReader)
        ) {
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                queryBuilder.append(line);
            }
            query = URLDecoder.decode(queryBuilder.toString(), StandardCharsets.UTF_8.toString());
        } catch (IOException e) {
            LOGGER.error("POST Request reading exception", e);
        }

        Map<String, String> requestParams = new HashMap<>();

        if(query != null && !query.equals("")) {
            String[] params = query.replaceAll("\\+", " ").split("&");
            for (String param : params) {
                requestParams.put(param.split("=")[0], param.split("=")[1]);
            }
        }
        return requestParams;
    }
}
