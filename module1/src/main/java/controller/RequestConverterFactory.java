package controller;

import java.util.Map;

public class RequestConverterFactory {

    private final Map<String, RequestConverter> converters;

    public RequestConverterFactory(Map<String, RequestConverter> converters) {
        this.converters = converters;
    }

    public RequestConverter getConverter(String httpMethod) {
        return converters.get(httpMethod);
    }
}
