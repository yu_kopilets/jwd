package controller;

import com.sun.net.httpserver.HttpExchange;

import java.util.Map;

public interface RequestConverter {

    Map<String, String> convert(HttpExchange httpExchange);
}
