package controller.command;

import static org.junit.Assert.*;

import controller.command.validator.SimpleFileVoucherValidator;
import dao.VoucherDao;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import service.AddVoucherService;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class AddCommandTest {

    private AddVoucherService voucherService;
    private VoucherDao voucherDao;
    private SimpleFileVoucherValidator validator;
    private AddCommand command;

    @Before
    public void initTest() {
        voucherDao = new VoucherDao();
        voucherService = new AddVoucherService(voucherDao);
        validator = new SimpleFileVoucherValidator();
        command = new AddCommand(voucherService, validator);
    }

    @Test
    public void addVouchersFromFile() {

        URL url = getClass().getClassLoader().getResource("vouchers.csv");
        try {
            InputStream inputStream = url.openStream();
            assertEquals("New vouchers added!", command.addVouchersFromFile(inputStream));
        } catch (IOException e) {
            e.printStackTrace();
        }

        url = getClass().getClassLoader().getResource("vouchers3(empty).txt");
        try {
            InputStream inputStream = url.openStream();
            assertEquals("The file does not have the correct data to add", command.addVouchersFromFile(inputStream));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @After
    public void afterTest() {
        voucherDao = null;
        voucherService = null;
        validator = null;
        command = null;
    }
}