package service;

import dao.VoucherDao;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class DisplayVoucherServiceTest {

    private VoucherDao voucherDao;
    private VoucherService voucherService;

    @Before
    public void initTest() {
        voucherDao = new VoucherDao();
        voucherService = new DisplayVoucherService(voucherDao);
    }

    @Test
    public void getVouchers() {
        assertNotNull(voucherService.getVouchers(null));
    }

    @After
    public void afterTest() {
        voucherDao = null;
        voucherService = null;
    }
}