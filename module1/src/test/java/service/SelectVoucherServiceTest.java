package service;

import dao.VoucherDao;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class SelectVoucherServiceTest {

    private VoucherDao voucherDao;
    private SelectVoucherService voucherService;
    private Map<String, String> params;

    @Before
    public void initTest() {
        voucherDao = new VoucherDao();
        voucherService = new SelectVoucherService(voucherDao);
        params = new HashMap<>();
        params.put("Egypt-Savanna", "checked");
    }

    @Test
    public void getVouchers() {
        assertNotNull(voucherService.getVouchers(params));
    }

    @Test
    public void sum() {
        assertEquals("1200.0", voucherService.sum(params).toString());
    }

    @After
    public void afterTest() {
        voucherDao = null;
        voucherService = null;
        params = null;
    }
}