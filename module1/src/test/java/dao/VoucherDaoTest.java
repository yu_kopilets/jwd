package dao;

import org.junit.*;

import static org.junit.Assert.*;

public class VoucherDaoTest {

    private VoucherDao voucherDao;

    @Before
    public void initTest() {
        voucherDao = new VoucherDao();
    }

    @Test
    public void getAllItems() {
        assertNotNull(InMemoryVoucherList.getInstance().getVouchers());
    }


    @Test
    public void getItem() {
        assertNull(voucherDao.getItem(""));
    }

    @After
    public void afterTest() {
        voucherDao = null;
    }
}